import { request } from '@/utils/request'
import type { FormStep, FormSteppassword } from '@/api/model/formModel'
import { ListResult } from "@/api/model/listModel";
// 分布式表单提交
export function formDataes(params: any) {
  return request.post<any>({
    url: '/v1/receiver',
    data: params
  })
}
// 密码验证
export function validatePassword(params: FormSteppassword) {
  return request.post<FormSteppassword>({
    url: '/validate-password',
    data: params
  })
}
export function getFormList(params) {
  return request.post<ListResult>({
    url: '/v1/receiver',
    data: params
  })
}

export function getFormCategoryList(params) {
  return request.post<any>({
    url: '/v1/receiver',
    data: params
  })
}

export function saveFormCategory(params) {
  return request.post<any>({
    url: '/v1/receiver',
    data: params
  })
}

export function delFormCategory(params) {
  return request.post<any>({
    url: '/v1/receiver',
    data: params
  })
}

export function delForm(params) {
  return request.post<any>({
    url: '/v1/receiver',
    data: params
  })
}

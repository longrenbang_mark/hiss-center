import { request } from '@/utils/request'
import type {
  CardListResult,
  ListResult,
  addListParams,
  deleteListParams,
  ListCollapseResult,
  ListTransferModel,
  ListCardsortResult
} from '@/api/model/listModel'

export function getApplicationList(params) {
  return request.post<ListResult>({
    url: '/v1/receiver',
    data: params
  })
}

export function saveApplication(params) {
  return request.post<any>({
    url: '/v1/receiver',
    data: params
  })
}

export function delApplication(params) {
  return request.post<any>({
    url: '/v1/receiver',
    data: params
  })
}

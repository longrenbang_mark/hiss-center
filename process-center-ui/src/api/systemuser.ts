import { request } from '@/utils/request'
import type {
  CardListResult,
  ListResult,
  addListParams,
  deleteListParams,
  ListCollapseResult,
  ListTransferModel,
  ListCardsortResult
} from '@/api/model/listModel'

export function getSystemUserList(params) {
  return request.post<ListResult>({
    url: '/v1/receiver',
    data: params
  })
}

export function saveSystemUser(params) {
  return request.post<any>({
    url: '/v1/receiver',
    data: params
  })
}

export function delSystemUser(params) {
  return request.post<any>({
    url: '/v1/receiver',
    data: params
  })
}

export function resetPasswordSystemUser(params) {
  return request.post<any>({
    url: '/v1/receiver',
    data: params
  })
}

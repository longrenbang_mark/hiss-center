import { request } from "@/utils/request";
import type { ListResult } from "@/api/model/listModel";

export function scaffoldExport(params) {
  return request.post<ListResult>({
    url: '/v1/receiver',
    data: params,
    timeout: 180000,
    responseType: 'blob'
  })
}

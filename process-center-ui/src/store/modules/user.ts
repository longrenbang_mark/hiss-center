import { defineStore } from 'pinia'
import {
  TOKEN_NAME,
  PROCESS_HEADER_TOKEN_NAME,
  PROCESS_AUTH_USER
} from '@/config/global'
import { store, usePermissionStore } from '@/store'

const InitUserInfo = {
  roles: []
}

export const useUserStore = defineStore('user', {
  state: () => ({
    token: localStorage.getItem(TOKEN_NAME) || 'main_token', // 默认token不走权限
    userInfo: { ...InitUserInfo }
  }),
  getters: {
    roles: (state) => {
      return state.userInfo?.roles
    }
  },
  actions: {
    async login(token: string) {
      localStorage.setItem(PROCESS_HEADER_TOKEN_NAME, token)
      localStorage.setItem(TOKEN_NAME, token)
      this.token = token
    },
    async setUserInfo(data: any) {
      const hissProcessUser = {
        userId: data.uid,
        userName: data.username,
        appId: 'tenant_hiss',
        admin: data.admin
      }
      localStorage.setItem(PROCESS_AUTH_USER, JSON.stringify(hissProcessUser))
      this.userInfo = data
    },
    async getUserInfo() {
      return this.userInfo
    },
    async logout() {
      localStorage.removeItem(TOKEN_NAME)
      this.token = ''
      this.userInfo = { ...InitUserInfo }
    },
    async removeToken() {
      this.token = ''
    }
  },
  persist: {
    afterRestore: (ctx) => {
      if (ctx.store.roles && ctx.store.roles.length > 0) {
        const permissionStore = usePermissionStore()
        permissionStore.initRoutes(ctx.store.roles)
      }
    }
  }
})

export function getUserStore() {
  return useUserStore(store)
}

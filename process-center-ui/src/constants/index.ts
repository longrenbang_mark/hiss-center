import proxy from '@/config/proxy'
export const getHissBaseUrl = () => {
  const env = import.meta.env.MODE || 'development'
  // 如果是mock模式 或 没启用直连代理 就不配置host 会走本地Mock拦截 或 Vite 代理
  const host = env === 'mock' || !proxy.isRequestProxy ? '' : proxy[env].host

  console.log(111, env)
  if (env === 'development' || env === 'mock') {
    return 'http://localhost:8081'
    // return 'http://172.16.43.97:8081/'
  } else {
    return host || 'http://localhost:8081/api/'
  }
}
// 合同状态枚举
export const CONTRACT_STATUS = {
  FAIL: 0,
  AUDIT_PENDING: 1,
  EXEC_PENDING: 2,
  EXECUTING: 3,
  FINISH: 4
}

export const CONTRACT_STATUS_OPTIONS = [
  { value: CONTRACT_STATUS.FAIL, label: '审核失败' },
  { value: CONTRACT_STATUS.AUDIT_PENDING, label: '待审核' },
  { value: CONTRACT_STATUS.EXEC_PENDING, label: '待履行' },
  { value: CONTRACT_STATUS.EXECUTING, label: '审核成功' },
  { value: CONTRACT_STATUS.FINISH, label: '已完成' }
]

// 合同类型枚举
export const CONTRACT_TYPES = {
  MAIN: 0,
  SUB: 1,
  SUPPLEMENT: 2
}

export const CONTRACT_TYPE_OPTIONS = [
  { value: CONTRACT_TYPES.MAIN, label: '主合同' },
  { value: CONTRACT_TYPES.SUB, label: '子合同' },
  { value: CONTRACT_TYPES.SUPPLEMENT, label: '补充合同' }
]

// 合同收付类型枚举
export const CONTRACT_PAYMENT_TYPES = {
  PAYMENT: 0,
  RECEIPT: 1
}

// 通知的优先级对应的TAG类型
export const NOTIFICATION_TYPES = {
  low: 'primary',
  middle: 'warning',
  high: 'danger'
}
// 状态
export const STATUS = [
  { value: 0, label: '关闭' },
  { value: 1, label: '运行中' },
  { value: 2, label: '已上线' },
  { value: 3, label: '异常' }
]

// 是否正常
export const USESTATUS = [
  { value: 0, label: '停用' },
  { value: 1, label: '正常' }
]

export const SEARCH_TYPE = [
  {
    key: 'EQ',
    value: '等于'
  },
  {
    key: 'GT',
    value: '大于'
  },
  {
    key: 'GTE',
    value: '大于等于'
  },
  {
    key: 'LT',
    value: '小于'
  },
  {
    key: 'LTE',
    value: '小于等于'
  },
  {
    key: 'LIKE',
    value: '模糊'
  },
  {
    key: 'BETWEEN',
    value: '区间'
  }
]

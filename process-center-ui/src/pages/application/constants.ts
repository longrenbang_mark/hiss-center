// 获取常用时间
import dayjs from "dayjs";

export const COLUMNS = [
  {
    title: '',
    align: 'left',
    width: 40,
    minWidth: 40,
    colKey: 'icon'
  },
  {
    title: '应用名称',
    align: 'left',
    width: 160,
    minWidth: 100,
    colKey: 'appName'
  },
  {
    title: '应用ID',
    colKey: 'appId',
    width: 300,
    sorter: true,
    sortType: 'all'
  },
  {
    title: '应用网站',
    minWidth: 150,
    ellipsis: true,
    colKey: 'website'
  },
  {
    title: '状态',
    ellipsis: true, // 文字超出宽度时显示省略号
    colKey: 'status',
    // 添加筛选
    filter: {
      type: 'single',
      list: [
        {
          label: '停用',
          value: 0
        },
        {
          label: '正常',
          value: 1
        }
      ],
      showConfirmAndReset: true
    },
    cell: (h, { row }) => {
      const statusList = {
        0: {
          label: '停用'
        },
        1: {
          label: '正常'
        }
      }
      return h(
        'span',
        {
          class: `status-dot status-dot-${row.status}`
        },
        statusList[row.status].label
      )
    }
  },
  {
    title: '更新时间',
    ellipsis: true,
    colKey: 'updatedTime',
    sorter: true,
    sortType: 'all',
    cell: (h, { row }) => {
      return h(
        'span',
        {},
        row.updatedTime
          ? dayjs(row.updatedTime).format('YYYY-MM-DD HH:mm:ss')
          : ''
      )
    }
  },
  {
    title: '创建时间',
    ellipsis: true,
    colKey: 'createdTime',
    sorter: true,
    sortType: 'all',
    cell: (h, { row }) => {
      return h(
        'span',
        {},
        row.createdTime
          ? dayjs(row.createdTime).format('YYYY-MM-DD HH:mm:ss')
          : ''
      )
    }
  },
  {
    align: 'center',
    className: 'osClass',
    fixed: 'right',
    width: 200,
    minWidth: '157px',
    colKey: 'op',
    title: '操作',
    class: 'yyyy'
  }
]

// vite.config.ts
import { loadEnv } from "vite";
import { viteMockServe } from "vite-plugin-mock";
import createVuePlugin from "@vitejs/plugin-vue";
import vueJsx from "@vitejs/plugin-vue-jsx";
import svgLoader from "vite-svg-loader";
import path from "path";
var CWD = process.cwd();
var vite_config_default = ({ mode }) => {
  const { VITE_BASE_URL } = loadEnv(mode, CWD);
  return {
    base: VITE_BASE_URL,
    define: {},
    resolve: {
      alias: {
        "@": path.resolve("E:\\workspace\\hiss-center\\process-center-ui", "./src")
      }
    },
    css: {
      preprocessorOptions: {
        less: {
          modifyVars: {
            hack: `true; @import (reference) "${path.resolve(
              "src/style/variables.less"
            )}";`
          },
          math: "strict",
          javascriptEnabled: true
        }
      }
    },
    plugins: [
      createVuePlugin(),
      vueJsx(),
      viteMockServe({
        mockPath: "mock",
        localEnabled: false,
        prodEnabled: true,
        supportTs: true,
        logger: true,
        injectCode: `
          import { setupProdMockServer } from '../mockProdServer';
          setupProdMockServer();
        `
      }),
      svgLoader()
    ],
    server: {
      port: 3e3,
      host: "0.0.0.0",
      open: false,
      hmr: true,
      proxy: {
        "/api/v1/receiver": {
          target: "http://localhost:8081",
          changeOrigin: true,
          rewrite: (path2) => path2.replace(/^\/api/, "")
        },
        "/api": {
          target: "http://mock.boxuegu.com/mock/3547/",
          changeOrigin: true,
          rewrite: (path2) => path2.replace(/^\/api/, "")
        }
      }
    }
  };
};
export {
  vite_config_default as default
};
//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAic291cmNlcyI6IFsidml0ZS5jb25maWcudHMiXSwKICAic291cmNlc0NvbnRlbnQiOiBbImltcG9ydCB7IENvbmZpZ0VudiwgVXNlckNvbmZpZywgbG9hZEVudiB9IGZyb20gJ3ZpdGUnXHJcbmltcG9ydCB7IHZpdGVNb2NrU2VydmUgfSBmcm9tICd2aXRlLXBsdWdpbi1tb2NrJ1xyXG5pbXBvcnQgY3JlYXRlVnVlUGx1Z2luIGZyb20gJ0B2aXRlanMvcGx1Z2luLXZ1ZSdcclxuaW1wb3J0IHZ1ZUpzeCBmcm9tICdAdml0ZWpzL3BsdWdpbi12dWUtanN4J1xyXG5pbXBvcnQgc3ZnTG9hZGVyIGZyb20gJ3ZpdGUtc3ZnLWxvYWRlcidcclxuXHJcbmltcG9ydCBwYXRoIGZyb20gJ3BhdGgnXHJcbi8vIGltcG9ydCB7IHdpdGhTY29wZUlkIH0gZnJvbSAndnVlJ1xyXG5cclxuY29uc3QgQ1dEID0gcHJvY2Vzcy5jd2QoKVxyXG5cclxuLy8gaHR0cHM6Ly92aXRlanMuZGV2L2NvbmZpZy9cclxuZXhwb3J0IGRlZmF1bHQgKHsgbW9kZSB9OiBDb25maWdFbnYpOiBVc2VyQ29uZmlnID0+IHtcclxuICBjb25zdCB7IFZJVEVfQkFTRV9VUkwgfSA9IGxvYWRFbnYobW9kZSwgQ1dEKVxyXG4gIHJldHVybiB7XHJcbiAgICBiYXNlOiBWSVRFX0JBU0VfVVJMLFxyXG4gICAgZGVmaW5lOiB7fSxcclxuICAgIHJlc29sdmU6IHtcclxuICAgICAgYWxpYXM6IHtcclxuICAgICAgICAnQCc6IHBhdGgucmVzb2x2ZShcIkU6XFxcXHdvcmtzcGFjZVxcXFxoaXNzLWNlbnRlclxcXFxwcm9jZXNzLWNlbnRlci11aVwiLCAnLi9zcmMnKVxyXG4gICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGNzczoge1xyXG4gICAgICBwcmVwcm9jZXNzb3JPcHRpb25zOiB7XHJcbiAgICAgICAgbGVzczoge1xyXG4gICAgICAgICAgbW9kaWZ5VmFyczoge1xyXG4gICAgICAgICAgICBoYWNrOiBgdHJ1ZTsgQGltcG9ydCAocmVmZXJlbmNlKSBcIiR7cGF0aC5yZXNvbHZlKFxyXG4gICAgICAgICAgICAgICdzcmMvc3R5bGUvdmFyaWFibGVzLmxlc3MnXHJcbiAgICAgICAgICAgICl9XCI7YFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIG1hdGg6ICdzdHJpY3QnLFxyXG4gICAgICAgICAgamF2YXNjcmlwdEVuYWJsZWQ6IHRydWVcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgcGx1Z2luczogW1xyXG4gICAgICBjcmVhdGVWdWVQbHVnaW4oKSxcclxuICAgICAgdnVlSnN4KCksXHJcbiAgICAgIHZpdGVNb2NrU2VydmUoe1xyXG4gICAgICAgIG1vY2tQYXRoOiAnbW9jaycsXHJcbiAgICAgICAgbG9jYWxFbmFibGVkOiBmYWxzZSxcclxuICAgICAgICBwcm9kRW5hYmxlZDogdHJ1ZSxcclxuICAgICAgICBzdXBwb3J0VHM6IHRydWUsXHJcbiAgICAgICAgbG9nZ2VyOiB0cnVlLFxyXG4gICAgICAgIGluamVjdENvZGU6IGBcclxuICAgICAgICAgIGltcG9ydCB7IHNldHVwUHJvZE1vY2tTZXJ2ZXIgfSBmcm9tICcuLi9tb2NrUHJvZFNlcnZlcic7XHJcbiAgICAgICAgICBzZXR1cFByb2RNb2NrU2VydmVyKCk7XHJcbiAgICAgICAgYFxyXG4gICAgICB9KSxcclxuICAgICAgc3ZnTG9hZGVyKClcclxuICAgIF0sXHJcblxyXG4gICAgc2VydmVyOiB7XHJcbiAgICAgIHBvcnQ6IDMwMDAsXHJcbiAgICAgIGhvc3Q6ICcwLjAuMC4wJyxcclxuICAgICAgb3BlbjogZmFsc2UsXHJcbiAgICAgIGhtcjogdHJ1ZSxcclxuICAgICAgcHJveHk6IHtcclxuICAgICAgICAnL2FwaS92MS9yZWNlaXZlcic6IHtcclxuICAgICAgICAgIHRhcmdldDogJ2h0dHA6Ly9sb2NhbGhvc3Q6ODA4MScsXHJcbiAgICAgICAgICAvLyB0YXJnZXQ6ICdodHRwOi8vMTcyLjE2LjQzLjk3OjgwODEvJywgLy8gXHU3RjU3XHU3Njg0SVBcclxuICAgICAgICAgIC8vIHRhcmdldDogJ2h0dHA6Ly9sb2NhbGhvc3Q6ODA4MS8nLFxyXG4gICAgICAgICAgY2hhbmdlT3JpZ2luOiB0cnVlLFxyXG4gICAgICAgICAgcmV3cml0ZTogKHBhdGgpID0+IHBhdGgucmVwbGFjZSgvXlxcL2FwaS8sICcnKVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgJy9hcGknOiB7XHJcbiAgICAgICAgICB0YXJnZXQ6ICdodHRwOi8vbW9jay5ib3h1ZWd1LmNvbS9tb2NrLzM1NDcvJyxcclxuICAgICAgICAgIGNoYW5nZU9yaWdpbjogdHJ1ZSxcclxuICAgICAgICAgIHJld3JpdGU6IChwYXRoKSA9PiBwYXRoLnJlcGxhY2UoL15cXC9hcGkvLCAnJylcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC8vICxcclxuICAgIC8vIGJ1aWxkOiB7XHJcbiAgICAvLyAgIC8vIG91dERpcjogJy4uL3Byb2Nlc3MtZGVzaWduZXItdWkvcHVibGljJywgLy8gJy9Vc2Vycy96aGFuZ2ppYW4vRGVza3RvcC9oaXNzL3Byb2Nlc3MtZGVzaWduZXItdWkvcHVibGljLycsXHJcbiAgICAvLyAgIHJvbGx1cE9wdGlvbnM6IHtcclxuICAgIC8vICAgICBpbnB1dDogJy4vc3JjL2J1aWxkLnRzJyxcclxuICAgIC8vICAgICBvdXRwdXQ6IHtcclxuICAgIC8vICAgICAgIC8vIGNodW5rRmlsZU5hbWVzOiAnanMvW25hbWVdLVtoYXNoXS5qcycsXHJcbiAgICAvLyAgICAgICBlbnRyeUZpbGVOYW1lczogJ2J1aWxkLmpzJ1xyXG4gICAgLy8gICAgIH1cclxuICAgIC8vICAgfVxyXG4gICAgLy8gfVxyXG4gIH1cclxufVxyXG4iXSwKICAibWFwcGluZ3MiOiAiO0FBQUEsU0FBZ0MsZUFBZTtBQUMvQyxTQUFTLHFCQUFxQjtBQUM5QixPQUFPLHFCQUFxQjtBQUM1QixPQUFPLFlBQVk7QUFDbkIsT0FBTyxlQUFlO0FBRXRCLE9BQU8sVUFBVTtBQUdqQixJQUFNLE1BQU0sUUFBUSxJQUFJO0FBR3hCLElBQU8sc0JBQVEsQ0FBQyxFQUFFLEtBQUssTUFBNkI7QUFDbEQsUUFBTSxFQUFFLGNBQWMsSUFBSSxRQUFRLE1BQU0sR0FBRztBQUMzQyxTQUFPO0FBQUEsSUFDTCxNQUFNO0FBQUEsSUFDTixRQUFRLENBQUM7QUFBQSxJQUNULFNBQVM7QUFBQSxNQUNQLE9BQU87QUFBQSxRQUNMLEtBQUssS0FBSyxRQUFRLGlEQUFpRCxPQUFPO0FBQUEsTUFDNUU7QUFBQSxJQUNGO0FBQUEsSUFFQSxLQUFLO0FBQUEsTUFDSCxxQkFBcUI7QUFBQSxRQUNuQixNQUFNO0FBQUEsVUFDSixZQUFZO0FBQUEsWUFDVixNQUFNLDhCQUE4QixLQUFLO0FBQUEsY0FDdkM7QUFBQSxZQUNGO0FBQUEsVUFDRjtBQUFBLFVBQ0EsTUFBTTtBQUFBLFVBQ04sbUJBQW1CO0FBQUEsUUFDckI7QUFBQSxNQUNGO0FBQUEsSUFDRjtBQUFBLElBRUEsU0FBUztBQUFBLE1BQ1AsZ0JBQWdCO0FBQUEsTUFDaEIsT0FBTztBQUFBLE1BQ1AsY0FBYztBQUFBLFFBQ1osVUFBVTtBQUFBLFFBQ1YsY0FBYztBQUFBLFFBQ2QsYUFBYTtBQUFBLFFBQ2IsV0FBVztBQUFBLFFBQ1gsUUFBUTtBQUFBLFFBQ1IsWUFBWTtBQUFBO0FBQUE7QUFBQTtBQUFBLE1BSWQsQ0FBQztBQUFBLE1BQ0QsVUFBVTtBQUFBLElBQ1o7QUFBQSxJQUVBLFFBQVE7QUFBQSxNQUNOLE1BQU07QUFBQSxNQUNOLE1BQU07QUFBQSxNQUNOLE1BQU07QUFBQSxNQUNOLEtBQUs7QUFBQSxNQUNMLE9BQU87QUFBQSxRQUNMLG9CQUFvQjtBQUFBLFVBQ2xCLFFBQVE7QUFBQSxVQUdSLGNBQWM7QUFBQSxVQUNkLFNBQVMsQ0FBQ0EsVUFBU0EsTUFBSyxRQUFRLFVBQVUsRUFBRTtBQUFBLFFBQzlDO0FBQUEsUUFDQSxRQUFRO0FBQUEsVUFDTixRQUFRO0FBQUEsVUFDUixjQUFjO0FBQUEsVUFDZCxTQUFTLENBQUNBLFVBQVNBLE1BQUssUUFBUSxVQUFVLEVBQUU7QUFBQSxRQUM5QztBQUFBLE1BQ0Y7QUFBQSxJQUNGO0FBQUEsRUFZRjtBQUNGOyIsCiAgIm5hbWVzIjogWyJwYXRoIl0KfQo=

package cn.itcast.hiss.api.client;

/*
 * @author miukoo
 * @description 流程内使用到的常量
 * @date 2023/6/3 11:23
 * @version 1.0
 **/
public class HissFormConstants {

    /**
     * 表单配置的资源名称
     */
    public final static String FORM_CONFIG_JSON = "hiss.form.json";

    /**
     * 表单数据存储到字段后，需要结构化解析的前缀标识
     */
    public final static String JSON_PRIFEX = "h_i_s_s__";

    /**
     * 主表的标识
     */
    public final static String FORM_MAIN_TABLE_FLAG = "main";

    /**
     * 内部表单主键
     */
    public final static String FORM_KEY_FIELD_NAME = "_id";


}

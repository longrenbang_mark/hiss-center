package cn.itcast.hiss.tcp;

/*
 * @author miukoo
 * @description TCP 用到的常量类
 * @date 2023/5/29 11:17
 * @version 1.0
 **/
public class TcpConstants {
    // 粘包和拆包
    public static final String MESSAGE_DELIMITER = "$_$";
}

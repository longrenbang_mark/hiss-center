package cn.itcast.hiss.common.enums;

/*
 * @author miukoo
 * @description 触发信息的类型
 * @date 2023/6/20 11:30
 * @version 1.0
 **/
public enum ReceivedTypeEnum {
    MSG,SIGN,COP
}

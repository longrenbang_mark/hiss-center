package cn.itcast.hiss.common.enums;

/*
 * @author miukoo
 * @description 激活节点的类型
 * @date 2023/6/5 9:50
 * @version 1.0
 **/
public enum ActiveNodeTypeEnum {
    NONE,MULTIPLE_PARALLEL,MULTIPLE_SEQUENTIAL,CC,STARTER,NOTIFICATION,SINGLE_APPROVE
}

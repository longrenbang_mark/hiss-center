package cn.itcast.hiss.common.enums;

/*
 * @author miukoo
 * @description 流程节点的类型，在前端viewer中可以显示
 * @date 2023/5/25 16:55
 * @version 1.0
 **/
public enum ProcessNodeStatusEnum {
    PRE,DEFAULT,COMPLETE,ACTIVE,PENDING,CANCEL
}

package cn.itcast.hiss.process.activiti.mapper;

import cn.itcast.hiss.process.activiti.pojo.HissProcessPreLaunch;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
* <p>
* hiss_process_pre_launch Mapper 接口
* </p>
*
* @author lenovo
* @since 2023-06-14 11:35:36
*/
public interface HissProcessPreLaunchMapper extends BaseMapper<HissProcessPreLaunch> {


}

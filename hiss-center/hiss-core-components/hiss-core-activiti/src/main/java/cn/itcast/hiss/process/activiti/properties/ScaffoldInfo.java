package cn.itcast.hiss.process.activiti.properties;

import lombok.Data;

/*
 * @author miukoo
 * @description 脚手架配置类
 * @date 2023/7/15 16:55
 * @version 1.0
 **/
@Data
public class ScaffoldInfo {

    String templatePath;
    String tempPath;

}

# 流程中心

#### 项目简介

> 流程中心主要是想审批流程的设计及相关自定义表单的设计

项目迭代日志：
23 年 6 月 7 - 提交初始代码
#### 集成配置
> 一共分为两个组件：
> 表单设计:  渲染表单 主要功能包含
> 生成表单: 主要两个功能 
> 1、提供表单设计的预览界面 
> 2、提供表单使用的表单渲染功能
```js
{
  type: 'render',  // 主要是针对 表单渲染  render 为渲染使用  preview // 预览 不走接口 走的是生成数据
  formRoute: { type: 'design', path: '/' }, // 三方集成需要提供使用页面的路由
  baseURL: { host: '', urlPrefix: '', url: '' }, // 请求信息的提供 - 三方集成时使用 - 这个后面会弃用
  // 文件上传地址配置 使用后端接口上传或者直传OSS 二选一
  uploadConf:{
    url:'https://service-bv448zsw-1257786608.gz.apigw.tencentcs.com/api/upload-demo', // 后端接口上传  只有 file 一个二进制的流文件 回参需要将url返回
    headers:{}
  }
  ossConf:{
    region:'oss-cn-zhangjiakou',
    accessKeyId: 'you_accessKeyId'
    accessKeySecret: 'you_accessKeySecret',
    bucket: 'you_bucket'
  }
  /*
  部门的返回数据为:
  {
    code: number, // 200成功 其他会走到error里 弹出 message 字段
    data:{
      id: string | number
      name: string
    }, 
    message: string
  }
  */
  departMentData: [], // 部门控件列表数据 - 于以下配置二选一 可以直接提供数据也可以提供请求
  departMentQueryData: { // 部门控件列表数据请求
    url: '' // 请求地址
    method: 'get'  // 请求方式 get post 
    params: {}  // 参数
    headers: {} // 头信息
  },
  /*
  人员的返回数据为:
  {
    code: number, // 200成功 其他会走到error里 弹出 message 字段
    data:{
      id: string | number
      name: string
    }, 
    message: string
  }
  */
  memberData:[] // 人员控件列表数据 - 于以下配置二选一 可以直接提供数据也可以提供请求
  memberQueryData: { // 人员控件列表请求
    url: '',  // 请求地址
    method: 'get', // 请求方式 get post 
    params: {}, // 参数
    headers: {} // 头信息
  }
  request: request,  // 解决跨域问题 集成到其他项目中的时候会有跨域问题 - 只在测试过程中使用
  baseData: {   // 流程数据 - 主要子集成到流程中 处理表单渲染处理 其中表单的formConfig.fields 下的type 整合到了表单展示中 包含 edit 可编辑、view 只读的预览、hide 隐藏 三种模式
    formId: 'dc0af849c7f98019a6918debe03fcf56',
    formName: '请假单',
    dataId: '706e05b3-8597-4aea-afd0-056061208c82',
    formConfig: {
      id: 20,
      name: '请假单',
      fields: [
        {
          fieldName: '61197625722839',
          fieldLabel: '姓名',
          type: 'edit'
        },
      ],
      type: '0',
      all: 'edit',
      nodeId: 'Activity_158gpps'
    },
    modelId: 'f04bc83843ab8e1001a90ae4b66cb38e'
  }
}
```
<!-- 子表的设计使用 -->
- 子表的设计 - 子表单 目前的实现是引用其他表单


#### 研发规范遵循：

- 主体使用驼峰命名
- 公共样式使用 - 连接命名
- 内部样式 驼峰命名
- 页面命名 使用小写开头的驼峰命名
- 组件使用大写开头命名

#### 产品原型及设计

#### 运行环境 - 初始开发环境及工具

- 项目开发环境: Mac + node: v17.8.0 + npm: 8.12.1 || pnpm: 6.32.8

#### 访问地址

#### 技术栈应用

Vue 3 + TypeScript +Tdesign + Vite + pinia

#### 项目结构

```
├── commitlint.config.js - commintlint 规范 
├── docker - docker 部署配置文件 
│ └── nginx.conf - 若开启 browerhistroy 可参考配置 
├── docs - 项目展示图 -首页截图
├── globals.d.ts - 通用声明文件 
├── index.html - 主 html 文件 
├── mock - mock 目录 
│ └── index.ts 
├── node_modules - 项目依赖 
├── package-lock.json 
├── package.json 
├── public - 公用资源文件 
│ └── favicon.ico 
├── shims-vue.d.ts 
├── src - 页面代码 
│ ├── api 请求相关 
│ ├── assets 公共资源 
│ │ ├── images 图片资源
│ ├── api - 接口 
│ ├── conponents - 公用组件 
│ │ ├── Delete - 删除弹层：只需从父组件传删除的内容提示 
│ │ ├── ImageMagnify - 查看图片弹层 
│ │ ├── Message - 提示弹层
│ │ │ ├──Success - 成功通知弹窗 
│ │ │ ├──ProdDisabled - 禁用提示弹窗 
│ │ ├── switchBar - tab切换 
│ │ │ ├──switchBar - tab切换 
│ │ │ ├──switchBarindex - 首页tab切换 
│ │ │ ├──switchBartop - 线条tab 
│ ├──czriComponents - 组件包：常用，但是原有组件库实现不了，因此进行了二次开发 
│ │ ├──AddInput - 动态添加input组件 
│ │ ├──CardListCollapse - 折叠列表 
│ │ ├──CardListSort - 带排序的列表 
│ │ ├──dropList - 可下拉展开的列表 
│ │ ├──ListDialog - 常用弹窗5（带列表的弹窗） 
│ │ ├──ListScrollDialog - 常用弹窗5（带列表的弹窗、滚动分页） 
│ │ ├──TabDialog - 常用弹窗6（带tab的弹窗）
│ │ ├──tabList - 带tab的列表 
│ │ ├──Transfer - 穿梭框(标题有hover状态) 
│ │ ├──treeList - 树形列表 
│ │ ├──UnitDialog - 常用弹窗2（带单位和数显） 
│ │ ├──index.less - 组件包样式 
│ ├── layouts - 页面架构 
│ │ ├──components - 页面架构公共组件 
│ │ │ ├──Breadcrumb - 面包屑 
│ │ │ ├──Content - 内置组件，避免重复渲染DOM 
│ │ │ ├──Footer - 底部公司名称 
│ │ │ ├──LayoutContentSide - 侧边栏 
│ │ │ ├──LayoutHeader - 侧边栏头部 
│ │ │ ├──Loginfo - 侧边栏退出区域 
│ │ │ ├──Notice - 通知中心，弃用 
│ │ │ ├──Search - 搜索功能 
│ │ ├──frame - 页面架构框架 
│ │ ├──simple2Components - 框架二公用内容 │
│ │ ├──Header - 框架顶部 
│ │ ├──simpleComponents - 框架公用内容 
│ │ │ ├──MenuContent - 简版布局 
│ │ │ ├──SideNav - 列表菜单 
│ │──index.vue - 框架布局
│ │──setting.vue = 设置框架风格 
│ ├── pages - 页面展示目录 
│ │ ├──dashboard - 首页 
│ │ ├──detail - 详情页 
│ │ │ ├──advanced - 多卡片详情页 
│ │ │ ├──base - 基础详情页 
│ │ │ ├──deploy - 数据详情页 
│ │ │ ├──secondary - 二级详情页 
│ │ ├── form - 表单页 
│ │ │ ├──base - 基础表单页 
│ │ │ ├──step - 分步表单页 
│ │ ├──login - 普通tab登录页 
│ │ ├──login2 - 左右布局登录页 
│ │ ├──module - 组件包调用入口页，调用的组件都在(czriComponents文件加中) 
│ │ ├──user - 个人中心
│ │ ├──list - 列表页 
│ │ │ ├──base - 基础列表页 
│ │ │ ├──upBase - 基础列表页（带图） 
│ │ │ ├──noSearch - 基础列表页（不带搜索） 
│ │ │ ├──card - 卡片列表页 
│ │ │ ├──noData - 列表无数据页 
│ │ │ ├──statistics - 统计数据列表页 
│ │ │ ├── tab - tab切换 
│ │ │ ├──topTab - 标签列表页（顶部） 
│ │ │ ├──tree - 树状筛选列表页 
│ ├── router - 定义路由页面 
│ ├── style - 样式 
│ │ ├──componentsReast - 组件重置、全局样式 
│ │ ├──theme - 全局颜色值、公用样式 
│ │ index.less - 样式总入口 
│ │ normal.less - 普通框架样式 
│ │ noSecondMenu.less - 普通框架简化版样式 
│ │ top.less - 上左右布局 
│ ├── utils 封装工具目录 
│ ├── main.ts - 项目入口文件 
│ ├── permission.ts - 路有权限控制 
├── tsconfig.json - ts配置文件 
├── README.md - 说明文档 
└── vite.config.js - vite 配置文件
```

#### 安装运行

```bash
## 安装依赖
npm install || yarn

## 启动项目

# 启动链接mock
npm run dev
# 启动链接测试环境
npm run start

## 构建正式环境 - 打包
npm run build

```

#### 插件

nprogress 进度条

viteMockServe vite 的数据模拟插件

vueJsx

> 使用 jsx 语法 jsx 语法可以更好地跟 Typescript 结合 在阅读 UI 框架源码时，发现在知名 UI 组件库 Ant Design Vue 跟 Naive UI 皆使用 tsx 语法开发
> vite-svg-loader

#### 参考

vite
vue3
pinia 中文文档 :类 vuex
vue-router
Tdesign
Tdesign-cli

tsconfig.json 配置

#### 文档

tsconfig.json 配置整理
vite.config.js vite 配置文件

// 项目入口文件
import { createApp } from 'vue'
import TDesign from 'tdesign-vue-next'
import { createRouter, createWebHashHistory, createWebHistory } from 'vue-router'
import 'tdesign-vue-next/es/style/index.css'
import 'default-passive-events'
import './style/iconfont/iconfont.css'
import { store } from './store'
import allRoutes from './router'
import '@/style/index.less'
import './permission' // permission是路由权限控制

import App from './App.vue'

type BusTp = {
  type: String // 'render' 渲染 : 'preview' // 预览
  baseURL?: Object
  formRoute: any
  baseData: any // 初始化表单ID
  history: String
}

const createForm = (val: BusTp) => {
  const app: any = createApp(App)

  if (val.formRoute.type === 'render') {
    allRoutes[0].path = val.formRoute.path
    allRoutes[1].path = '/design'
  } else {
    allRoutes[0].path = '/render'
    allRoutes[1].path = val.formRoute.path
  }
  // 创建路由
  let history = null
  if (!val.history || val.history === 'Hash') {
    history = createWebHashHistory()
  } else {
    history = createWebHistory()
  }
  const router = createRouter({
    history,
    routes: allRoutes
  })

  app.$initConfig = val
  window.$initConfig = val
  app.use(TDesign)
  app.use(store)
  app.use(router)
  return app
}

window.createForm = createForm

export default createForm

// 获取常用时间
import dayjs from "dayjs";

export const LAST_7_DAYS = [
  dayjs().subtract(7, "days").format("YYYY-MM-DD"),
  dayjs().subtract(1, "days").format("YYYY-MM-DD"),
];

export const LAST_30_DAYS = [
  dayjs().subtract(30, "days").format("YYYY-MM-DD"),
  dayjs().subtract(1, "days").format("YYYY-MM-DD"),
];

import { request } from '@/utils/request'

// request 为暂缓方案 这里原因是集成到别的项目 本项目的代理实现会出现跨域
// 分布式表单提交
export function formDataes(params: any) {
  const reqFn = (window as any).$initConfig.request
  const fn = reqFn
    ? reqFn({
        url: '/v1/receiver',
        method: 'post',
        data: params
      })
    : request.post({
        url: '/api/v1/receiver',
        data: params
      })
  return fn
}

// export function formDataes(params: any) {
//   return request.post({
//     url: '/api/v1/receiver',
//     data: params
//   })
// }

// 密码验证
// export function validatePassword(params: FormSteppassword) {
//   return request<FormSteppassword>({
//     url: '/validate-password',
//     method: 'post',
//     data: params
//   })
// }
// // 分布式表单提交
// export function formDataes(params: any) {
//   return request.post<any>({
//     url: '/v1/receiver',
//     data: params
//   })
// }
// // 密码验证
// export function validatePassword(params: FormSteppassword) {
//   return request.post<FormSteppassword>({
//     url: '/validate-password',
//     data: params
//   })
// }

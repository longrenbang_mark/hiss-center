// 项目入口文件
import { createApp } from 'vue'
import TDesign from 'tdesign-vue-next'
import { createRouter, createWebHashHistory } from 'vue-router'
import 'tdesign-vue-next/es/style/index.css'
import 'default-passive-events'
import './style/iconfont/iconfont.css'
import { store } from './store'
import allRoutes from './router'
import '@/style/index.less'
import './permission' // permission是路由权限控制

import App from './App.vue'

type BusTp = {
  type: String // 'render' 渲染 : 'preview' // 预览
  baseURL?: Object
  formRoute: any
  renderFromHeight?: string // 渲染表单的高 主要解决渲染表单给默认高度的问题  默认是auto
  renderLeabelWidth?: string // 可以给固定宽 '100px'  也可以给模式 'comput' 计算规则是 文字长度 * 20  默认是 100px
  baseData: any // 初始化表单ID
  uploadConf?: Object
  ossConf?: Object
  departMentQueryData?: {
    url: string // 请求地址
    method?: string // 请求方式 get post
    params?: Object
    headers?: Object
  }
  memberQueryData?: {
    url: string // 请求地址
    method?: string // 请求方式 get post
    params?: Object
    headers?: Object
  }
}

const createForm = (val: BusTp) => {
  const app: any = createApp(App)

  if (val.formRoute.type === 'render') {
    allRoutes[0].path = val.formRoute.path
  } else {
    allRoutes[1].path = val.formRoute.path
  }

  const router = createRouter({
    history: createWebHashHistory(),
    routes: allRoutes
  })

  app.submitFn = (val) => {
    console.log(val)
  } // 暴露给实例的方法
  app.submitCatchFn = (val) => {
    console.log(val)
  } // 暴露给实例的方法
  app.$initConfig = val
  window.$initConfig = val
  app.use(TDesign)
  app.use(store)
  app.use(router)
  return app
}

window.createForm = createForm

export default createForm

createForm({
  type: 'render',
  formRoute: { type: 'design', path: '/' }, // {type: 'render', path: '/'},
  baseURL: { host: '', urlPrefix: '', url: '' },
  renderFromHeight: 'calc(100vh - 200px)', //
  // request: request,  // 解决跨域问题 使用外部的
  baseData: { formId: '475c7298c7689aeb7d4efdc46ed31fed' },
  uploadConf: {
    url: 'http://172.16.43.97:9995/form/upload',
    headers: {}
  },
  departMentQueryData: {
    // 部门控件列表数据请求
    url: 'http://172.16.43.97:9995/dept', // 请求地址
    method: 'post', // 请求方式 get post
    params: {
      type: 'query'
    } // 参数
  },
  memberQueryData: {
    // 部门控件列表数据请求
    url: 'http://172.16.43.97:9995/user', // 请求地址
    method: 'post', // 请求方式 get post
    params: {
      type: 'query'
    } // 参数
  }
  // ossConf: {
  //   region: 'oss-cn-zhangjiakou', // 服务器所在地区
  //   accessKeyId: 'Key',
  //   accessKeySecret: '秘钥',
  //   bucket: 'bucket'
  // }
}).mount('#app')

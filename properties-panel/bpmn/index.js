'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var hooks = require('../preact/hooks');
var minDash = require('min-dash');
var compat = require('../preact/compat');
var jsxRuntime = require('../preact/jsx-runtime');
var classnames = require('classnames');
var minDom = require('min-dom');
var preact = require('../preact');
var feelers = require('feelers');
var FeelEditor = require('@bpmn-io/feel-editor');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var classnames__default = /*#__PURE__*/_interopDefaultLegacy(classnames);
var FeelEditor__default = /*#__PURE__*/_interopDefaultLegacy(FeelEditor);

var ArrowIcon = function ArrowIcon(props) {
  return jsxRuntime.jsx("svg", {
    ...props,
    children: jsxRuntime.jsx("path", {
      fillRule: "evenodd",
      d: "m11.657 8-4.95 4.95a1 1 0 0 1-1.414-1.414L8.828 8 5.293 4.464A1 1 0 1 1 6.707 3.05L11.657 8Z"
    })
  });
};
ArrowIcon.defaultProps = {
  xmlns: "http://www.w3.org/2000/svg",
  width: "16",
  height: "16"
};
var CreateIcon = function CreateIcon(props) {
  return jsxRuntime.jsx("svg", {
    ...props,
    children: jsxRuntime.jsx("path", {
      fillRule: "evenodd",
      d: "M9 13V9h4a1 1 0 0 0 0-2H9V3a1 1 0 1 0-2 0v4H3a1 1 0 1 0 0 2h4v4a1 1 0 0 0 2 0Z"
    })
  });
};
CreateIcon.defaultProps = {
  xmlns: "http://www.w3.org/2000/svg",
  width: "16",
  height: "16"
};
var DeleteIcon = function DeleteIcon(props) {
  return jsxRuntime.jsx("svg", {
    ...props,
    children: jsxRuntime.jsx("path", {
      fillRule: "evenodd",
      d: "M12 6v7c0 1.1-.4 1.55-1.5 1.55h-5C4.4 14.55 4 14.1 4 13V6h8Zm-1.5 1.5h-5v4.3c0 .66.5 1.2 1.111 1.2H9.39c.611 0 1.111-.54 1.111-1.2V7.5ZM13 3h-2l-1-1H6L5 3H3v1.5h10V3Z"
    })
  });
};
DeleteIcon.defaultProps = {
  xmlns: "http://www.w3.org/2000/svg",
  width: "16",
  height: "16"
};
var ExternalLinkIcon = function ExternalLinkIcon(props) {
  return jsxRuntime.jsx("svg", {
    ...props,
    children: jsxRuntime.jsx("path", {
      fillRule: "evenodd",
      clipRule: "evenodd",
      d: "M12.637 12.637v-4.72h1.362v4.721c0 .36-.137.676-.411.95-.275.275-.591.412-.95.412H3.362c-.38 0-.703-.132-.967-.396A1.315 1.315 0 0 1 2 12.638V3.362c0-.38.132-.703.396-.967S2.982 2 3.363 2h4.553v1.363H3.363v9.274h9.274ZM14 2H9.28l-.001 1.362h2.408L5.065 9.984l.95.95 6.622-6.622v2.409H14V2Z",
      fill: "#818798"
    })
  });
};
ExternalLinkIcon.defaultProps = {
  width: "16",
  height: "16",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};
var FeelRequiredIcon = function FeelRequiredIcon(props) {
  return jsxRuntime.jsxs("svg", {
    ...props,
    children: [jsxRuntime.jsx("path", {
      d: "M5.8 7.06V5.95h4.307v1.11H5.8Zm0 3.071v-1.11h4.307v1.11H5.8Z",
      fill: "currentColor"
    }), jsxRuntime.jsx("path", {
      fillRule: "evenodd",
      clipRule: "evenodd",
      d: "M8 3.268A4.732 4.732 0 1 0 12.732 8H14a6 6 0 1 1-6-6v1.268Z",
      fill: "currentColor"
    }), jsxRuntime.jsx("path", {
      d: "m11.28 6.072-.832-.56 1.016-1.224L10 3.848l.312-.912 1.392.584L11.632 2h1.032l-.072 1.52 1.392-.584.312.912-1.464.44 1.008 1.224-.832.552-.864-1.296-.864 1.304Z",
      fill: "currentColor"
    })]
  });
};
FeelRequiredIcon.defaultProps = {
  viewBox: "0 0 16 16",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};
var FeelOptionalIcon = function FeelOptionalIcon(props) {
  return jsxRuntime.jsxs("svg", {
    ...props,
    children: [jsxRuntime.jsx("path", {
      d: "M5.845 7.04V5.93h4.307v1.11H5.845Zm0 3.07V9h4.307v1.11H5.845Z",
      fill: "currentColor"
    }), jsxRuntime.jsx("path", {
      fillRule: "evenodd",
      clipRule: "evenodd",
      d: "M3.286 8a4.714 4.714 0 1 0 9.428 0 4.714 4.714 0 0 0-9.428 0ZM8 2a6 6 0 1 0 0 12A6 6 0 0 0 8 2Z",
      fill: "currentColor"
    })]
  });
};
FeelOptionalIcon.defaultProps = {
  viewBox: "0 0 16 16",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};

function Header(props) {
  const {
    element,
    headerProvider
  } = props;
  const {
    getElementIcon,
    getDocumentationRef,
    getElementLabel,
    getTypeLabel
  } = headerProvider;
  const label = getElementLabel(element);
  const type = getTypeLabel(element);
  const documentationRef = getDocumentationRef && getDocumentationRef(element);
  const ElementIcon = getElementIcon(element);
  return jsxRuntime.jsxs("div", {
    class: "bio-properties-panel-header",
    children: [jsxRuntime.jsx("div", {
      class: "bio-properties-panel-header-icon",
      children: ElementIcon && jsxRuntime.jsx(ElementIcon, {
        width: "32",
        height: "32",
        viewBox: "0 0 32 32"
      })
    }), jsxRuntime.jsxs("div", {
      class: "bio-properties-panel-header-labels",
      children: [jsxRuntime.jsx("div", {
        title: type,
        class: "bio-properties-panel-header-type",
        children: type
      }), label ? jsxRuntime.jsx("div", {
        title: label,
        class: "bio-properties-panel-header-label",
        children: label
      }) : null]
    }), jsxRuntime.jsx("div", {
      class: "bio-properties-panel-header-actions",
      children: documentationRef ? jsxRuntime.jsx("a", {
        rel: "noopener",
        class: "bio-properties-panel-header-link",
        href: documentationRef,
        title: "Open documentation",
        target: "_blank",
        children: jsxRuntime.jsx(ExternalLinkIcon, {})
      }) : null
    })]
  });
}

const DescriptionContext = preact.createContext({
  description: {},
  getDescriptionForId: () => {}
});

const ErrorsContext = preact.createContext({
  errors: {}
});

/**
 * @typedef {Function} <propertiesPanel.showEntry> callback
 *
 * @example
 *
 * useEvent('propertiesPanel.showEntry', ({ focus = false, ...rest }) => {
 *   // ...
 * });
 *
 * @param {Object} context
 * @param {boolean} [context.focus]
 *
 * @returns void
 */
const EventContext = preact.createContext({
  eventBus: null
});

const LayoutContext = preact.createContext({
  layout: {},
  setLayout: () => {},
  getLayoutForKey: () => {},
  setLayoutForKey: () => {}
});

/**
 * Accesses the global DescriptionContext and returns a description for a given id and element.
 *
 * @example
 * ```jsx
 * function TextField(props) {
 *   const description = useDescriptionContext('input1', element);
 * }
 * ```
 *
 * @param {string} id
 * @param {object} element
 *
 * @returns {string}
 */
function useDescriptionContext(id, element) {
  const {
    getDescriptionForId
  } = hooks.useContext(DescriptionContext);
  return getDescriptionForId(id, element);
}

function useError(id) {
  const {
    errors
  } = hooks.useContext(ErrorsContext);
  return errors[id];
}

/**
 * Subscribe to an event immediately. Update subscription after inputs changed.
 *
 * @param {string} event
 * @param {Function} callback
 */
function useEvent(event, callback, eventBus) {
  const eventContext = hooks.useContext(EventContext);
  if (!eventBus) {
    ({
      eventBus
    } = eventContext);
  }
  const didMount = hooks.useRef(false);

  // (1) subscribe immediately
  if (eventBus && !didMount.current) {
    eventBus.on(event, callback);
  }

  // (2) update subscription after inputs changed
  hooks.useEffect(() => {
    if (eventBus && didMount.current) {
      eventBus.on(event, callback);
    }
    didMount.current = true;
    return () => {
      if (eventBus) {
        eventBus.off(event, callback);
      }
    };
  }, [callback, event, eventBus]);
}

const KEY_LENGTH = 6;

/**
 * Create a persistent key factory for plain objects without id.
 *
 * @example
 * ```jsx
 * function List({ objects }) {
 *   const getKey = useKeyFactory();
 *   return (<ol>{
 *     objects.map(obj => {
 *       const key = getKey(obj);
 *       return <li key={key}>obj.name</li>
 *     })
 *   }</ol>);
 * }
 * ```
 *
 * @param {any[]} dependencies
 * @returns {(element: object) => string}
 */
function useKeyFactory(dependencies = []) {
  const map = hooks.useMemo(() => new Map(), dependencies);
  const getKey = el => {
    let key = map.get(el);
    if (!key) {
      key = Math.random().toString().slice(-KEY_LENGTH);
      map.set(el, key);
    }
    return key;
  };
  return getKey;
}

/**
 * Creates a state that persists in the global LayoutContext.
 *
 * @example
 * ```jsx
 * function Group(props) {
 *   const [ open, setOpen ] = useLayoutState([ 'groups', 'foo', 'open' ], false);
 * }
 * ```
 *
 * @param {(string|number)[]} path
 * @param {any} [defaultValue]
 *
 * @returns {[ any, Function ]}
 */
function useLayoutState(path, defaultValue) {
  const {
    getLayoutForKey,
    setLayoutForKey
  } = hooks.useContext(LayoutContext);
  const layoutForKey = getLayoutForKey(path, defaultValue);
  const setState = hooks.useCallback(newValue => {
    setLayoutForKey(path, newValue);
  }, [setLayoutForKey]);
  return [layoutForKey, setState];
}

/**
 * @pinussilvestrus: we need to introduce our own hook to persist the previous
 * state on updates.
 *
 * cf. https://reactjs.org/docs/hooks-faq.html#how-to-get-the-previous-props-or-state
 */

function usePrevious(value) {
  const ref = hooks.useRef();
  hooks.useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

/**
 * Subscribe to `propertiesPanel.showEntry`.
 *
 * @param {string} id
 *
 * @returns {import('preact').Ref}
 */
function useShowEntryEvent(id) {
  const {
    onShow
  } = hooks.useContext(LayoutContext);
  const ref = hooks.useRef();
  const focus = hooks.useRef(false);
  const onShowEntry = hooks.useCallback(event => {
    if (event.id === id) {
      onShow();
      if (!focus.current) {
        focus.current = true;
      }
    }
  }, [id]);
  hooks.useEffect(() => {
    if (focus.current && ref.current) {
      if (minDash.isFunction(ref.current.focus)) {
        ref.current.focus();
      }
      if (minDash.isFunction(ref.current.select)) {
        ref.current.select();
      }
      focus.current = false;
    }
  });
  useEvent('propertiesPanel.showEntry', onShowEntry);
  return ref;
}

/**
 * @callback setSticky
 * @param {boolean} value
 */

/**
 * Use IntersectionObserver to identify when DOM element is in sticky mode.
 * If sticky is observered setSticky(true) will be called.
 * If sticky mode is left, setSticky(false) will be called.
 *
 *
 * @param {Object} ref
 * @param {string} scrollContainerSelector
 * @param {setSticky} setSticky
 */
function useStickyIntersectionObserver(ref, scrollContainerSelector, setSticky) {
  const [scrollContainer, setScrollContainer] = hooks.useState(minDom.query(scrollContainerSelector));
  const updateScrollContainer = hooks.useCallback(() => {
    const newScrollContainer = minDom.query(scrollContainerSelector);
    if (newScrollContainer !== scrollContainer) {
      setScrollContainer(newScrollContainer);
    }
  }, [scrollContainerSelector, scrollContainer]);
  hooks.useEffect(() => {
    updateScrollContainer();
  }, [updateScrollContainer]);
  useEvent('propertiesPanel.attach', updateScrollContainer);
  useEvent('propertiesPanel.detach', updateScrollContainer);
  hooks.useEffect(() => {
    const Observer = IntersectionObserver;

    // return early if IntersectionObserver is not available
    if (!Observer) {
      return;
    }

    // TODO(@barmac): test this
    if (!ref.current || !scrollContainer) {
      return;
    }
    const observer = new Observer(entries => {
      // scroll container is unmounted, do not update sticky state
      if (scrollContainer.scrollHeight === 0) {
        return;
      }
      entries.forEach(entry => {
        if (entry.intersectionRatio < 1) {
          setSticky(true);
        } else if (entry.intersectionRatio === 1) {
          setSticky(false);
        }
      });
    }, {
      root: scrollContainer,
      rootMargin: '0px 0px 999999% 0px',
      // Use bottom margin to avoid stickyness when scrolling out to bottom
      threshold: [1]
    });
    observer.observe(ref.current);

    // Unobserve if unmounted
    return () => {
      observer.unobserve(ref.current);
    };
  }, [ref.current, scrollContainer, setSticky]);
}

/**
 * Creates a static function reference with changing body.
 * This is necessary when external libraries require a callback function
 * that has references to state variables.
 *
 * Usage:
 * const callback = useStaticCallback((val) => {val === currentState});
 *
 * The `callback` reference is static and can be safely used in external
 * libraries or as a prop that does not cause rerendering of children.
 *
 * @param {Function} callback function with changing reference
 * @returns {Function} static function reference
 */
function useStaticCallback(callback) {
  const callbackRef = hooks.useRef(callback);
  callbackRef.current = callback;
  return hooks.useCallback((...args) => callbackRef.current(...args), []);
}

function Group(props) {
  const {
    element,
    entries = [],
    id,
    label,
    shouldOpen = false
  } = props;
  const groupRef = hooks.useRef(null);
  const [open, setOpen] = useLayoutState(['groups', id, 'open'], shouldOpen);
  const onShow = hooks.useCallback(() => setOpen(true), [setOpen]);
  const toggleOpen = () => setOpen(!open);
  const [edited, setEdited] = hooks.useState(false);
  const [sticky, setSticky] = hooks.useState(false);

  // set edited state depending on all entries
  hooks.useEffect(() => {
    const hasOneEditedEntry = entries.find(entry => {
      const {
        id,
        isEdited
      } = entry;
      const entryNode = minDom.query(`[data-entry-id="${id}"]`);
      if (!minDash.isFunction(isEdited) || !entryNode) {
        return false;
      }
      const inputNode = minDom.query('.bio-properties-panel-input', entryNode);
      return isEdited(inputNode);
    });
    setEdited(hasOneEditedEntry);
  }, [entries]);

  // set css class when group is sticky to top
  useStickyIntersectionObserver(groupRef, 'div.bio-properties-panel-scroll-container', setSticky);
  const propertiesPanelContext = {
    ...hooks.useContext(LayoutContext),
    onShow
  };
  return jsxRuntime.jsxs("div", {
    class: "bio-properties-panel-group",
    "data-group-id": 'group-' + id,
    ref: groupRef,
    children: [jsxRuntime.jsxs("div", {
      class: classnames__default["default"]('bio-properties-panel-group-header', edited ? '' : 'empty', open ? 'open' : '', sticky && open ? 'sticky' : ''),
      onClick: toggleOpen,
      children: [jsxRuntime.jsx("div", {
        title: label,
        class: "bio-properties-panel-group-header-title",
        children: label
      }), jsxRuntime.jsxs("div", {
        class: "bio-properties-panel-group-header-buttons",
        children: [edited && jsxRuntime.jsx(DataMarker, {}), jsxRuntime.jsx("button", {
          title: "Toggle section",
          class: "bio-properties-panel-group-header-button bio-properties-panel-arrow",
          children: jsxRuntime.jsx(ArrowIcon, {
            class: open ? 'bio-properties-panel-arrow-down' : 'bio-properties-panel-arrow-right'
          })
        })]
      })]
    }), jsxRuntime.jsx("div", {
      class: classnames__default["default"]('bio-properties-panel-group-entries', open ? 'open' : ''),
      children: jsxRuntime.jsx(LayoutContext.Provider, {
        value: propertiesPanelContext,
        children: entries.map(entry => {
          const {
            component: Component,
            id
          } = entry;
          return preact.createElement(Component, {
            ...entry,
            element: element,
            key: id
          });
        })
      })
    })]
  });
}
function DataMarker() {
  return jsxRuntime.jsx("div", {
    title: "Section contains data",
    class: "bio-properties-panel-dot"
  });
}

/**
 * @typedef { {
 *  text: (element: object) => string,
 *  icon?: (element: Object) => import('preact').Component
 * } } PlaceholderDefinition
 *
 * @param { PlaceholderDefinition } props
 */
function Placeholder(props) {
  const {
    text,
    icon: Icon
  } = props;
  return jsxRuntime.jsx("div", {
    class: "bio-properties-panel open",
    children: jsxRuntime.jsxs("section", {
      class: "bio-properties-panel-placeholder",
      children: [Icon && jsxRuntime.jsx(Icon, {
        class: "bio-properties-panel-placeholder-icon"
      }), jsxRuntime.jsx("p", {
        class: "bio-properties-panel-placeholder-text",
        children: text
      })]
    })
  });
}

const DEFAULT_LAYOUT = {};
const DEFAULT_DESCRIPTION = {};

/**
 * @typedef { {
 *    component: import('preact').Component,
 *    id: String,
 *    isEdited?: Function
 * } } EntryDefinition
 *
 * @typedef { {
 *    autoFocusEntry: String,
 *    autoOpen?: Boolean,
 *    entries: Array<EntryDefinition>,
 *    id: String,
 *    label: String,
 *    remove: (event: MouseEvent) => void
 * } } ListItemDefinition
 *
 * @typedef { {
 *    add: (event: MouseEvent) => void,
 *    component: import('preact').Component,
 *    element: Object,
 *    id: String,
 *    items: Array<ListItemDefinition>,
 *    label: String,
 *    shouldSort?: Boolean,
 *    shouldOpen?: Boolean
 * } } ListGroupDefinition
 *
 * @typedef { {
 *    component?: import('preact').Component,
 *    entries: Array<EntryDefinition>,
 *    id: String,
 *    label: String,
 *    shouldOpen?: Boolean
 * } } GroupDefinition
 *
 *  @typedef { {
 *    [id: String]: GetDescriptionFunction
 * } } DescriptionConfig
 *
 * @callback { {
 * @param {string} id
 * @param {Object} element
 * @returns {string}
 * } } GetDescriptionFunction
 *
 * @typedef { {
 *  getEmpty: (element: object) => import('./components/Placeholder').PlaceholderDefinition,
 *  getMultiple: (element: Object) => import('./components/Placeholder').PlaceholderDefinition
 * } } PlaceholderProvider
 *
 */

/**
 * A basic properties panel component. Describes *how* content will be rendered, accepts
 * data from implementor to describe *what* will be rendered.
 *
 * @param {Object} props
 * @param {Object|Array} props.element
 * @param {import('./components/Header').HeaderProvider} props.headerProvider
 * @param {PlaceholderProvider} [props.placeholderProvider]
 * @param {Array<GroupDefinition|ListGroupDefinition>} props.groups
 * @param {Object} [props.layoutConfig]
 * @param {Function} [props.layoutChanged]
 * @param {DescriptionConfig} [props.descriptionConfig]
 * @param {Function} [props.descriptionLoaded]
 * @param {Object} [props.eventBus]
 */
function PropertiesPanel(props) {
  const {
    element,
    headerProvider,
    placeholderProvider,
    groups,
    layoutConfig,
    layoutChanged,
    descriptionConfig,
    descriptionLoaded,
    eventBus
  } = props;

  // set-up layout context
  const [layout, setLayout] = hooks.useState(createLayout(layoutConfig));

  // react to external changes in the layout config
  useUpdateLayoutEffect(() => {
    const newLayout = createLayout(layoutConfig);
    setLayout(newLayout);
  }, [layoutConfig]);
  hooks.useEffect(() => {
    if (typeof layoutChanged === 'function') {
      layoutChanged(layout);
    }
  }, [layout, layoutChanged]);
  const getLayoutForKey = (key, defaultValue) => {
    return minDash.get(layout, key, defaultValue);
  };
  const setLayoutForKey = (key, config) => {
    const newLayout = minDash.assign({}, layout);
    minDash.set(newLayout, key, config);
    setLayout(newLayout);
  };
  const layoutContext = {
    layout,
    setLayout,
    getLayoutForKey,
    setLayoutForKey
  };

  // set-up description context
  const description = hooks.useMemo(() => createDescriptionContext(descriptionConfig), [descriptionConfig]);
  hooks.useEffect(() => {
    if (typeof descriptionLoaded === 'function') {
      descriptionLoaded(description);
    }
  }, [description, descriptionLoaded]);
  const getDescriptionForId = (id, element) => {
    return description[id] && description[id](element);
  };
  const descriptionContext = {
    description,
    getDescriptionForId
  };
  const [errors, setErrors] = hooks.useState({});
  const onSetErrors = ({
    errors
  }) => setErrors(errors);
  useEvent('propertiesPanel.setErrors', onSetErrors, eventBus);
  const errorsContext = {
    errors
  };
  const eventContext = {
    eventBus
  };
  const propertiesPanelContext = {
    element
  };

  // empty state
  if (placeholderProvider && !element) {
    return jsxRuntime.jsx(Placeholder, {
      ...placeholderProvider.getEmpty()
    });
  }

  // multiple state
  if (placeholderProvider && minDash.isArray(element)) {
    return jsxRuntime.jsx(Placeholder, {
      ...placeholderProvider.getMultiple()
    });
  }
  return jsxRuntime.jsx(LayoutContext.Provider, {
    value: propertiesPanelContext,
    children: jsxRuntime.jsx(ErrorsContext.Provider, {
      value: errorsContext,
      children: jsxRuntime.jsx(DescriptionContext.Provider, {
        value: descriptionContext,
        children: jsxRuntime.jsx(LayoutContext.Provider, {
          value: layoutContext,
          children: jsxRuntime.jsx(EventContext.Provider, {
            value: eventContext,
            children: jsxRuntime.jsxs("div", {
              class: "bio-properties-panel",
              children: [jsxRuntime.jsx(Header, {
                element: element,
                headerProvider: headerProvider
              }), jsxRuntime.jsx("div", {
                class: "bio-properties-panel-scroll-container",
                children: groups.map(group => {
                  const {
                    component: Component = Group,
                    id
                  } = group;
                  return preact.createElement(Component, {
                    ...group,
                    key: id,
                    element: element
                  });
                })
              })]
            })
          })
        })
      })
    })
  });
}

// helpers //////////////////

function createLayout(overrides = {}, defaults = DEFAULT_LAYOUT) {
  return {
    ...defaults,
    ...overrides
  };
}
function createDescriptionContext(overrides = {}) {
  return {
    ...DEFAULT_DESCRIPTION,
    ...overrides
  };
}

// hooks //////////////////

/**
 * This hook behaves like useLayoutEffect, but does not trigger on the first render.
 *
 * @param {Function} effect
 * @param {Array} deps
 */
function useUpdateLayoutEffect(effect, deps) {
  const isMounted = hooks.useRef(false);
  hooks.useLayoutEffect(() => {
    if (isMounted.current) {
      return effect();
    } else {
      isMounted.current = true;
    }
  }, deps);
}

function DropdownButton(props) {
  const {
    class: className,
    children,
    menuItems = []
  } = props;
  const dropdownRef = hooks.useRef(null);
  const menuRef = hooks.useRef(null);
  const [open, setOpen] = hooks.useState(false);
  const close = () => setOpen(false);
  function onDropdownToggle(event) {
    if (menuRef.current && menuRef.current.contains(event.target)) {
      return;
    }
    event.stopPropagation();
    setOpen(open => !open);
  }
  function onActionClick(event, action) {
    event.stopPropagation();
    close();
    action();
  }
  useGlobalClick([dropdownRef.current], () => close());
  return jsxRuntime.jsxs("div", {
    class: classnames__default["default"]('bio-properties-panel-dropdown-button', {
      open
    }, className),
    onClick: onDropdownToggle,
    ref: dropdownRef,
    children: [children, jsxRuntime.jsx("div", {
      class: "bio-properties-panel-dropdown-button__menu",
      ref: menuRef,
      children: menuItems.map((item, index) => jsxRuntime.jsx(MenuItem, {
        onClick: onActionClick,
        item: item
      }, index))
    })]
  });
}
function MenuItem({
  item,
  onClick
}) {
  if (item.separator) {
    return jsxRuntime.jsx("div", {
      class: "bio-properties-panel-dropdown-button__menu-item bio-properties-panel-dropdown-button__menu-item--separator"
    });
  }
  if (item.action) {
    return jsxRuntime.jsx("button", {
      class: "bio-properties-panel-dropdown-button__menu-item bio-properties-panel-dropdown-button__menu-item--actionable",
      onClick: event => onClick(event, item.action),
      children: item.entry
    });
  }
  return jsxRuntime.jsx("div", {
    class: "bio-properties-panel-dropdown-button__menu-item",
    children: item.entry
  });
}

/**
 *
 * @param {Array<null | Element>} ignoredElements
 * @param {Function} callback
 */
function useGlobalClick(ignoredElements, callback) {
  hooks.useEffect(() => {
    /**
     * @param {MouseEvent} event
     */
    function listener(event) {
      if (ignoredElements.some(element => element && element.contains(event.target))) {
        return;
      }
      callback();
    }
    document.addEventListener('click', listener, {
      capture: true
    });
    return () => document.removeEventListener('click', listener, {
      capture: true
    });
  }, [...ignoredElements, callback]);
}

function HeaderButton(props) {
  const {
    children = null,
    class: classname,
    onClick = () => {},
    ...otherProps
  } = props;
  return jsxRuntime.jsx("button", {
    ...otherProps,
    onClick: onClick,
    class: classnames__default["default"]('bio-properties-panel-group-header-button', classname),
    children: children
  });
}

function CollapsibleEntry(props) {
  const {
    element,
    entries = [],
    id,
    label,
    open: shouldOpen,
    remove
  } = props;
  const [open, setOpen] = hooks.useState(shouldOpen);
  const toggleOpen = () => setOpen(!open);
  const {
    onShow
  } = hooks.useContext(LayoutContext);
  const propertiesPanelContext = {
    ...hooks.useContext(LayoutContext),
    onShow: hooks.useCallback(() => {
      setOpen(true);
      if (minDash.isFunction(onShow)) {
        onShow();
      }
    }, [onShow, setOpen])
  };

  // todo(pinussilvestrus): translate once we have a translate mechanism for the core
  const placeholderLabel = '<empty>';
  return jsxRuntime.jsxs("div", {
    "data-entry-id": id,
    class: classnames__default["default"]('bio-properties-panel-collapsible-entry', open ? 'open' : ''),
    children: [jsxRuntime.jsxs("div", {
      class: "bio-properties-panel-collapsible-entry-header",
      onClick: toggleOpen,
      children: [jsxRuntime.jsx("div", {
        title: label || placeholderLabel,
        class: classnames__default["default"]('bio-properties-panel-collapsible-entry-header-title', !label && 'empty'),
        children: label || placeholderLabel
      }), jsxRuntime.jsx("button", {
        title: "Toggle list item",
        class: "bio-properties-panel-arrow  bio-properties-panel-collapsible-entry-arrow",
        children: jsxRuntime.jsx(ArrowIcon, {
          class: open ? 'bio-properties-panel-arrow-down' : 'bio-properties-panel-arrow-right'
        })
      }), remove ? jsxRuntime.jsx("button", {
        title: "Delete item",
        class: "bio-properties-panel-remove-entry",
        onClick: remove,
        children: jsxRuntime.jsx(DeleteIcon, {})
      }) : null]
    }), jsxRuntime.jsx("div", {
      class: classnames__default["default"]('bio-properties-panel-collapsible-entry-entries', open ? 'open' : ''),
      children: jsxRuntime.jsx(LayoutContext.Provider, {
        value: propertiesPanelContext,
        children: entries.map(entry => {
          const {
            component: Component,
            id
          } = entry;
          return preact.createElement(Component, {
            ...entry,
            element: element,
            key: id
          });
        })
      })
    })]
  });
}

function ListItem(props) {
  const {
    autoFocusEntry,
    autoOpen
  } = props;

  // focus specified entry on auto open
  hooks.useEffect(() => {
    if (autoOpen && autoFocusEntry) {
      const entry = minDom.query(`[data-entry-id="${autoFocusEntry}"]`);
      const focusableInput = minDom.query('.bio-properties-panel-input', entry);
      if (focusableInput) {
        if (minDash.isFunction(focusableInput.select)) {
          focusableInput.select();
        } else if (minDash.isFunction(focusableInput.focus)) {
          focusableInput.focus();
        }
      }
    }
  }, [autoOpen, autoFocusEntry]);
  return jsxRuntime.jsx("div", {
    class: "bio-properties-panel-list-item",
    children: jsxRuntime.jsx(CollapsibleEntry, {
      ...props,
      open: autoOpen
    })
  });
}

const noop$3 = () => {};

/**
 * @param {import('../PropertiesPanel').ListGroupDefinition} props
 */
function ListGroup(props) {
  const {
    add,
    element,
    id,
    items,
    label,
    shouldOpen = true,
    shouldSort = true,
    autoOpenItem = true
  } = props;
  const groupRef = hooks.useRef(null);
  const [open, setOpen] = useLayoutState(['groups', id, 'open'], shouldOpen);
  const [sticky, setSticky] = hooks.useState(false);
  const onShow = hooks.useCallback(() => setOpen(true), [setOpen]);
  const [ordering, setOrdering] = hooks.useState([]);
  const [newItemAdded, setNewItemAdded] = hooks.useState(false);

  // Flag to mark that add button was clicked in the last render cycle
  const [addTriggered, setAddTriggered] = hooks.useState(false);
  const prevItems = usePrevious(items);
  const prevElement = usePrevious(element);
  const elementChanged = element !== prevElement;
  const shouldHandleEffects = !elementChanged && (shouldSort || shouldOpen);

  // reset initial ordering when element changes (before first render)
  if (elementChanged) {
    setOrdering(createOrdering(shouldSort ? sortItems(items) : items));
  }

  // keep ordering in sync to items - and open changes

  // (0) set initial ordering from given items
  hooks.useEffect(() => {
    if (!prevItems || !shouldSort) {
      setOrdering(createOrdering(items));
    }
  }, [items, element]);

  // (1) items were added
  hooks.useEffect(() => {
    // reset addTriggered flag
    setAddTriggered(false);
    if (shouldHandleEffects && prevItems && items.length > prevItems.length) {
      let add = [];
      items.forEach(item => {
        if (!ordering.includes(item.id)) {
          add.push(item.id);
        }
      });
      let newOrdering = ordering;

      // open if not open, configured and triggered by add button
      //
      // TODO(marstamm): remove once we refactor layout handling for listGroups.
      // Ideally, opening should be handled as part of the `add` callback and
      // not be a concern for the ListGroup component.
      if (addTriggered && !open && shouldOpen) {
        toggleOpen();
      }

      // filter when not open and configured
      if (!open && shouldSort) {
        newOrdering = createOrdering(sortItems(items));
      }

      // add new items on top or bottom depending on sorting behavior
      newOrdering = newOrdering.filter(item => !add.includes(item));
      if (shouldSort) {
        newOrdering.unshift(...add);
      } else {
        newOrdering.push(...add);
      }
      setOrdering(newOrdering);
      setNewItemAdded(addTriggered);
    } else {
      setNewItemAdded(false);
    }
  }, [items, open, shouldHandleEffects, addTriggered]);

  // (2) sort items on open if shouldSort is set
  hooks.useEffect(() => {
    if (shouldSort && open && !newItemAdded) {
      setOrdering(createOrdering(sortItems(items)));
    }
  }, [open, shouldSort]);

  // (3) items were deleted
  hooks.useEffect(() => {
    if (shouldHandleEffects && prevItems && items.length < prevItems.length) {
      let keep = [];
      ordering.forEach(o => {
        if (getItem(items, o)) {
          keep.push(o);
        }
      });
      setOrdering(keep);
    }
  }, [items, shouldHandleEffects]);

  // set css class when group is sticky to top
  useStickyIntersectionObserver(groupRef, 'div.bio-properties-panel-scroll-container', setSticky);
  const toggleOpen = () => setOpen(!open);
  const hasItems = !!items.length;
  const propertiesPanelContext = {
    ...hooks.useContext(LayoutContext),
    onShow
  };
  const handleAddClick = e => {
    setAddTriggered(true);
    add(e);
  };
  return jsxRuntime.jsxs("div", {
    class: "bio-properties-panel-group",
    "data-group-id": 'group-' + id,
    ref: groupRef,
    children: [jsxRuntime.jsxs("div", {
      class: classnames__default["default"]('bio-properties-panel-group-header', hasItems ? '' : 'empty', hasItems && open ? 'open' : '', sticky && open ? 'sticky' : ''),
      onClick: hasItems ? toggleOpen : noop$3,
      children: [jsxRuntime.jsx("div", {
        title: label,
        class: "bio-properties-panel-group-header-title",
        children: label
      }), jsxRuntime.jsxs("div", {
        class: "bio-properties-panel-group-header-buttons",
        children: [add ? jsxRuntime.jsxs("button", {
          title: "Create new list item",
          class: "bio-properties-panel-group-header-button bio-properties-panel-add-entry",
          onClick: handleAddClick,
          children: [jsxRuntime.jsx(CreateIcon, {}), !hasItems ? jsxRuntime.jsx("span", {
            class: "bio-properties-panel-add-entry-label",
            children: "Create"
          }) : null]
        }) : null, hasItems ? jsxRuntime.jsx("div", {
          title: `List contains ${items.length} item${items.length != 1 ? 's' : ''}`,
          class: "bio-properties-panel-list-badge",
          children: items.length
        }) : null, hasItems ? jsxRuntime.jsx("button", {
          title: "Toggle section",
          class: "bio-properties-panel-group-header-button bio-properties-panel-arrow",
          children: jsxRuntime.jsx(ArrowIcon, {
            class: open ? 'bio-properties-panel-arrow-down' : 'bio-properties-panel-arrow-right'
          })
        }) : null]
      })]
    }), jsxRuntime.jsx("div", {
      class: classnames__default["default"]('bio-properties-panel-list', open && hasItems ? 'open' : ''),
      children: jsxRuntime.jsx(LayoutContext.Provider, {
        value: propertiesPanelContext,
        children: ordering.map((o, index) => {
          const item = getItem(items, o);
          if (!item) {
            return;
          }
          const {
            id
          } = item;

          // if item was added, open it
          // Existing items will not be affected as autoOpen is only applied on first render
          let autoOpen = newItemAdded;
          if (!autoOpen) {
            autoOpen = autoOpenItem;
          }
          return preact.createElement(ListItem, {
            ...item,
            autoOpen: autoOpen,
            element: element,
            index: index,
            key: id
          });
        })
      })
    })]
  });
}

// helpers ////////////////////

/**
 * Sorts given items alphanumeric by label
 */
function sortItems(items) {
  return minDash.sortBy(items, i => i.label.toLowerCase());
}
function getItem(items, id) {
  return minDash.find(items, i => i.id === id);
}
function createOrdering(items) {
  return items.map(i => i.id);
}

function Description(props) {
  const {
    element,
    forId,
    value
  } = props;
  const contextDescription = useDescriptionContext(forId, element);
  const description = value || contextDescription;
  if (description) {
    return jsxRuntime.jsx("div", {
      class: "bio-properties-panel-description",
      children: description
    });
  }
}

function Checkbox(props) {
  const {
    id,
    label,
    onChange,
    disabled,
    value = false,
    onFocus,
    onBlur
  } = props;
  const [localValue, setLocalValue] = hooks.useState(value);
  const handleChangeCallback = ({
    target
  }) => {
    onChange(target.checked);
  };
  const handleChange = e => {
    handleChangeCallback(e);
    setLocalValue(e.target.value);
  };
  hooks.useEffect(() => {
    if (value === localValue) {
      return;
    }
    setLocalValue(value);
  }, [value]);
  const ref = useShowEntryEvent(id);
  return jsxRuntime.jsxs("div", {
    class: "bio-properties-panel-checkbox",
    children: [jsxRuntime.jsx("input", {
      ref: ref,
      id: prefixId$7(id),
      name: id,
      onFocus: onFocus,
      onBlur: onBlur,
      type: "checkbox",
      class: "bio-properties-panel-input",
      onChange: handleChange,
      checked: localValue,
      disabled: disabled
    }), jsxRuntime.jsx("label", {
      for: prefixId$7(id),
      class: "bio-properties-panel-label",
      children: label
    })]
  });
}

/**
 * @param {Object} props
 * @param {Object} props.element
 * @param {String} props.id
 * @param {String} props.description
 * @param {String} props.label
 * @param {Function} props.getValue
 * @param {Function} props.setValue
 * @param {Function} props.onFocus
 * @param {Function} props.onBlur
 * @param {boolean} [props.disabled]
 */
function CheckboxEntry(props) {
  const {
    element,
    id,
    description,
    label,
    getValue,
    setValue,
    disabled,
    onFocus,
    onBlur,
    defaultValue
  } = props;
  const value = getValue(element);
  const error = useError(id);
  return jsxRuntime.jsxs("div", {
    class: "bio-properties-panel-entry bio-properties-panel-checkbox-entry",
    "data-entry-id": id,
    children: [jsxRuntime.jsx(Checkbox, {
      disabled: disabled,
      id: id,
      label: label,
      onChange: setValue,
      onFocus: onFocus,
      onBlur: onBlur,
      checked: value || value == 'true' || defaultValue,
      value: value
    }, element), error && jsxRuntime.jsx("div", {
      class: "bio-properties-panel-error",
      children: error
    }), jsxRuntime.jsx(Description, {
      forId: id,
      element: element,
      value: description
    })]
  });
}
function isEdited$9(node) {
  return node && !!node.checked;
}

// helpers /////////////////

function prefixId$7(id) {
  return `bio-properties-panel-${id}`;
}

const useBufferedFocus$1 = function (editor, ref) {
  const [buffer, setBuffer] = hooks.useState(undefined);
  ref.current = hooks.useMemo(() => ({
    focus: offset => {
      if (editor) {
        editor.focus(offset);
      } else {
        if (typeof offset === 'undefined') {
          offset = Infinity;
        }
        setBuffer(offset);
      }
    }
  }), [editor]);
  hooks.useEffect(() => {
    if (typeof buffer !== 'undefined' && editor) {
      editor.focus(buffer);
      setBuffer(false);
    }
  }, [editor, buffer]);
};
const CodeEditor$1 = compat.forwardRef((props, ref) => {
  const {
    onInput,
    disabled,
    tooltipContainer,
    enableGutters,
    value,
    onLint = () => {},
    contentAttributes = {},
    hostLanguage = null,
    singleLine = false
  } = props;
  const inputRef = hooks.useRef();
  const [editor, setEditor] = hooks.useState();
  const [localValue, setLocalValue] = hooks.useState(value || '');
  useBufferedFocus$1(editor, ref);
  const handleInput = useStaticCallback(newValue => {
    onInput(newValue);
    setLocalValue(newValue);
  });
  hooks.useEffect(() => {
    let editor;
    editor = new feelers.FeelersEditor({
      container: inputRef.current,
      onChange: handleInput,
      value: localValue,
      onLint,
      contentAttributes,
      tooltipContainer,
      enableGutters,
      hostLanguage,
      singleLine
    });
    setEditor(editor);
    return () => {
      onLint([]);
      inputRef.current.innerHTML = '';
      setEditor(null);
    };
  }, []);
  hooks.useEffect(() => {
    if (!editor) {
      return;
    }
    if (value === localValue) {
      return;
    }
    editor.setValue(value);
    setLocalValue(value);
  }, [value]);
  const handleClick = () => {
    ref.current.focus();
  };
  return jsxRuntime.jsx("div", {
    name: props.name,
    class: classnames__default["default"]('bio-properties-panel-feelers-editor bio-properties-panel-input', localValue ? 'edited' : null, disabled ? 'disabled' : null),
    ref: inputRef,
    onClick: handleClick
  });
});

const useBufferedFocus = function (editor, ref) {
  const [buffer, setBuffer] = hooks.useState(undefined);
  ref.current = hooks.useMemo(() => ({
    focus: offset => {
      if (editor) {
        editor.focus(offset);
      } else {
        if (typeof offset === 'undefined') {
          offset = Infinity;
        }
        setBuffer(offset);
      }
    }
  }), [editor]);
  hooks.useEffect(() => {
    if (typeof buffer !== 'undefined' && editor) {
      editor.focus(buffer);
      setBuffer(false);
    }
  }, [editor, buffer]);
};
const CodeEditor = compat.forwardRef((props, ref) => {
  const {
    value,
    onInput,
    onFeelToggle,
    onLint = () => {},
    disabled,
    tooltipContainer,
    variables
  } = props;
  const inputRef = hooks.useRef();
  const [editor, setEditor] = hooks.useState();
  const [localValue, setLocalValue] = hooks.useState(value || '');
  useBufferedFocus(editor, ref);
  const handleInput = useStaticCallback(newValue => {
    onInput(newValue);
    setLocalValue(newValue);
  });
  hooks.useEffect(() => {
    let editor;

    /* Trigger FEEL toggle when
     *
     * - `backspace` is pressed
     * - AND the cursor is at the beginning of the input
     */
    const onKeyDown = e => {
      if (e.key !== 'Backspace' || !editor) {
        return;
      }
      const selection = editor.getSelection();
      const range = selection.ranges[selection.mainIndex];
      if (range.from === 0 && range.to === 0) {
        onFeelToggle();
      }
    };
    editor = new FeelEditor__default["default"]({
      container: inputRef.current,
      onChange: handleInput,
      onKeyDown: onKeyDown,
      onLint: onLint,
      tooltipContainer: tooltipContainer,
      value: localValue,
      variables: variables
    });
    setEditor(editor);
    return () => {
      onLint([]);
      inputRef.current.innerHTML = '';
      setEditor(null);
    };
  }, []);
  hooks.useEffect(() => {
    if (!editor) {
      return;
    }
    if (value === localValue) {
      return;
    }
    editor.setValue(value);
    setLocalValue(value);
  }, [value]);
  hooks.useEffect(() => {
    if (!editor) {
      return;
    }
    editor.setVariables(variables);
  }, [variables]);
  const handleClick = () => {
    ref.current.focus();
  };
  return jsxRuntime.jsx("div", {
    class: classnames__default["default"]('bio-properties-panel-feel-editor-container', disabled ? 'disabled' : null),
    children: jsxRuntime.jsx("div", {
      name: props.name,
      class: classnames__default["default"]('bio-properties-panel-input', localValue ? 'edited' : null),
      ref: inputRef,
      onClick: handleClick
    })
  });
});

function FeelIndicator(props) {
  const {
    active
  } = props;
  if (!active) {
    return null;
  }
  return jsxRuntime.jsx("span", {
    class: "bio-properties-panel-feel-indicator",
    children: "="
  });
}

const noop$2 = () => {};

/**
 * @param {Object} props
 * @param {Object} props.label
 * @param {String} props.feel
 */
function FeelIcon(props) {
  const {
    label,
    feel = false,
    active,
    disabled = false,
    onClick = noop$2
  } = props;
  const feelRequiredLabel = ' must be a FEEL expression';
  const feelOptionalLabel = ' can optionally be a FEEL expression';
  const handleClick = e => {
    onClick(e);

    // when pointer event was created from keyboard, keep focus on button
    if (!e.pointerType) {
      e.stopPropagation();
    }
  };
  return jsxRuntime.jsx("button", {
    class: classnames__default["default"]('bio-properties-panel-feel-icon', active ? 'active' : null, feel === 'required' ? 'required' : 'optional'),
    onClick: handleClick,
    disabled: feel === 'required' || disabled,
    title: label + (feel === 'required' ? feelRequiredLabel : feelOptionalLabel),
    children: feel === 'required' ? jsxRuntime.jsx(FeelRequiredIcon, {}) : jsxRuntime.jsx(FeelOptionalIcon, {})
  });
}

const noop$1 = () => {};
function FeelTextfield(props) {
  const {
    debounce,
    id,
    label,
    onInput,
    onError,
    feel,
    value = '',
    disabled = false,
    variables,
    tooltipContainer,
    OptionalComponent = OptionalFeelInput
  } = props;
  const [localValue, _setLocalValue] = hooks.useState(value);
  const editorRef = useShowEntryEvent(id);
  const containerRef = hooks.useRef();
  const feelActive = localValue.startsWith('=') || feel === 'required';
  const feelOnlyValue = localValue.startsWith('=') ? localValue.substring(1) : localValue;
  const [focus, _setFocus] = hooks.useState(undefined);
  const setFocus = (offset = 0) => {
    const hasFocus = containerRef.current.contains(document.activeElement);

    // Keep caret position if it is already focused, otherwise focus at the end
    const position = hasFocus ? document.activeElement.selectionStart : Infinity;
    _setFocus(position + offset);
  };
  const handleInputCallback = hooks.useMemo(() => {
    return debounce(newValue => {
      onInput(newValue);
    });
  }, [onInput, debounce]);
  const setLocalValue = newValue => {
    _setLocalValue(newValue);
    if (!newValue || newValue === '=') {
      handleInputCallback(undefined);
    } else {
      handleInputCallback(newValue);
    }
  };
  const handleFeelToggle = useStaticCallback(() => {
    if (feel === 'required') {
      return;
    }
    if (!feelActive) {
      setLocalValue('=' + localValue);
    } else {
      setLocalValue(feelOnlyValue);
    }
  });
  const handleLocalInput = newValue => {
    if (feelActive) {
      newValue = '=' + newValue;
    }
    if (newValue === localValue) {
      return;
    }
    setLocalValue(newValue);
    if (!feelActive && newValue.startsWith('=')) {
      // focus is behind `=` sign that will be removed
      setFocus(-1);
    }
  };
  const handleLint = useStaticCallback(lint => {
    if (!(lint && lint.length)) {
      onError(undefined);
      return;
    }
    const error = lint[0];
    const message = `${error.source}: ${error.message}`;
    onError(message);
  });
  hooks.useEffect(() => {
    if (typeof focus !== 'undefined') {
      editorRef.current.focus(focus);
      _setFocus(undefined);
    }
  }, [focus]);
  hooks.useEffect(() => {
    if (value === localValue) {
      return;
    }

    // External value change removed content => keep FEEL configuration
    if (!value) {
      setLocalValue(feelActive ? '=' : '');
      return;
    }
    setLocalValue(value);
  }, [value]);

  // copy-paste integration
  hooks.useEffect(() => {
    const copyHandler = event => {
      if (!feelActive) {
        return;
      }
      event.clipboardData.setData('application/FEEL', event.clipboardData.getData('text'));
    };
    const pasteHandler = event => {
      if (feelActive) {
        return;
      }
      const data = event.clipboardData.getData('application/FEEL');
      if (data) {
        setTimeout(() => {
          handleFeelToggle();
          setFocus();
        });
      }
    };
    containerRef.current.addEventListener('copy', copyHandler);
    containerRef.current.addEventListener('cut', copyHandler);
    containerRef.current.addEventListener('paste', pasteHandler);
    return () => {
      containerRef.current.removeEventListener('copy', copyHandler);
      containerRef.current.removeEventListener('cut', copyHandler);
      containerRef.current.removeEventListener('paste', pasteHandler);
    };
  }, [containerRef, feelActive, handleFeelToggle, setFocus]);
  return jsxRuntime.jsxs("div", {
    class: "bio-properties-panel-feel-entry",
    children: [jsxRuntime.jsxs("label", {
      for: prefixId$6(id),
      class: "bio-properties-panel-label",
      onClick: () => setFocus(),
      children: [label, jsxRuntime.jsx(FeelIcon, {
        label: label,
        feel: feel,
        onClick: handleFeelToggle,
        active: feelActive
      })]
    }), jsxRuntime.jsxs("div", {
      class: "bio-properties-panel-feel-container",
      ref: containerRef,
      children: [jsxRuntime.jsx(FeelIndicator, {
        active: feelActive,
        disabled: feel !== 'optional' || disabled,
        onClick: handleFeelToggle
      }), feelActive ? jsxRuntime.jsx(CodeEditor, {
        id: prefixId$6(id),
        name: id,
        onInput: handleLocalInput,
        disabled: disabled,
        onFeelToggle: () => {
          handleFeelToggle();
          setFocus(true);
        },
        onLint: handleLint,
        value: feelOnlyValue,
        variables: variables,
        ref: editorRef,
        tooltipContainer: tooltipContainer
      }) : jsxRuntime.jsx(OptionalComponent, {
        ...props,
        onInput: handleLocalInput,
        contentAttributes: {
          'id': prefixId$6(id)
        },
        value: localValue,
        ref: editorRef
      })]
    })]
  });
}
const OptionalFeelInput = compat.forwardRef((props, ref) => {
  const {
    id,
    disabled,
    onInput,
    value,
    onFocus,
    onBlur
  } = props;
  const inputRef = hooks.useRef();

  // To be consistent with the FEEL editor, set focus at start of input
  // this ensures clean editing experience when switching with the keyboard
  ref.current = {
    focus: position => {
      const input = inputRef.current;
      if (!input) {
        return;
      }
      input.focus();
      if (typeof position === 'number') {
        if (position > value.length) {
          position = value.length;
        }
        input.setSelectionRange(position, position);
      }
    }
  };
  return jsxRuntime.jsx("input", {
    id: prefixId$6(id),
    type: "text",
    ref: inputRef,
    name: id,
    spellCheck: "false",
    autoComplete: "off",
    disabled: disabled,
    class: "bio-properties-panel-input",
    onInput: e => onInput(e.target.value),
    onFocus: onFocus,
    onBlur: onBlur,
    value: value || ''
  });
});
const OptionalFeelTextArea = compat.forwardRef((props, ref) => {
  const {
    id,
    disabled,
    onInput,
    value,
    onFocus,
    onBlur
  } = props;
  const inputRef = hooks.useRef();

  // To be consistent with the FEEL editor, set focus at start of input
  // this ensures clean editing experience when switching with the keyboard
  ref.current = {
    focus: () => {
      const input = inputRef.current;
      if (!input) {
        return;
      }
      input.focus();
      input.setSelectionRange(0, 0);
    }
  };
  return jsxRuntime.jsx("textarea", {
    id: prefixId$6(id),
    type: "text",
    ref: inputRef,
    name: id,
    spellCheck: "false",
    autoComplete: "off",
    disabled: disabled,
    class: "bio-properties-panel-input",
    onInput: e => onInput(e.target.value),
    onFocus: onFocus,
    onBlur: onBlur,
    value: value || '',
    "data-gramm": "false"
  });
});

/**
 * @param {Object} props
 * @param {Object} props.element
 * @param {String} props.id
 * @param {String} props.description
 * @param {Boolean} props.debounce
 * @param {Boolean} props.disabled
 * @param {Boolean} props.feel
 * @param {String} props.label
 * @param {Function} props.getValue
 * @param {Function} props.setValue
 * @param {Function} props.tooltipContainer
 * @param {Function} props.validate
 * @param {Function} props.show
 * @param {Function} props.example
 * @param {Function} props.variables
 * @param {Function} props.onFocus
 * @param {Function} props.onBlur
 */
function FeelEntry(props) {
  const {
    element,
    id,
    description,
    debounce,
    disabled,
    feel,
    label,
    getValue,
    setValue,
    tooltipContainer,
    hostLanguage,
    singleLine,
    validate,
    show = noop$1,
    example,
    variables,
    onFocus,
    onBlur
  } = props;
  const [cachedInvalidValue, setCachedInvalidValue] = hooks.useState(null);
  const [validationError, setValidationError] = hooks.useState(null);
  const [localError, setLocalError] = hooks.useState(null);
  let value = getValue(element);
  const previousValue = usePrevious(value);
  hooks.useEffect(() => {
    if (minDash.isFunction(validate)) {
      const newValidationError = validate(value) || null;
      setValidationError(newValidationError);
    }
  }, [value]);
  const onInput = useStaticCallback(newValue => {
    let newValidationError = null;
    if (minDash.isFunction(validate)) {
      newValidationError = validate(newValue) || null;
    }
    if (newValidationError) {
      setCachedInvalidValue(newValue);
    } else {
      // don't create multiple commandStack entries for the same value
      if (newValue !== value) {
        setValue(newValue);
      }
    }
    setValidationError(newValidationError);
  });
  const onError = hooks.useCallback(err => {
    setLocalError(err);
  }, []);
  if (previousValue === value && validationError) {
    value = cachedInvalidValue;
  }
  const temporaryError = useError(id);
  const error = localError || temporaryError || validationError;
  return jsxRuntime.jsxs("div", {
    class: classnames__default["default"](props.class, 'bio-properties-panel-entry', error ? 'has-error' : ''),
    "data-entry-id": id,
    children: [jsxRuntime.jsx(FeelTextfield, {
      debounce: debounce,
      disabled: disabled,
      feel: feel,
      id: id,
      label: label,
      onInput: onInput,
      onError: onError,
      onFocus: onFocus,
      onBlur: onBlur,
      example: example,
      hostLanguage: hostLanguage,
      singleLine: singleLine,
      show: show,
      value: value,
      variables: variables,
      tooltipContainer: tooltipContainer,
      OptionalComponent: props.OptionalComponent
    }, element), error && jsxRuntime.jsx("div", {
      class: "bio-properties-panel-error",
      children: error
    }), jsxRuntime.jsx(Description, {
      forId: id,
      element: element,
      value: description
    })]
  });
}

/**
 * @param {Object} props
 * @param {Object} props.element
 * @param {String} props.id
 * @param {String} props.description
 * @param {Boolean} props.debounce
 * @param {Boolean} props.disabled
 * @param {Boolean} props.feel
 * @param {String} props.label
 * @param {Function} props.getValue
 * @param {Function} props.setValue
 * @param {Function} props.tooltipContainer
 * @param {Function} props.validate
 * @param {Function} props.show
 * @param {Function} props.example
 * @param {Function} props.variables
 * @param {Function} props.onFocus
 * @param {Function} props.onBlur
 */
function FeelTextAreaEntry(props) {
  return jsxRuntime.jsx(FeelEntry, {
    class: "bio-properties-panel-feel-textarea",
    OptionalComponent: OptionalFeelTextArea,
    ...props
  });
}

/**
 * @param {Object} props
 * @param {Object} props.element
 * @param {String} props.id
 * @param {String} props.description
 * @param {String} props.hostLanguage
 * @param {Boolean} props.singleLine
 * @param {Boolean} props.debounce
 * @param {Boolean} props.disabled
 * @param {Boolean} props.feel
 * @param {String} props.label
 * @param {Function} props.getValue
 * @param {Function} props.setValue
 * @param {Function} props.tooltipContainer
 * @param {Function} props.validate
 * @param {Function} props.show
 * @param {Function} props.example
 * @param {Function} props.variables
 * @param {Function} props.onFocus
 * @param {Function} props.onBlur
 */
function FeelTemplatingEntry(props) {
  return jsxRuntime.jsx(FeelEntry, {
    class: "bio-properties-panel-feel-templating",
    OptionalComponent: CodeEditor$1,
    ...props
  });
}
function isEdited$8(node) {
  return node && (!!node.value || node.classList.contains('edited'));
}

// helpers /////////////////

function prefixId$6(id) {
  return `bio-properties-panel-${id}`;
}

const noop = () => {};

/**
 * @param {Object} props
 * @param {Object} props.element
 * @param {String} props.id
 * @param {String} props.description
 * @param {Boolean} props.debounce
 * @param {Boolean} props.disabled
 * @param {String} props.label
 * @param {Function} props.getValue
 * @param {Function} props.setValue
 * @param {Function} props.tooltipContainer
 * @param {Function} props.validate
 * @param {Function} props.show
 */
function TemplatingEntry(props) {
  const {
    element,
    id,
    description,
    debounce,
    disabled,
    label,
    getValue,
    setValue,
    tooltipContainer,
    validate,
    show = noop
  } = props;
  const [cachedInvalidValue, setCachedInvalidValue] = hooks.useState(null);
  const [validationError, setValidationError] = hooks.useState(null);
  const [localError, setLocalError] = hooks.useState(null);
  let value = getValue(element);
  const previousValue = usePrevious(value);
  hooks.useEffect(() => {
    if (minDash.isFunction(validate)) {
      const newValidationError = validate(value) || null;
      setValidationError(newValidationError);
    }
  }, [value]);
  const onInput = useStaticCallback(newValue => {
    let newValidationError = null;
    if (minDash.isFunction(validate)) {
      newValidationError = validate(newValue) || null;
    }
    if (newValidationError) {
      setCachedInvalidValue(newValue);
    } else {
      // don't create multiple commandStack entries for the same value
      if (newValue !== value) {
        setValue(newValue);
      }
    }
    setValidationError(newValidationError);
  });
  const onError = hooks.useCallback(err => {
    setLocalError(err);
  }, []);
  if (previousValue === value && validationError) {
    value = cachedInvalidValue;
  }
  const temporaryError = useError(id);
  const error = localError || temporaryError || validationError;
  return jsxRuntime.jsxs("div", {
    class: classnames__default["default"]('bio-properties-panel-entry', error ? 'has-error' : ''),
    "data-entry-id": id,
    children: [jsxRuntime.jsx(Templating, {
      debounce: debounce,
      disabled: disabled,
      id: id,
      label: label,
      onInput: onInput,
      onError: onError,
      show: show,
      value: value,
      tooltipContainer: tooltipContainer
    }, element), error && jsxRuntime.jsx("div", {
      class: "bio-properties-panel-error",
      children: error
    }), jsxRuntime.jsx(Description, {
      forId: id,
      element: element,
      value: description
    })]
  });
}
function Templating(props) {
  const {
    debounce,
    id,
    label,
    onInput,
    onError,
    value = '',
    disabled = false,
    tooltipContainer
  } = props;
  const [localValue, setLocalValue] = hooks.useState(value);
  const editorRef = useShowEntryEvent(id);
  const containerRef = hooks.useRef();
  const [focus, _setFocus] = hooks.useState(undefined);
  const setFocus = (offset = 0) => {
    const hasFocus = containerRef.current.contains(document.activeElement);

    // Keep caret position if it is already focused, otherwise focus at the end
    const position = hasFocus ? document.activeElement.selectionStart : Infinity;
    _setFocus(position + offset);
  };
  const handleInputCallback = hooks.useMemo(() => {
    return debounce(newValue => onInput(newValue.length ? newValue : undefined));
  }, [onInput, debounce]);
  const handleInput = newValue => {
    handleInputCallback(newValue);
    setLocalValue(newValue);
  };
  const handleLint = useStaticCallback(lint => {
    const errors = lint && lint.length && lint.filter(e => e.severity === 'error') || [];
    if (!errors.length) {
      onError(undefined);
      return;
    }
    const error = lint[0];
    const message = `${error.source}: ${error.message}`;
    onError(message);
  });
  hooks.useEffect(() => {
    if (typeof focus !== 'undefined') {
      editorRef.current.focus(focus);
      _setFocus(undefined);
    }
  }, [focus]);
  hooks.useEffect(() => {
    if (value === localValue) {
      return;
    }
    setLocalValue(value ? value : '');
  }, [value]);
  return jsxRuntime.jsxs("div", {
    class: "bio-properties-panel-feelers",
    children: [jsxRuntime.jsx("label", {
      id: prefixIdLabel(id),
      class: "bio-properties-panel-label",
      onClick: () => setFocus(),
      children: label
    }), jsxRuntime.jsx("div", {
      class: "bio-properties-panel-feelers-input",
      ref: containerRef,
      children: jsxRuntime.jsx(CodeEditor$1, {
        name: id,
        onInput: handleInput,
        contentAttributes: {
          'aria-labelledby': prefixIdLabel(id)
        },
        disabled: disabled,
        onLint: handleLint,
        value: localValue,
        ref: editorRef,
        tooltipContainer: tooltipContainer
      })
    })]
  });
}
function isEdited$7(node) {
  return node && (!!node.value || node.classList.contains('edited'));
}

// helpers /////////////////

function prefixIdLabel(id) {
  return `bio-properties-panel-feelers-${id}-label`;
}

function List(props) {
  let {
    id,
    element,
    items = [],
    component,
    label = '<empty>',
    open: shouldOpen,
    onAdd,
    onRemove,
    showAdd,
    autoFocusEntry,
    compareFn,
    ...restProps
  } = props;
  const [open, setOpen] = hooks.useState(!!shouldOpen);
  const hasItems = !!items.length;
  const toggleOpen = () => hasItems && setOpen(!open);
  const opening = !usePrevious(open) && open;
  const elementChanged = usePrevious(element) !== element;
  const shouldReset = opening || elementChanged;
  const sortedItems = useSortedItems(items, compareFn, shouldReset);
  const newItems = useNewItems(items, elementChanged);
  if (showAdd == undefined) {
    showAdd = true;
  }
  hooks.useEffect(() => {
    if (open && !hasItems) {
      setOpen(false);
    }
  }, [open, hasItems]);

  /**
   * @param {MouseEvent} event
   */
  function addItem(event) {
    event.stopPropagation();
    onAdd();
    if (!open) {
      setOpen(true);
    }
  }
  return jsxRuntime.jsxs("div", {
    "data-entry-id": id,
    class: classnames__default["default"]('bio-properties-panel-entry', 'bio-properties-panel-list-entry', hasItems ? '' : 'empty', open ? 'open' : ''),
    children: [jsxRuntime.jsxs("div", {
      class: "bio-properties-panel-list-entry-header",
      onClick: toggleOpen,
      children: [jsxRuntime.jsx("div", {
        title: label,
        class: classnames__default["default"]('bio-properties-panel-list-entry-header-title', open && 'open'),
        children: label
      }), jsxRuntime.jsxs("div", {
        class: "bio-properties-panel-list-entry-header-buttons",
        children: [showAdd && jsxRuntime.jsxs("button", {
          title: "Create new list item",
          onClick: addItem,
          className: "bio-properties-panel-add-entry",
          children: [jsxRuntime.jsx(CreateIcon, {}), !hasItems ? jsxRuntime.jsx("span", {
            className: "bio-properties-panel-add-entry-label",
            children: "Create"
          }) : null]
        }), hasItems && jsxRuntime.jsx("div", {
          title: `List contains ${items.length} item${items.length != 1 ? 's' : ''}`,
          class: "bio-properties-panel-list-badge",
          children: items.length
        }), hasItems && jsxRuntime.jsx("button", {
          title: "Toggle list item",
          class: "bio-properties-panel-arrow",
          children: jsxRuntime.jsx(ArrowIcon, {
            class: open ? 'bio-properties-panel-arrow-down' : 'bio-properties-panel-arrow-right'
          })
        })]
      })]
    }), hasItems && jsxRuntime.jsx(ItemsList, {
      ...restProps,
      autoFocusEntry: autoFocusEntry,
      component: component,
      element: element,
      id: id,
      items: sortedItems,
      newItems: newItems,
      onRemove: onRemove,
      open: open
    })]
  });
}
function ItemsList(props) {
  const {
    autoFocusEntry,
    component: Component,
    element,
    id,
    items,
    newItems,
    onRemove,
    showAdd,
    open,
    ...restProps
  } = props;
  const getKey = useKeyFactory();
  const newItem = newItems[0];
  hooks.useEffect(() => {
    if (newItem && autoFocusEntry) {
      // (0) select the parent entry (containing all list items)
      const entry = minDom.query(`[data-entry-id="${id}"]`);

      // (1) select the first input or a custom element to be focussed
      const selector = typeof autoFocusEntry === 'boolean' ? '.bio-properties-panel-input' : autoFocusEntry;
      const focusableInput = minDom.query(selector, entry);

      // (2) set focus
      if (focusableInput) {
        if (minDash.isFunction(focusableInput.select)) {
          focusableInput.select();
        } else if (minDash.isFunction(focusableInput.focus)) {
          focusableInput.focus();
        }
      }
    }
  }, [newItem, autoFocusEntry, id]);
  return jsxRuntime.jsx("ol", {
    class: classnames__default["default"]('bio-properties-panel-list-entry-items', open ? 'open' : ''),
    children: items.map((item, index) => {
      const key = getKey(item);
      return jsxRuntime.jsxs("li", {
        class: "bio-properties-panel-list-entry-item",
        children: [jsxRuntime.jsx(Component, {
          ...restProps,
          element: element,
          id: id,
          index: index,
          item: item,
          showAdd: showAdd,
          open: item === newItem
        }), onRemove && jsxRuntime.jsx("button", {
          type: "button",
          title: "Delete item",
          class: "bio-properties-panel-remove-entry bio-properties-panel-remove-list-entry",
          onClick: () => onRemove && onRemove(item),
          children: jsxRuntime.jsx(DeleteIcon, {})
        })]
      }, key);
    })
  });
}

/**
 * Place new items in the beginning of the list and sort the rest with provided function.
 *
 * @template Item
 * @param {Item[]} currentItems
 * @param {(a: Item, b: Item) => 0 | 1 | -1} [compareFn] function used to sort items
 * @param {boolean} [shouldReset=false] set to `true` to reset state of the hook
 * @returns {Item[]}
 */
function useSortedItems(currentItems, compareFn, shouldReset = false) {
  const itemsRef = hooks.useRef(currentItems.slice());

  // (1) Reset and optionally sort.
  if (shouldReset) {
    itemsRef.current = currentItems.slice();
    if (compareFn) {
      itemsRef.current.sort(compareFn);
    }
  } else {
    const items = itemsRef.current;

    // (2) Add new item to the list.
    for (const item of currentItems) {
      if (!items.includes(item)) {
        // Unshift or push depending on whether we have a compareFn
        compareFn ? items.unshift(item) : items.push(item);
      }
    }

    // (3) Filter out removed items.
    itemsRef.current = items.filter(item => currentItems.includes(item));
  }
  return itemsRef.current;
}
function useNewItems(items = [], shouldReset) {
  const previousItems = usePrevious(items.slice()) || [];
  if (shouldReset) {
    return [];
  }
  return previousItems ? items.filter(item => !previousItems.includes(item)) : [];
}

function NumberField(props) {
  const {
    debounce,
    disabled,
    id,
    label,
    max,
    min,
    onInput,
    step,
    value = '',
    onFocus,
    onBlur
  } = props;
  const [localValue, setLocalValue] = hooks.useState(value);
  const handleInputCallback = hooks.useMemo(() => {
    return debounce(event => {
      const {
        validity,
        value
      } = event.target;
      if (validity.valid) {
        onInput(value ? parseFloat(value) : undefined);
      }
    });
  }, [onInput, debounce]);
  const handleInput = e => {
    handleInputCallback(e);
    setLocalValue(e.target.value);
  };
  hooks.useEffect(() => {
    if (value === localValue) {
      return;
    }
    setLocalValue(value);
  }, [value]);
  return jsxRuntime.jsxs("div", {
    class: "bio-properties-panel-numberfield",
    children: [jsxRuntime.jsx("label", {
      for: prefixId$5(id),
      class: "bio-properties-panel-label",
      children: label
    }), jsxRuntime.jsx("input", {
      id: prefixId$5(id),
      type: "number",
      name: id,
      spellCheck: "false",
      autoComplete: "off",
      disabled: disabled,
      class: "bio-properties-panel-input",
      max: max,
      min: min,
      onInput: handleInput,
      onFocus: onFocus,
      onBlur: onBlur,
      step: step,
      value: localValue
    })]
  });
}

/**
 * @param {Object} props
 * @param {Boolean} props.debounce
 * @param {String} props.description
 * @param {Boolean} props.disabled
 * @param {Object} props.element
 * @param {Function} props.getValue
 * @param {String} props.id
 * @param {String} props.label
 * @param {String} props.max
 * @param {String} props.min
 * @param {Function} props.setValue
 * @param {Function} props.onFocus
 * @param {Function} props.onBlur
 * @param {String} props.step
 * @param {Function} props.validate
 */
function NumberFieldEntry(props) {
  const {
    debounce,
    description,
    disabled,
    element,
    getValue,
    id,
    label,
    max,
    min,
    setValue,
    step,
    onFocus,
    onBlur,
    validate
  } = props;
  const [cachedInvalidValue, setCachedInvalidValue] = hooks.useState(null);
  const globalError = useError(id);
  const [localError, setLocalError] = hooks.useState(null);
  let value = getValue(element);
  const previousValue = usePrevious(value);
  hooks.useEffect(() => {
    if (minDash.isFunction(validate)) {
      const newValidationError = validate(value) || null;
      setLocalError(newValidationError);
    }
  }, [value]);
  const onInput = newValue => {
    let newValidationError = null;
    if (minDash.isFunction(validate)) {
      newValidationError = validate(newValue) || null;
    }
    if (newValidationError) {
      setCachedInvalidValue(newValue);
    } else {
      setValue(newValue);
    }
    setLocalError(newValidationError);
  };
  if (previousValue === value && localError) {
    value = cachedInvalidValue;
  }
  const error = globalError || localError;
  return jsxRuntime.jsxs("div", {
    class: classnames__default["default"]('bio-properties-panel-entry', error ? 'has-error' : ''),
    "data-entry-id": id,
    children: [jsxRuntime.jsx(NumberField, {
      debounce: debounce,
      disabled: disabled,
      id: id,
      label: label,
      onFocus: onFocus,
      onBlur: onBlur,
      onInput: onInput,
      max: max,
      min: min,
      step: step,
      value: value
    }, element), error && jsxRuntime.jsx("div", {
      class: "bio-properties-panel-error",
      children: error
    }), jsxRuntime.jsx(Description, {
      forId: id,
      element: element,
      value: description
    })]
  });
}
function isEdited$6(node) {
  return node && !!node.value;
}

// helpers /////////////////

function prefixId$5(id) {
  return `bio-properties-panel-${id}`;
}

function Select(props) {
  const {
    id,
    label,
    onChange,
    options = [],
    value = '',
    disabled,
    onFocus,
    onBlur
  } = props;
  const ref = useShowEntryEvent(id);
  const [localValue, setLocalValue] = hooks.useState(value);
  const handleChangeCallback = ({
    target
  }) => {
    onChange(target.value);
  };
  const handleChange = e => {
    handleChangeCallback(e);
    setLocalValue(e.target.value);
  };
  hooks.useEffect(() => {
    if (value === localValue) {
      return;
    }
    setLocalValue(value);
  }, [value]);
  return jsxRuntime.jsxs("div", {
    class: "bio-properties-panel-select",
    children: [jsxRuntime.jsx("label", {
      for: prefixId$4(id),
      class: "bio-properties-panel-label",
      children: label
    }), jsxRuntime.jsx("select", {
      ref: ref,
      id: prefixId$4(id),
      name: id,
      class: "bio-properties-panel-input",
      onInput: handleChange,
      onFocus: onFocus,
      onBlur: onBlur,
      value: localValue,
      disabled: disabled,
      children: options.map((option, idx) => {
        if (option.children) {
          return jsxRuntime.jsx("optgroup", {
            label: option.label,
            children: option.children.map((child, idx) => jsxRuntime.jsx("option", {
              value: child.value,
              disabled: child.disabled,
              children: child.label
            }, idx))
          }, idx);
        }
        return jsxRuntime.jsx("option", {
          value: option.value,
          disabled: option.disabled,
          children: option.label
        }, idx);
      })
    })]
  });
}

/**
 * @param {object} props
 * @param {object} props.element
 * @param {string} props.id
 * @param {string} [props.description]
 * @param {string} props.label
 * @param {Function} props.getValue
 * @param {Function} props.setValue
 * @param {Function} props.onFocus
 * @param {Function} props.onBlur
 * @param {Function} props.getOptions
 * @param {boolean} [props.disabled]
 * @param {Function} [props.validate]
 */
function SelectEntry(props) {
  const {
    element,
    id,
    description,
    label,
    getValue,
    setValue,
    getOptions,
    disabled,
    onFocus,
    onBlur,
    validate,
    defaultValue
  } = props;
  const options = getOptions(element);
  const [cachedInvalidValue, setCachedInvalidValue] = hooks.useState(null);
  const globalError = useError(id);
  const [localError, setLocalError] = hooks.useState(null);
  let value = getValue(element) || defaultValue;
  const previousValue = usePrevious(value);
  hooks.useEffect(() => {
    if (minDash.isFunction(validate)) {
      const newValidationError = validate(value) || null;
      setLocalError(newValidationError);
    }
  }, [value]);
  const onChange = newValue => {
    let newValidationError = null;
    if (minDash.isFunction(validate)) {
      newValidationError = validate(newValue) || null;
    }
    if (newValidationError) {
      setCachedInvalidValue(newValue);
    } else {
      setValue(newValue);
    }
    setLocalError(newValidationError);
  };
  if (previousValue === value && localError) {
    value = cachedInvalidValue;
  }
  const error = globalError || localError;
  return jsxRuntime.jsxs("div", {
    class: classnames__default["default"]('bio-properties-panel-entry', error ? 'has-error' : ''),
    "data-entry-id": id,
    children: [jsxRuntime.jsx(Select, {
      id: id,
      label: label,
      value: value,
      onChange: onChange,
      onFocus: onFocus,
      onBlur: onBlur,
      options: options,
      disabled: disabled
    }, element), error && jsxRuntime.jsx("div", {
      class: "bio-properties-panel-error",
      children: error
    }), jsxRuntime.jsx(Description, {
      forId: id,
      element: element,
      value: description
    })]
  });
}
function isEdited$5(node) {
  return node && !!node.value;
}

// helpers /////////////////

function prefixId$4(id) {
  return `bio-properties-panel-${id}`;
}

function Simple(props) {
  const {
    debounce,
    disabled,
    element,
    getValue,
    id,
    onBlur,
    onFocus,
    setValue
  } = props;
  const value = getValue(element);
  const [localValue, setLocalValue] = hooks.useState(value);
  const handleInputCallback = hooks.useMemo(() => {
    return debounce(({
      target
    }) => setValue(target.value.length ? target.value : undefined));
  }, [setValue, debounce]);
  const handleInput = e => {
    handleInputCallback(e);
    setLocalValue(e.target.value);
  };
  hooks.useEffect(() => {
    if (value === localValue) {
      return;
    }
    setLocalValue(value);
  }, [value]);
  return jsxRuntime.jsx("div", {
    class: "bio-properties-panel-simple",
    children: jsxRuntime.jsx("input", {
      id: prefixId$3(id),
      type: "text",
      name: id,
      spellCheck: "false",
      autoComplete: "off",
      disabled: disabled,
      class: "bio-properties-panel-input",
      onInput: handleInput,
      "aria-label": localValue || '<empty>',
      onFocus: onFocus,
      onBlur: onBlur,
      value: localValue
    }, element)
  });
}
function isEdited$4(node) {
  return node && !!node.value;
}

// helpers /////////////////

function prefixId$3(id) {
  return `bio-properties-panel-${id}`;
}

function resizeToContents(element) {
  element.style.height = 'auto';

  // a 2px pixel offset is required to prevent scrollbar from
  // appearing on OS with a full length scroll bar (Windows/Linux)
  element.style.height = `${element.scrollHeight + 2}px`;
}
function TextArea(props) {
  const {
    id,
    label,
    debounce,
    onInput,
    value = '',
    disabled,
    monospace,
    onFocus,
    onBlur,
    autoResize,
    rows = autoResize ? 1 : 2
  } = props;
  const [localValue, setLocalValue] = hooks.useState(value);
  const ref = useShowEntryEvent(id);
  const handleInputCallback = hooks.useMemo(() => {
    return debounce(({
      target
    }) => onInput(target.value.length ? target.value : undefined));
  }, [onInput, debounce]);
  const handleInput = e => {
    handleInputCallback(e);
    autoResize && resizeToContents(e.target);
    setLocalValue(e.target.value);
  };
  hooks.useLayoutEffect(() => {
    autoResize && resizeToContents(ref.current);
  }, []);
  hooks.useEffect(() => {
    if (value === localValue) {
      return;
    }
    setLocalValue(value);
  }, [value]);
  return jsxRuntime.jsxs("div", {
    class: "bio-properties-panel-textarea",
    children: [jsxRuntime.jsx("label", {
      for: prefixId$2(id),
      class: "bio-properties-panel-label",
      children: label
    }), jsxRuntime.jsx("textarea", {
      ref: ref,
      id: prefixId$2(id),
      name: id,
      spellCheck: "false",
      class: classnames__default["default"]('bio-properties-panel-input', monospace ? 'bio-properties-panel-input-monospace' : '', autoResize ? 'auto-resize' : ''),
      onInput: handleInput,
      onFocus: onFocus,
      onBlur: onBlur,
      rows: rows,
      value: localValue,
      disabled: disabled,
      "data-gramm": "false"
    })]
  });
}

/**
 * @param {object} props
 * @param {object} props.element
 * @param {string} props.id
 * @param {string} props.description
 * @param {boolean} props.debounce
 * @param {string} props.label
 * @param {Function} props.getValue
 * @param {Function} props.setValue
 * @param {Function} props.onFocus
 * @param {Function} props.onBlur
 * @param {number} props.rows
 * @param {boolean} props.monospace
 * @param {Function} [props.validate]
 * @param {boolean} [props.disabled]
 */
function TextAreaEntry(props) {
  const {
    element,
    id,
    description,
    debounce,
    label,
    getValue,
    setValue,
    rows,
    monospace,
    disabled,
    validate,
    onFocus,
    onBlur,
    autoResize
  } = props;
  const [cachedInvalidValue, setCachedInvalidValue] = hooks.useState(null);
  const globalError = useError(id);
  const [localError, setLocalError] = hooks.useState(null);
  let value = getValue(element);
  const previousValue = usePrevious(value);
  hooks.useEffect(() => {
    if (minDash.isFunction(validate)) {
      const newValidationError = validate(value) || null;
      setLocalError(newValidationError);
    }
  }, [value]);
  const onInput = newValue => {
    let newValidationError = null;
    if (minDash.isFunction(validate)) {
      newValidationError = validate(newValue) || null;
    }
    if (newValidationError) {
      setCachedInvalidValue(newValue);
    } else {
      setValue(newValue);
    }
    setLocalError(newValidationError);
  };
  if (previousValue === value && localError) {
    value = cachedInvalidValue;
  }
  const error = globalError || localError;
  return jsxRuntime.jsxs("div", {
    class: classnames__default["default"]('bio-properties-panel-entry', error ? 'has-error' : ''),
    "data-entry-id": id,
    children: [jsxRuntime.jsx(TextArea, {
      id: id,
      label: label,
      value: value,
      onInput: onInput,
      onFocus: onFocus,
      onBlur: onBlur,
      rows: rows,
      debounce: debounce,
      monospace: monospace,
      disabled: disabled,
      autoResize: autoResize
    }, element), error && jsxRuntime.jsx("div", {
      class: "bio-properties-panel-error",
      children: error
    }), jsxRuntime.jsx(Description, {
      forId: id,
      element: element,
      value: description
    })]
  });
}
function isEdited$3(node) {
  return node && !!node.value;
}

// helpers /////////////////

function prefixId$2(id) {
  return `bio-properties-panel-${id}`;
}

function Textfield(props) {
  const {
    debounce,
    disabled = false,
    id,
    label,
    onInput,
    onFocus,
    onBlur,
    placeholder,
    labelPostion,
    value = ''
  } = props;
  const [localValue, setLocalValue] = hooks.useState(value || '');
  const ref = useShowEntryEvent(id);
  const handleInputCallback = hooks.useMemo(() => {
    return debounce(({
      target
    }) => onInput(target.value.length ? target.value : undefined));
  }, [onInput, debounce]);
  const handleInput = e => {
    handleInputCallback(e);
    setLocalValue(e.target.value);
  };
  hooks.useEffect(() => {
    if (value === localValue) {
      return;
    }
    setLocalValue(value);
  }, [value]);
  return jsxRuntime.jsxs("div", {
    class: "bio-properties-panel-textfield",
    children: [jsxRuntime.jsx("label", {
      for: prefixId$1(id),
      class: labelPostion == 'right' ? "bio-properties-panel-label right" : "bio-properties-panel-label",
      children: label
    }), jsxRuntime.jsx("input", {
      ref: ref,
      id: prefixId$1(id),
      type: "text",
      name: id,
      placeholder: placeholder,
      spellCheck: "false",
      autoComplete: "off",
      disabled: disabled,
      class: "bio-properties-panel-input",
      onInput: handleInput,
      onFocus: onFocus,
      onBlur: onBlur,
      value: localValue
    })]
  });
}

/**
 * @param {Object} props
 * @param {Object} props.element
 * @param {String} props.id
 * @param {String} props.description
 * @param {Boolean} props.debounce
 * @param {Boolean} props.disabled
 * @param {String} props.label
 * @param {Function} props.getValue
 * @param {Function} props.setValue
 * @param {Function} props.onFocus
 * @param {Function} props.onBlur
 * @param {Function} props.validate
 */
function TextfieldEntry(props) {
  const {
    element,
    id,
    description,
    debounce,
    disabled,
    label,
    getValue,
    setValue,
    validate,
    onFocus,
    placeholder,
    labelPostion,
    onBlur
  } = props;
  const [cachedInvalidValue, setCachedInvalidValue] = hooks.useState(null);
  const globalError = useError(id);
  const [localError, setLocalError] = hooks.useState(null);
  let value = getValue(element);
  const previousValue = usePrevious(value);
  hooks.useEffect(() => {
    if (minDash.isFunction(validate)) {
      const newValidationError = validate(value) || null;
      setLocalError(newValidationError);
    }
  }, [value]);
  const onInput = newValue => {
    let newValidationError = null;
    if (minDash.isFunction(validate)) {
      newValidationError = validate(newValue) || null;
    }
    if (newValidationError) {
      setCachedInvalidValue(newValue);
    } else {
      setValue(newValue);
    }
    setLocalError(newValidationError);
  };
  if (previousValue === value && localError) {
    value = cachedInvalidValue;
  }
  const error = globalError || localError;
  return jsxRuntime.jsxs("div", {
    class: classnames__default["default"]('bio-properties-panel-entry', error ? 'has-error' : ''),
    "data-entry-id": id,
    children: [jsxRuntime.jsx(Textfield, {
      debounce: debounce,
      disabled: disabled,
      id: id,
      label: label,
      placeholder: placeholder,
      labelPostion: labelPostion,
      onInput: onInput,
      onFocus: onFocus,
      onBlur: onBlur,
      value: value
    }, element), error && jsxRuntime.jsx("div", {
      class: "bio-properties-panel-error",
      children: error
    }), jsxRuntime.jsx(Description, {
      forId: id,
      element: element,
      value: description
    })]
  });
}
function isEdited$2(node) {
  return node && !!node.value;
}

// helpers /////////////////

function prefixId$1(id) {
  return `bio-properties-panel-${id}`;
}

function ToggleSwitch(props) {
  const {
    id,
    label,
    onInput,
    value,
    switcherLabel,
    onFocus,
    onBlur
  } = props;
  const [localValue, setLocalValue] = hooks.useState(value);
  const handleInputCallback = async () => {
    onInput(!value);
  };
  const handleInput = e => {
    handleInputCallback();
    setLocalValue(e.target.value);
  };
  hooks.useEffect(() => {
    if (value === localValue) {
      return;
    }
    setLocalValue(value);
  }, [value]);
  return jsxRuntime.jsxs("div", {
    class: "bio-properties-panel-toggle-switch",
    children: [jsxRuntime.jsx("label", {
      class: "bio-properties-panel-label",
      for: prefixId(id),
      children: label
    }), jsxRuntime.jsxs("div", {
      class: "bio-properties-panel-field-wrapper",
      children: [jsxRuntime.jsxs("label", {
        class: "bio-properties-panel-toggle-switch__switcher",
        children: [jsxRuntime.jsx("input", {
          id: prefixId(id),
          class: "bio-properties-panel-input",
          type: "checkbox",
          onFocus: onFocus,
          onBlur: onBlur,
          name: id,
          onInput: handleInput,
          checked: !!localValue
        }), jsxRuntime.jsx("span", {
          class: "bio-properties-panel-toggle-switch__slider"
        })]
      }), jsxRuntime.jsx("p", {
        class: "bio-properties-panel-toggle-switch__label",
        children: switcherLabel
      })]
    })]
  });
}

/**
 * @param {Object} props
 * @param {Object} props.element
 * @param {String} props.id
 * @param {String} props.description
 * @param {String} props.label
 * @param {String} props.switcherLabel
 * @param {Function} props.getValue
 * @param {Function} props.setValue
 * @param {Function} props.onFocus
 * @param {Function} props.onBlur
 */
function ToggleSwitchEntry(props) {
  const {
    element,
    id,
    description,
    label,
    switcherLabel,
    getValue,
    setValue,
    onFocus,
    onBlur
  } = props;
  const value = getValue(element);
  return jsxRuntime.jsxs("div", {
    class: "bio-properties-panel-entry bio-properties-panel-toggle-switch-entry",
    "data-entry-id": id,
    children: [jsxRuntime.jsx(ToggleSwitch, {
      id: id,
      label: label,
      value: value,
      onInput: setValue,
      onFocus: onFocus,
      onBlur: onBlur,
      switcherLabel: switcherLabel
    }), jsxRuntime.jsx(Description, {
      forId: id,
      element: element,
      value: description
    })]
  });
}
function isEdited$1(node) {
  return node && !!node.checked;
}

// helpers /////////////////

function prefixId(id) {
  return `bio-properties-panel-${id}`;
}

/**
 * @param {object} props
 * @param {object} props.element
 * @param {string} props.id
 * @param {string} [props.description]
 * @param {string} props.label
 * @param {Function} props.getValue
 * @param {Function} props.setValue
 * @param {Function} props.onFocus
 * @param {Function} props.onBlur
 * @param {Function} props.getOptions
 * @param {boolean} [props.disabled]
 * @param {Function} [props.validate]
 */
function RadioEntry(props) {
  const {
    element,
    id,
    description,
    label,
    getValue,
    setValue,
    getOptions,
    disabled,
    onFocus,
    onBlur,
    validate,
    defaultValue
  } = props;
  const options = getOptions(element);
  const [cachedInvalidValue, setCachedInvalidValue] = hooks.useState(null);
  const globalError = useError(id);
  const [localError, setLocalError] = hooks.useState(null);
  let value = getValue(element) || defaultValue;
  const previousValue = usePrevious(value);
  hooks.useEffect(() => {
    if (minDash.isFunction(validate)) {
      const newValidationError = validate(value) || null;
      setLocalError(newValidationError);
    }
  }, [value]);
  if (previousValue === value && localError) {
    value = cachedInvalidValue;
  }
  const error = globalError || localError;
  return jsxRuntime.jsxs("div", {
    class: classnames__default["default"]('bio-properties-panel-entry', error ? 'has-error' : ''),
    "data-entry-id": id,
    children: [error && jsxRuntime.jsx("div", {
      class: "bio-properties-panel-error",
      children: error
    }), options.map((option, idx) => {
      return jsxRuntime.jsxs("label", {
        children: [jsxRuntime.jsx("input", {
          id: id,
          type: "radio",
          label: label,
          value: option.value,
          checked: value == option.value,
          onChange: setValue,
          onFocus: onFocus,
          onBlur: onBlur,
          disabled: disabled
        }, element), option.label]
      });
    }), jsxRuntime.jsx(Description, {
      forId: id,
      element: element,
      value: description
    })]
  });
}
function isEdited(node) {
  return node && !!node.value;
}

const DEFAULT_DEBOUNCE_TIME = 300;
function debounceInput(debounceDelay) {
  return function _debounceInput(fn) {
    if (debounceDelay !== false) {
      var debounceTime = minDash.isNumber(debounceDelay) ? debounceDelay : DEFAULT_DEBOUNCE_TIME;
      return minDash.debounce(fn, debounceTime);
    } else {
      return fn;
    }
  };
}
debounceInput.$inject = ['config.debounceInput'];

var index = {
  debounceInput: ['factory', debounceInput]
};

exports.ArrowIcon = ArrowIcon;
exports.CheckboxEntry = CheckboxEntry;
exports.CollapsibleEntry = CollapsibleEntry;
exports.CreateIcon = CreateIcon;
exports.DebounceInputModule = index;
exports.DeleteIcon = DeleteIcon;
exports.DescriptionContext = DescriptionContext;
exports.DescriptionEntry = Description;
exports.DropdownButton = DropdownButton;
exports.ErrorsContext = ErrorsContext;
exports.EventContext = EventContext;
exports.ExternalLinkIcon = ExternalLinkIcon;
exports.FeelEntry = FeelEntry;
exports.FeelOptionalIcon = FeelOptionalIcon;
exports.FeelRequiredIcon = FeelRequiredIcon;
exports.FeelTemplatingEntry = FeelTemplatingEntry;
exports.FeelTextAreaEntry = FeelTextAreaEntry;
exports.Group = Group;
exports.Header = Header;
exports.HeaderButton = HeaderButton;
exports.LayoutContext = LayoutContext;
exports.ListEntry = List;
exports.ListGroup = ListGroup;
exports.ListItem = ListItem;
exports.NumberFieldEntry = NumberFieldEntry;
exports.Placeholder = Placeholder;
exports.PropertiesPanel = PropertiesPanel;
exports.PropertiesPanelContext = LayoutContext;
exports.RadioEntry = RadioEntry;
exports.SelectEntry = SelectEntry;
exports.SimpleEntry = Simple;
exports.TemplatingEntry = TemplatingEntry;
exports.TextAreaEntry = TextAreaEntry;
exports.TextFieldEntry = TextfieldEntry;
exports.ToggleSwitchEntry = ToggleSwitchEntry;
exports.isCheckboxEntryEdited = isEdited$9;
exports.isFeelEntryEdited = isEdited$8;
exports.isNumberFieldEntryEdited = isEdited$6;
exports.isRadioEntryEdited = isEdited;
exports.isSelectEntryEdited = isEdited$5;
exports.isSimpleEntryEdited = isEdited$4;
exports.isTemplatingEntryEdited = isEdited$7;
exports.isTextAreaEntryEdited = isEdited$3;
exports.isTextFieldEntryEdited = isEdited$2;
exports.isToggleSwitchEntryEdited = isEdited$1;
exports.useDescriptionContext = useDescriptionContext;
exports.useError = useError;
exports.useEvent = useEvent;
exports.useKeyFactory = useKeyFactory;
exports.useLayoutState = useLayoutState;
exports.usePrevious = usePrevious;
exports.useShowEntryEvent = useShowEntryEvent;
exports.useStaticCallback = useStaticCallback;
exports.useStickyIntersectionObserver = useStickyIntersectionObserver;
//# sourceMappingURL=index.js.map

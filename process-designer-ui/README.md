# process-ui

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## 使用指南

### 确认项目路径

1. 确保 `process-designer-ui` 和 `properties-panel` 项目位于同一级目录中。

### 打包`properties-panel`

2. **不要** 直接去`process-designer-ui` 目录下运行 `npm install`。

3. 切换到 `properties-panel` 目录，并运行 `yarn` 命令（也可以尝试使用 `npm install`需要配置国内镜像）

   ```bash
   cd properties-panel
   yarn
   ```

    - **注意**：如果 `yarn` 命令没有自动执行 build 脚本，请参考 `package.json` 文件中的内容手动执行：

      ```json
      "scripts": {
        "build": "del-cli preact dist && rollup -c",
        // ... 其他脚本
      }
      ```

4. 打包完成后不需要修改任何其他内容

### 部署`process-designer-ui`

5. 打包完成后，返回到 `process-designer-ui` 目录，并编辑 `package.json` 和 `package-lock.json` 文件

    - 将 `@bpmn-io/properties-panel` 的路径更改为相对路径：

      ```json
      "@bpmn-io/properties-panel": "../properties-panel"
      ```

6. 在 `process-designer-ui` 目录下运行 `yarn` 来安装依赖。

   ```bash
   yarn
   ```

### 访问页面

7. 打开`process-designer-ui`项目内的 `process-designer-ui/src/router/index.js` 文件，查看定义的路由路径。
8. 在浏览器中打开 `http://localhost:8080/#/`。这是一个空白页面，您需要在 URL 后添加在 `index.js` 中定义的相应路径来访问特定页面。例如：`http://localhost:8080/#/bis`。
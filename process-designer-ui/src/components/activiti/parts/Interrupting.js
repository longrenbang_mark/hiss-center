import {
    CheckboxEntry,
    isCheckboxEntryEdited,
    isTextFieldEntryEdited,
    SelectEntry,
    TextFieldEntry
} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";
import {getBusinessObject, is} from "bpmn-js/lib/util/ModelUtil";

const component = {
    id: 'interrupting',
    component: Interrupting
}

if(window['register']) {
    window['register'].regComponent(component.id,component)
}
export default function(element) {
    component['element']=element;
    return component;
}

function Interrupting(props) {
    const {
        element
    } = props;
    const modeling = useService('modeling');
    const commandStack = useService('commandStack');
    const translate = useService('translate');
    let getValue, setValue;
    setValue = value => {
        modeling.updateProperties(element, {
            isInterrupting: value
        });
    };
    getValue = element => {
        return element.businessObject.isInterrupting;
    };
    return CheckboxEntry({
        element,
        id: 'isInterrupting',
        label: translate('Interrupting'),
        getValue,
        setValue
    });
}

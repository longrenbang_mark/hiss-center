import {html} from 'htm/preact';
import {isTextFieldEntryEdited, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";
import {updateMultiInstance} from "@/components/activiti/util";
import {getBusinessObject} from "bpmn-js/lib/util/ModelUtil";

const component = {
  id: 'completionCondition',
  component: CompletionCondition,
  isEdited: isTextFieldEntryEdited
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

function getBody(expression) {
  return expression && expression.get('body');
}

function CompletionCondition(props) {
  const { element, id } = props;
  const translate = useService('translate');
  const debounce = useService('debounceInput');
  const bpmnFactory = useService('bpmnFactory');
  const commandStack = useService('commandStack');
  const businessObject = getBusinessObject(element);
  let loopCharacteristics = businessObject.get('loopCharacteristics');
  const getValue = () => {
    if(loopCharacteristics){
      return getBody(loopCharacteristics.get('completionCondition')) || '';
    }
    return '';
  }

  const setValue = value => {
    return updateMultiInstance(element,businessObject,bpmnFactory,commandStack,undefined,'completionCondition',{
      body: value
    })
  }

  return html`<${TextFieldEntry}
      id=${ element.id+"-"+id }
    element=${ element }
    description=${ translate('nrOfInstances: total number of instances, nrOfCompletedInstances: number of completed instances, for example: ${nrOfCompletedInstances/nrOfInstances>=0.6}, 60% of completed instances can be completed') }
    label=${ translate('CompletionCondition') }
    getValue=${ getValue }
    setValue=${ setValue }
    debounce=${ debounce }
  />`
}


import {isTextFieldEntryEdited, TextAreaEntry, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";
import {getBusinessObject} from "bpmn-js/lib/util/ModelUtil";

const component = {
    id: 'resource',
    component: Resource,
    isEdited: isTextFieldEntryEdited
}

if(window['register']) {
    window['register'].regComponent(component.id,component)
}
export default function(element) {
    component['element']=element;
    return component;
}

function Resource(props) {
    const {
        element,
        idPrefix,
        script
    } = props;
    const commandStack = useService('commandStack');
    const translate = useService('translate');
    const debounce = useService('debounceInput');
    const businessObject = script || getBusinessObject(element);
    const getValue = () => {
        return businessObject.get('activiti:resource');
    };
    const setValue = value => {
        commandStack.execute('element.updateModdleProperties', {
            element,
            moddleElement: businessObject,
            properties: {
                'activiti:resource': value || ''
            }
        });
    };
    return TextFieldEntry({
        element,
        id: idPrefix + 'scriptResource',
        label: translate('Resource'),
        getValue,
        setValue,
        debounce
    });
}

import {isTextFieldEntryEdited, SelectEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";

const component = {
    id: 'propertyType',
    component: PropertyType,
    isEdited: isTextFieldEntryEdited
}

if(window['register']) {
    window['register'].regComponent(component.id,component)
}
export default function(element) {
    component['element']=element;
    return component;
}

function PropertyType(props) {
    const {
        idPrefix,
        element,
        field
    } = props;
    const commandStack = useService('commandStack'),
        translate = useService('translate');
    const getValue = field => {
        return determineType(field);
    };
    const setValue = value => {
        const properties = Object.assign({}, DEFAULT_PROPS$2);
        properties[value] = '';
        commandStack.execute('element.updateModdleProperties', {
            element,
            moddleElement: field,
            properties
        });
    };
    const getOptions = element => {
        const options = [{
            value: 'string',
            label: translate('String')
        }, {
            value: 'expression',
            label: translate('Expression')
        }];
        return options;
    };
    return SelectEntry({
        element: field,
        id: idPrefix + '-type',
        label: translate('Type'),
        getValue,
        setValue,
        getOptions
    });
}
const DEFAULT_PROPS$2 = {
    'stringValue': undefined,
    'string': undefined,
    'expression': undefined
};
function determineType(field) {
    // string is the default type
    return 'string' in field && 'string' || 'expression' in field && 'expression' || 'stringValue' in field && 'string' || 'string';
}

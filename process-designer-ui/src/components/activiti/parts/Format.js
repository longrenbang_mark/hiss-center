import {isTextFieldEntryEdited, SelectEntry, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";
import {getBusinessObject} from "bpmn-js/lib/util/ModelUtil";

const component = {
    id: 'format',
    component: Format,
    isEdited: isTextFieldEntryEdited
}

if(window['register']) {
    window['register'].regComponent(component.id,component)
}
export default function(element) {
    component['element']=element;
    return component;
}

function Format(props) {
    const {
        element,
        idPrefix,
        script
    } = props;
    const commandStack = useService('commandStack');
    const translate = useService('translate');
    const debounce = useService('debounceInput');
    const businessObject = script || getBusinessObject(element);
    const getValue = () => {
        return businessObject.get('scriptFormat');
    };
    const setValue = value => {
        commandStack.execute('element.updateModdleProperties', {
            element,
            moddleElement: businessObject,
            properties: {
                scriptFormat: value
            }
        });
    };
    const getOptions = () => {
        const options = [{
            value: 'javascript',
            label: 'javascript'
        }, {
            value: 'groovy',
            label: 'groovy'
        }];
        return options;
    };
    return SelectEntry({
        element,
        id: idPrefix + 'scriptFormat',
        label: translate('Format'),
        getValue,
        setValue,
        getOptions
    });
}

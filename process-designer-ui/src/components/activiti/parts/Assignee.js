import {html} from 'htm/preact';
import {isTextFieldEntryEdited, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";
import {useContext} from "@bpmn-io/properties-panel/preact/hooks";
import {getBusinessObject} from "bpmn-js/lib/util/ModelUtil";

const component = {
  id: 'assignee',
  component: Assignee,
  isEdited: isTextFieldEntryEdited
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

function Assignee(props) {
  const { element, id } = props;
  const modeling = useService('modeling');
  const translate = useService('translate');
  const debounce = useService('debounceInput');
  const businessObject = getBusinessObject(element);
  const getValue = () => {
    return businessObject.get('activiti:assignee') || '';
  }

  const setValue = value => {
    return modeling.updateProperties(element, {
      'activiti:assignee': value
    });
  }

  return html`<${TextFieldEntry}
    id=${ id+"-assignee" }
    element=${ element }
    label=${ translate('Assignee') }
    getValue=${ getValue }
    setValue=${ setValue }
    debounce=${ debounce }
  />`
}

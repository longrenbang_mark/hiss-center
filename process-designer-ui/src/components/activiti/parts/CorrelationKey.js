import {isTextFieldEntryEdited, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";
import {getBusinessObject} from "bpmn-js/lib/util/ModelUtil";
import activitiMapping from "@/components/activiti/ActivitiMapping";

const component = {
  id: 'correlationKey',
  component: CorrelationKey,
  isEdited: isTextFieldEntryEdited
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

function CorrelationKey(props) {
  const {
    element,
    id = 'correlationKey'
  } = props;
  const commandStack = useService('commandStack');
  const translate = useService('translate');
  const debounce = useService('debounceInput');
  let businessObject = getBusinessObject(element);
  if(businessObject.eventDefinitions){
    businessObject = businessObject.eventDefinitions[0];
  }
  const getValue = () => {
    return businessObject.get('activiti:correlationKey');
  };
  const setValue = value => {
    commandStack.execute('element.updateModdleProperties', {
      element,
      moddleElement: businessObject,
      properties: {
        'activiti:correlationKey': value || ''
      }
    });
  };
  return TextFieldEntry({
    element,
    id:id+"-correlationKey",
    label: translate('CorrelationKey'),
    getValue,
    setValue,
    debounce
  });
}

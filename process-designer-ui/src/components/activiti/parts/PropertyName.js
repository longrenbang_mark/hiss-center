import {html} from 'htm/preact';
import {isTextFieldEntryEdited, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";

const component = {
    id: 'propertyName',
    component: PropertyName,
    isEdited: isTextFieldEntryEdited
}

if(window['register']) {
    window['register'].regComponent(component.id,component)
}
export default function(element) {
    component['element']=element;
    return component;
}

function PropertyName(props) {
    const {
        idPrefix,
        element,
        property
    } = props;
    const commandStack = useService('commandStack');
    const translate = useService('translate');
    const debounce = useService('debounceInput');
    const setValue = value => {
        commandStack.execute('element.updateModdleProperties', {
            element,
            moddleElement: property,
            properties: {
                name: value
            }
        });
    };
    const getValue = () => {
        return property.name;
    };
    return TextFieldEntry({
        element: property,
        id: idPrefix + '-name',
        label: translate('Name'),
        getValue,
        setValue,
        debounce
    });
}

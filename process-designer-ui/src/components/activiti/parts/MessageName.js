import {html} from 'htm/preact';
import {isTextFieldEntryEdited, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";
import {getBusinessObject} from "bpmn-js/lib/util/ModelUtil";

const component = {
  id: 'messageName',
  component: MessageName,
  isEdited: isTextFieldEntryEdited
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

function MessageName(props) {
  const {
    element,listener
  } = props;
  const commandStack = useService('commandStack');
  const translate = useService('translate');
  const debounce = useService('debounceInput');
  const businessObject = getBusinessObject(listener);
  const getValue = () => {
    return businessObject.get('messageName');
  };
  const setValue = value => {
    commandStack.execute('element.updateModdleProperties', {
      element,
      moddleElement: businessObject,
      properties: {
        'messageName': value
      }
    });
  };
  return TextFieldEntry({
    element,
    id: element.id+'-messageName',
    label: translate('MessageName'),
    getValue,
    setValue,
    debounce
  });
}

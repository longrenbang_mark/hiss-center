import {isTextFieldEntryEdited, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";
import {getBusinessObject} from "bpmn-js/lib/util/ModelUtil";

const component = {
  id: 'calledElement',
  component: CalledElement,
  isEdited: isTextFieldEntryEdited
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

function CalledElement(props) {
  const {
    element,
    id = 'calledElement'
  } = props;
  const commandStack = useService('commandStack');
  const translate = useService('translate');
  const debounce = useService('debounceInput');
  let businessObject = getBusinessObject(element);
  const getValue = () => {
    return businessObject.get('calledElement');
  };
  const setValue = value => {
    commandStack.execute('element.updateModdleProperties', {
      element,
      moddleElement: businessObject,
      properties: {
        'calledElement': value || ''
      }
    });
  };
  const validate = value => {
    if(!value){
      return translate('value required')
    }
  }
  return TextFieldEntry({
    element,
    id:id+"-calledElement",
    label: translate('CalledElement'),
    getValue,
    setValue,
    debounce,
    validate
  });
}

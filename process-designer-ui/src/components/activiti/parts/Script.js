import {isTextFieldEntryEdited, TextAreaEntry, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";
import {getBusinessObject} from "bpmn-js/lib/util/ModelUtil";
import {getScriptProperty,getScriptValue} from "../util"

const component = {
    id: 'script',
    component: Script,
    isEdited: isTextFieldEntryEdited
}

if(window['register']) {
    window['register'].regComponent(component.id,component)
}
export default function(element) {
    component['element']=element;
    return component;
}

function Script(props) {
    const {
        element,
        idPrefix,
        script
    } = props;
    const commandStack = useService('commandStack');
    const translate = useService('translate');
    const debounce = useService('debounceInput');
    const businessObject = script || getBusinessObject(element);
    const scriptProperty = getScriptProperty(businessObject);
    const getValue = () => {
        return getScriptValue(businessObject);
    };
    const setValue = value => {
        commandStack.execute('element.updateModdleProperties', {
            element,
            moddleElement: businessObject,
            properties: {
                [scriptProperty]: value || ''
            }
        });
    };
    return TextAreaEntry({
        element,
        id: idPrefix + 'scriptValue',
        label: translate('Script'),
        description:translate('Built-in variables: execution, * Service, processEngineConfiguration') ,
        getValue,
        setValue,
        debounce,
        monospace: true
    });
}

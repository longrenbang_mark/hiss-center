import Ids from 'ids';
import {getBusinessObject, is} from 'bpmn-js/lib/util/ModelUtil';
import {isAny} from "bpmn-js/lib/features/modeling/util/ModelingUtil";
import {without,isArray} from 'min-dash';
import {useService} from "bpmn-js-properties-panel";
import {TextAreaEntry, TextFieldEntry} from "@bpmn-io/properties-panel";

export function getExtension(element, type) {
  if (!element.extensionElements) {
    return null;
  }

  return element.extensionElements.values.filter(function(e) {
    return e.$instanceOf(type);
  })[0];
}

export function getListenersContainer(element) {
  const businessObject = getBusinessObject(element);
  return businessObject.get('processRef') || businessObject;
}

export function createElement(elementType, properties, parent, factory) {
  const element = factory.create(elementType, properties);

  if (parent) {
    element.$parent = parent;
  }

  return element;
}

export function nextId(prefix) {
  const ids = new Ids([ 32,32,1 ]);

  return ids.nextPrefixed(prefix);
}

export function getScriptType(element) {
  const businessObject = getBusinessObject(element);
  const scriptValue = getScriptValue(businessObject);
  if (typeof scriptValue !== 'undefined') {
    return 'script';
  }
  const resource = businessObject.get('activiti:resource');
  if (typeof resource !== 'undefined') {
    return 'resource';
  }
}
export function getScriptValue(businessObject) {
  return businessObject.get(getScriptProperty(businessObject));
}
export function isScript$2(element) {
  return is(element, 'activiti:Script');
}
export function getScriptProperty(businessObject) {
  return isScript$2(businessObject) ? 'value' : 'script';
}
export function isInterrupting(element) {
  return element && getBusinessObject(element).isInterrupting !== false;
}

export function getRelevantBusinessObject(element) {
  let businessObject = getBusinessObject(element);
  if (is(element, 'bpmn:Participant')) {
    return businessObject.get('processRef');
  }
  return businessObject;
}

export function getPropertyName(namespace = 'activiti') {
  if (namespace === 'zeebe') {
    return 'properties';
  }
  return 'values';
}

export function getExtensionElementsAndName(businessObject, type = undefined,name = undefined) {
  if(businessObject==undefined){
    return [];
  }
  const extensionElements = businessObject.get('extensionElements');
  if (!extensionElements) {
    return [];
  }
  let values = extensionElements.get('values');
  if (!values || !values.length) {
    return [];
  }
  if (type) {
    values = values.filter(value => is(value, type));
  }
  if(name) {
    values = values.filter(value => value.get('name')==name);
  }
  return values;
}


export function getExtensionElementsList(businessObject, type = undefined) {
  if(businessObject==undefined){
    return [];
  }
  const extensionElements = businessObject.get('extensionElements');
  if (!extensionElements) {
    return [];
  }
  const values = extensionElements.get('values');
  if (!values || !values.length) {
    return [];
  }
  if (type) {
    return values.filter(value => is(value, type));
  }
  return values;
}

export function getProperties(element, namespace = 'activiti') {
  const businessObject = getRelevantBusinessObject(element);
  return getExtensionElementsList(businessObject, `${namespace}:Properties`)[0];
}
export function getPropertiesList(element, namespace = 'activiti') {
  const businessObject = getRelevantBusinessObject(element);
  const properties = getProperties(businessObject, namespace);
  return properties && properties.get(getPropertyName(namespace));
}

export function getListenerBusinessObject(businessObject) {
  if (isAny(businessObject, ['activiti:ExecutionListener', 'activiti:TaskListener', 'activiti:EventListener'])) {
    return businessObject;
  }
}
export function getImplementationType(element) {
  const businessObject = getListenerBusinessObject(element);
  if (!businessObject) {
    return;
  }
  const cls = businessObject.get('activiti:class');
  if (typeof cls !== 'undefined') {
    return 'class';
  }
  const expression = businessObject.get('activiti:expression');
  if (typeof expression !== 'undefined') {
    return 'expression';
  }
  const delegateExpression = businessObject.get('activiti:delegateExpression');
  if (typeof delegateExpression !== 'undefined') {
    return 'delegateExpression';
  }
  const throwEvent = businessObject.get('activiti:throwEvent');
  if (typeof throwEvent !== 'undefined') {
    return 'throwEvent';
  }
  const script = businessObject.get('script');
  if (typeof script !== 'undefined') {
    return 'script';
  }
}
export function removeListenerFactory({ commandStack, element, listener, type }) {
  return function(event) {
    event.stopPropagation();
    // 'activiti:ExecutionListener'
    let businessObject = getBusinessObject(element)
    const extensionElements = businessObject.extensionElements;
    if (!extensionElements) {
      return;
    }
    const parameters = without(extensionElements.get('values'), listener);
    commandStack.execute('element.updateModdleProperties', {
      element,
      moddleElement: extensionElements,
      properties: {
        values: parameters
      }
    });
  };
}
export function addListenerFactory({ element, bpmnFactory, commandStack, type }) {
  return function(event) {
    event.stopPropagation();
    const commands = [];
    const businessObject = getBusinessObject(element);
    let extensionElements = businessObject.get('extensionElements');

    if (!extensionElements) {
      extensionElements = createElement(
          'bpmn:ExtensionElements',
          { values: [] },
          businessObject,
          bpmnFactory
      );

      commands.push({
        cmd: 'element.updateModdleProperties',
        context: {
          element,
          moddleElement: businessObject,
          properties: { extensionElements }
        }
      });
    }
    // 'activiti:ExecutionListener'
    const newListener = createElement(type, {
      name: nextId('exec_lis_'),
      value: ''
    }, extensionElements, bpmnFactory);

    commands.push({
      cmd: 'element.updateModdleProperties',
      context: {
        element,
        moddleElement: extensionElements,
        properties: {
          values: [ ...extensionElements.get('values'), newListener ]
        }
      }
    });
    commandStack.execute('properties-panel.multi-command-executor', commands);
  };
}

export function implementationActivitiListenerDetails(element,id,register,translate,listener,list) {
  const type = getImplementationType(listener)
  let component=[];
  if (type === 'class') {
    component.push(register.findEntries('javaClass',translate))
  }else if (type === 'expression') {
    component.push(register.findEntries('listenerExpression',translate))
  }else if (type === 'delegateExpression') {
    component.push(register.findEntries('delegateExpression',translate))
  }else if (type === 'throwEvent') {
    throwEventTypeProps(element,id,register,translate,listener,component)
  }
  component.push(register.findEntries('fields',translate))
  if(component){
    for (let i = 0; i < component.length; i++) {
      let temp = {...component[i]}
      if(type){
        temp.id = id+"-"+type
      }else{
        temp.id = id+"-"+component[i].id
      }
      temp.businessObject =listener;
      temp.idPrefix = temp.id
      temp.script = listener.get('script')
      temp.element=element
      temp.listener=listener
      temp.register=register
      list.push(temp)
    }
  }
}

export function implementationExecutionListenerDetails(element,id,register,translate,listener,list) {
  const type = getImplementationType(listener)
  let component=[];
  if (type === 'class') {
    component.push(register.findEntries('javaClass',translate))
  }else if (type === 'expression') {
    component.push(register.findEntries('listenerExpression',translate))
  }else if (type === 'delegateExpression') {
    component.push(register.findEntries('delegateExpression',translate))
  }
  // else if (type === 'throwEvent') {
  //   throwEventTypeProps(element,id,register,translate,listener,component)
  // }
  const customType = getCustomImplementationType(listener)
  component.push(register.findEntries('listenerCustomType',translate))
  if (type === 'class') {
    component.push(register.findEntries('customJavaClass',translate))
  }else if (type === 'expression') {
    component.push(register.findEntries('customExpression',translate))
  }else if (type === 'delegateExpression') {
    component.push(register.findEntries('customDelegateExpression',translate))
  }
  component.push(register.findEntries('fields',translate))
  if(component){
    for (let i = 0; i < component.length; i++) {
      let temp = {...component[i]}
      if(type){
        temp.id = id+"-"+type
      }else{
        temp.id = id+"-"+component[i].id
      }
      temp.businessObject =listener;
      temp.idPrefix = temp.id
      temp.script = listener.get('script')
      temp.element=element
      temp.listener=listener
      temp.register=register
      list.push(temp)
    }
  }
}

export function getCustomImplementationType(element) {
  const businessObject = getListenerBusinessObject(element);
  if (!businessObject) {
    return;
  }
  const cls = businessObject.get('activiti:customPropertiesResolverClass');
  if (typeof cls !== 'undefined') {
    return 'class';
  }
  const expression = businessObject.get('activiti:customPropertiesResolverExpression');
  if (typeof expression !== 'undefined') {
    return 'expression';
  }
  const delegateExpression = businessObject.get('activiti:customPropertiesResolverDelegateExpression');
  if (typeof delegateExpression !== 'undefined') {
    return 'delegateExpression';
  }
}

export function throwEventTypeProps(element,id,register,translate,listener,component) {
  const throwEventType = listener.get('throwEvent');
  component.push(register.findEntries('throwEventType',translate))
  if (throwEventType === 'signal'||throwEventType === 'globalSignal') {
    component.push(register.findEntries('signalName',translate))
  }
  if (throwEventType === 'message') {
    component.push(register.findEntries('messageName',translate))
  }
  if (throwEventType === 'error') {
    component.push(register.findEntries('errorCode',translate))
  }
}

export function implementationDetails(element,id,register,translate,listener,list) {
  const type = getImplementationType(listener)
  let component=[];
  if (type === 'class') {
    component.push(register.findEntries('javaClass',translate))
  }else if (type === 'expression') {
    component.push(register.findEntries('listenerExpression',translate))
    // component.push(register.findEntries('listenerResultVariable',translate))
  }else if (type === 'delegateExpression') {
    component.push(register.findEntries('delegateExpression',translate))
  }else if (type === 'script') {
    scriptProps(element,id,register,translate,listener,component)
  }
  component.push(register.findEntries('fields',translate))
  if(component){
    for (let i = 0; i < component.length; i++) {
      let temp = {...component[i]}
      if(type){
        temp.id = id+"-"+type
      }else{
        temp.id = id+"-"+component[i].id
      }
      temp.businessObject =listener;
      temp.idPrefix = temp.id
      temp.script = listener.get('script')
      temp.element=element
      temp.listener=listener
      temp.register=register
      list.push(temp)
    }
  }
}

export function scriptProps(element,id,register,translate,listener,component) {
  const scriptType = getScriptType(listener.get('script'));
  component.push(register.findEntries('format',translate))
  component.push(register.findEntries('scriptType',translate))
  if (scriptType === 'script') {
    component.push(register.findEntries('script',translate))
  }
  if (scriptType === 'resource') {
    component.push(register.findEntries('resource',translate))
  }
}

export function updateConditionExpression(element,businessObject,bpmnFactory,commandStack,body) {
  const commands = [];

  if (businessObject==undefined) {
    businessObject  = createElement(
        'bpmn:FormalExpression',
        body,
        element,
        bpmnFactory
    );
    let properties = {}
    properties['conditionExpression'] = businessObject;
    commands.push({
      cmd: 'element.updateModdleProperties',
      context: {
        element,
        moddleElement: getBusinessObject(element),
        properties
      }
    });
  }
  console.log(businessObject)
  commands.push({
    cmd: 'element.updateModdleProperties',
    context: {
      element,
      moddleElement: businessObject,
      properties: body
    }
  });
  commandStack.execute('properties-panel.multi-command-executor', commands);
}

/**
 * 更新多实例的数据
 * @param element
 * @param businessObject
 * @param bpmnFactory
 * @param commandStack
 * @param props   multiInstanceLoopCharacteristics属性
 * @param bodyElementName   multiInstanceLoopCharacteristics子元素名称
 * @param body  multiInstanceLoopCharacteristics子元素的body内容
 */
export function updateMultiInstance(element,businessObject,bpmnFactory,commandStack,props,bodyElementName,body) {
  const commands = [];
  let loopCharacteristics = businessObject.get('loopCharacteristics');

  // 创建multiInstanceLoopCharacteristics
  if (!loopCharacteristics) {
    loopCharacteristics = createElement(
        'bpmn:MultiInstanceLoopCharacteristics',
        {...props},
        businessObject,
        bpmnFactory
    );

    commands.push({
      cmd: 'element.updateModdleProperties',
      context: {
        element,
        moddleElement: businessObject,
        properties: { loopCharacteristics }
      }
    });
  }
  // 创建loopCharacteristics的子元素
  if(bodyElementName) {
    let values =loopCharacteristics.get('values')
    if (values && values.length>0) {
      values = values.filter(value => is(value, bodyElementName));
    }
    // 创建元素
    if (!values || !values.length) {
      let temp = createElement(
          'bpmn:FormalExpression',
          body,
          loopCharacteristics,
          bpmnFactory
      );
      let properties = {}
      properties[bodyElementName] = temp;
      commands.push({
        cmd: 'element.updateModdleProperties',
        context: {
          element,
          moddleElement: loopCharacteristics,
          properties
        }
      });
    }
  }

  commands.push({
    cmd: 'element.updateModdleProperties',
    context: {
      element,
      moddleElement: loopCharacteristics,
      properties: props
    }
  });
  commandStack.execute('properties-panel.multi-command-executor', commands);
}
export function addMultiInstanceFactory({ element, bpmnFactory, commandStack }) {
  return function(event) {
    event.stopPropagation();
    const commands = [];
    const businessObject = getBusinessObject(element);
    let loopCharacteristics = businessObject.get('loopCharacteristics');

    if (!loopCharacteristics) {
      loopCharacteristics = createElement(
          'bpmn:MultiInstanceLoopCharacteristics',
          {  },
          businessObject,
          bpmnFactory
      );

      commands.push({
        cmd: 'element.updateModdleProperties',
        context: {
          element,
          moddleElement: businessObject,
          properties: { loopCharacteristics }
        }
      });
    }
    commandStack.execute('properties-panel.multi-command-executor', commands);
  };
}
export function removeMultiInstanceFactory({ commandStack, element, loopCharacteristics }) {
  return function(event) {
    event.stopPropagation();
    const businessObject = getBusinessObject(element);
    commandStack.execute('element.updateModdleProperties', {
      element,
      moddleElement: businessObject,
      properties: {
        loopCharacteristics: undefined
      }
    });
  };
}
export function getLoopCharacteristics(businessObject) {
  if(businessObject==undefined){
    return businessObject;
  }
  const loopCharacteristics = businessObject.get('loopCharacteristics');
  if (loopCharacteristics) {
    return loopCharacteristics;
  }
}
export function getLoopCharacteristicsList(businessObject, type = undefined) {
  if(businessObject==undefined){
    return [];
  }
  const loopCharacteristics = businessObject.get('loopCharacteristics');
  if (!loopCharacteristics) {
    return [];
  }
  const values = loopCharacteristics.get('values');
  if (!values || !values.length) {
    return [];
  }
  if (type) {
    return values.filter(value => is(value, type));
  }
  return values;
}
export function removeExtensionElements(element, businessObject, extensionElementsToRemove, commandStack) {
  if (!isArray(extensionElementsToRemove)) {
    extensionElementsToRemove = [extensionElementsToRemove];
  }
  const extensionElements = businessObject.get('extensionElements'),
      values = extensionElements.get('values').filter(value => !extensionElementsToRemove.includes(value));
  commandStack.execute('element.updateModdleProperties', {
    element,
    moddleElement: extensionElements,
    properties: {
      values
    }
  });
}
export function addExtensionElements(element, businessObject, extensionElementToAdd, bpmnFactory, commandStack) {
  const commands = [];
  let extensionElements = businessObject.get('extensionElements');

  // (1) create bpmn:ExtensionElements if it doesn't exist
  if (!extensionElements) {
    extensionElements = createElement('bpmn:ExtensionElements', {
      values: []
    }, businessObject, bpmnFactory);
    commands.push({
      cmd: 'element.updateModdleProperties',
      context: {
        element,
        moddleElement: businessObject,
        properties: {
          extensionElements
        }
      }
    });
  }
  extensionElementToAdd.$parent = extensionElements;

  // (2) add extension element to list
  commands.push({
    cmd: 'element.updateModdleProperties',
    context: {
      element,
      moddleElement: extensionElements,
      properties: {
        values: [...extensionElements.get('values'), extensionElementToAdd]
      }
    }
  });
  commandStack.execute('properties-panel.multi-command-executor', commands);
}
// 构建邮件的各个字段
export function FieldProps(props) {
  const { element, id, filedName, filedType } = props;
  const commandStack = useService('commandStack');
  const bpmnFactory = useService('bpmnFactory');
  const modeling = useService('modeling');
  const translate = useService('translate');
  const debounce = useService('debounceInput');
  const businessObject = getBusinessObject(element)
  let extensionElementsTo = getExtensionElementsAndName(businessObject,'activiti:Field',filedName);
  let field;
  if(extensionElementsTo&&extensionElementsTo.length){
    field = extensionElementsTo[0]
  }
  const getValue = () => {
    if(field) {
      return field.string || field.stringValue || field.expression;
    }
    return '';
  }

  const setValue = value => {
    // 如果值不为空，元素不存在，创建
    if(value&&!field){
      field = createElement('activiti:Field', {name:filedName}, businessObject, bpmnFactory);
      addExtensionElements(element, businessObject, field, bpmnFactory, commandStack);
    }
    // 有值则更新
    if(value){
      let properties = {};
      properties[filedType] = value
      console.log(properties)
      return commandStack.execute('element.updateModdleProperties', {
        element,
        moddleElement: field,
        properties
      });
    }else {
      // 删除元素
      removeExtensionElements(element, businessObject, field, commandStack);
    }
  }
  if(filedName=='html'){
    return TextAreaEntry({
      element,
      id: id + '-'+filedName,
      label: translate(id),
      getValue,
      setValue,
      debounce
    });
  }else {
    return TextFieldEntry({
      element,
      id: id + '-' + filedName,
      label: translate(id),
      getValue,
      setValue,
      debounce
    });
  }
}

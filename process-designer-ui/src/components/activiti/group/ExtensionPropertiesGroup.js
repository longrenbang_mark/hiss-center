import {ListGroup} from "@bpmn-io/properties-panel";
import {createElement, getProperties, getPropertiesList, getPropertyName, getRelevantBusinessObject} from '../util'
import {without} from 'min-dash';

const extensionPropertiesGroup = {
    id: 'extensionProperties',
    label: 'Extension properties',
    component:ListGroup,
    function:function (groups,element, translate,injector,register){
        let a = ExtensionPropertiesProps(
            element,
            translate,
            injector,
            register
        )
        return a;
    }
};
export default extensionPropertiesGroup

function ExtensionPropertiesProps(element,translate,injector,register) {
    let businessObject = getRelevantBusinessObject(element);

    // do not offer for empty pools
    if (!businessObject) {
        return;
    }
    const namespace='activiti'
    const properties = getPropertiesList(businessObject, namespace) || [];
    const bpmnFactory = injector.get('bpmnFactory'),
        commandStack = injector.get('commandStack');
    const items = properties.map((property, index) => {
        const id = element.id + '-extensionProperty-' + index;
        return {
            id,
            label: property.get('name') || '',
            entries: ExtensionProperty(id,property,translate,injector,register),
            autoFocusEntry: id + '-name',
            remove: removeFactoryProperty({
                commandStack,
                element,
                property,
                namespace
            })
        };
    });
    return {
        items,
        add: addFactoryProperty({
            bpmnFactory,
            commandStack,
            element,
            namespace
        }),
        shouldSort: false
    };
}
// 返回每个属性的填写字段
function ExtensionProperty(id,property,translate,injector,register) {
    const arrs = ['propertyName','propertyValue'];
    const entries = []
    for (let i = 0; i < arrs.length; i++) {
        let com = register.findEntries(arrs[i],translate);
        com = {...com}
        com.idPrefix=id + '-'+arrs[i]
        com.property=property
        entries.push(com)
    }
    return entries;
}

function removeFactoryProperty({
                             commandStack,
                             element,
                             property,
                             namespace
                         }) {
    return function (event) {
        event.stopPropagation();
        const commands = [];
        const businessObject = getRelevantBusinessObject(element);
        const extensionElements = businessObject.get('extensionElements');
        const properties = getProperties(businessObject, namespace);
        if (!properties) {
            return;
        }
        const propertyName = getPropertyName(namespace);
        const values = without(properties.get(propertyName), property);
        commands.push({
            cmd: 'element.updateModdleProperties',
            context: {
                element,
                moddleElement: properties,
                properties: {
                    [propertyName]: values
                }
            }
        });

        // remove camunda:Properties if there are no properties anymore
        if (!values.length) {
            commands.push({
                cmd: 'element.updateModdleProperties',
                context: {
                    element,
                    moddleElement: extensionElements,
                    properties: {
                        values: without(extensionElements.get('values'), properties)
                    }
                }
            });
        }
        commandStack.execute('properties-panel.multi-command-executor', commands);
    };
}
function addFactoryProperty({
                          bpmnFactory,
                          commandStack,
                          element,
                          namespace
                      }) {
    return function (event) {
        event.stopPropagation();
        let commands = [];
        const businessObject = getRelevantBusinessObject(element);
        let extensionElements = businessObject.get('extensionElements');
        // (1) ensure extension elements
        if (!extensionElements) {
            extensionElements = createElement('bpmn:ExtensionElements', {
                values: []
            }, businessObject, bpmnFactory);
            commands.push({
                cmd: 'element.updateModdleProperties',
                context: {
                    element,
                    moddleElement: businessObject,
                    properties: {
                        extensionElements
                    }
                }
            });
        }
        const propertyName = getPropertyName(namespace);

        // (2) ensure camunda:Properties
        let properties = getProperties(businessObject, namespace);
        if (!properties) {
            const parent = extensionElements;
            properties = createElement(`${namespace}:Properties`, {
                [propertyName]: []
            }, parent, bpmnFactory);
            commands.push({
                cmd: 'element.updateModdleProperties',
                context: {
                    element,
                    moddleElement: extensionElements,
                    properties: {
                        values: [...extensionElements.get('values'), properties]
                    }
                }
            });
        }

        // (3) create camunda:Property
        const property = createElement(`${namespace}:Property`, {}, properties, bpmnFactory);

        // (4) add property to list
        commands.push({
            cmd: 'element.updateModdleProperties',
            context: {
                element,
                moddleElement: properties,
                properties: {
                    [propertyName]: [...properties.get(propertyName), property]
                }
            }
        });

        // (5) commit all updates
        commandStack.execute('properties-panel.multi-command-executor', commands);
    };
}

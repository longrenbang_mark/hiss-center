import {ListGroup,useLayoutState} from "@bpmn-io/properties-panel";
import {getBusinessObject} from "bpmn-js/lib/util/ModelUtil";
import {createElement} from "../util"

const serviceTaskGroup = {
    id: 'serviceTask',
    label: 'ServiceTask',
    component:ListGroup,
    function:function (groups,element, translate,injector,register){
        let a = ServiceTaskProps({
            element,
            translate,
            injector,
            register
        })
        return a;
    }
};
export default serviceTaskGroup

function ServiceTaskProps(props) {
    const {
        element,injector,register
    } = props;
    const bpmnFactory = injector.get('bpmnFactory');
    const translate = injector.get('translate');
    const businessObject = getBusinessObject(element);
    const items = []
    let id = element.id + '-stask';
    const type = getServiceTaskType(businessObject)||'请选择类型'
    items.push({
        id,
        label: type,
        autoFocusEntry: id+'-serviceTaskType',
        entries: getEntries(element,id,register,translate,businessObject,bpmnFactory),
        autoOpen:true,
        open:true,
        shouldOpen:true
    })
    return {
        items,
        shouldOpen:true
    };
}

function getEntries(element,id,register,translate,businessObject,bpmnFactory){
    let arrs = ['serviceTaskType'];
    const  list = []
    for (let i = 0; i < arrs.length; i++) {
        let entries = register.findEntries(arrs[i],translate)
        entries = {...entries}
        entries.id = id;
        entries.object =businessObject;
        list.push(entries)
    }
    implementationServiceTask(element,id,register,translate,businessObject,list,bpmnFactory);
    return list
}

function getServiceTaskType(businessObject){
    let type = businessObject.type;
    if(type){
        return type;
    }else{
        let expression = businessObject.get('activiti:expression');
        if(expression){
            return 'expression';
        }
        let delegateExpression = businessObject.get('activiti:delegateExpression');
        if(delegateExpression){
            return 'delegateExpression';
        }
        let cla = businessObject.get('activiti:class');
        if(cla){
            return 'class';
        }
    }
}
function implementationServiceTask(element,id,register,translate,businessObject,list,bpmnFactory) {
    let type = getServiceTaskType(businessObject)
    if(type==undefined){
       type='class'
   }
    let component=[];
    if (type === 'class') {
        component.push(register.findEntries('javaClass',translate))
    }else if (type === 'expression') {
        component.push(register.findEntries('listenerExpression',translate))
        component.push(register.findEntries('resultVariableName',translate))
    }else if (type === 'delegateExpression') {
        component.push(register.findEntries('delegateExpression',translate))
    }else if(type){
        component.push(register.findEntries('fields',translate))
    }
    let extensionElements = businessObject.get('extensionElements');
    if (!extensionElements) {
        extensionElements = createElement('bpmn:ExtensionElements', {}, businessObject, bpmnFactory);
        businessObject.set('extensionElements', extensionElements);
    }

    if(component){
        for (let i = 0; i < component.length; i++) {
            let temp = {...component[i]}
            if(type){
                temp.id = id+"-"+type
            }else{
                temp.id = id+"-"+component[i].id
            }
            temp.businessObject =businessObject;
            temp.idPrefix = temp.id
            temp.element=element
            temp.listener=businessObject.get('extensionElements');
            temp.register=register
            list.push(temp)
        }
    }
}

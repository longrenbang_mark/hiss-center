// 定义每个元素的分组信息：包含的字段属性
const activitiMapping = {
    'bpmn:Process':{
        'activitiListenerGroup':[],
        'executionListener':[]
        // ,
        // 'extensionProperties':[]
    },
    'bpmn:StartEvent':{
        'advice':['initiator', 'interrupting']
    },
    'bpmn:UserTask':{
        'advice':['assignee','candidateUsers','candidateGroups','dueDate','priority','skipExpression'],
        'multiInstance':[],
        'taskListener':[]
    },
    'bpmn:ServiceTask':{
        'serviceTask':[]
        // ,
        // 'multiInstance':[]
    },
    'bpmn:ScriptTask':{
        'advice':['format', 'script','resultVariable']
    },
    'bpmn:SequenceFlow':{
        'advice':['conditionExpression']
    },
    'bpmn:MessageEventDefinition':{
        'advice':['correlationKey','messageExpression']
    },
    'bpmn:CallActivity':{
        'advice':['calledElement']
    },
    'bpmn:SendTask':{
        'advice':['mailTo','mailSubject','mailHtml','mailAttachments']
    }
}

export default activitiMapping;

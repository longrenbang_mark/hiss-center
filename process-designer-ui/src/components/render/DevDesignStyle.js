export const STYLE_CONFIG = {
    'complete':{
        frame: {
            fill :'#39c147', // 填充颜色
            strokeWidth:2, // 边框宽度
            stroke:'#39c147', // 边框颜色
            fillOpacity:0.3,// 填充透明度
            strokeOpacity:1,//边框透明度
            opacity:1,// 元素透明度
            strokeLinecap:'round',// 线的两端线帽,butt: round 圆形， square
            strokeLinejoin:'round',//两条先的交点外观 miter | round | bevel | inherit
            strokeDasharray:'4,4',//虚线设置 5,5 表示实线5，空白线5
        },
        shape: {
            fill :'#39c147', // 填充颜色
            strokeWidth:2, // 边框宽度
            stroke:'#39c147', // 边框颜色
            fillOpacity:0.3,// 填充透明度
            strokeOpacity:1,//边框透明度
            opacity:1,// 元素透明度
            strokeLinecap:'round',// 线的两端线帽,butt: round 圆形， square
            strokeLinejoin:'round',//两条先的交点外观 miter | round | bevel | inherit
            strokeDasharray:'',//虚线设置 5,5 表示实线5，空白线5
        },
        connection:{
            fill :'none', // 填充颜色
            strokeWidth:2, // 边框宽度
            stroke:'#39c147', // 边框颜色
            fillOpacity:0.3,// 填充透明度
            strokeOpacity:1,//边框透明度
            opacity:1,// 元素透明度
            strokeLinecap:'round',// 线的两端线帽,butt: round 圆形， square
            strokeLinejoin:'round',//两条先的交点外观 miter | round | bevel | inherit
            strokeDasharray:'',//虚线设置 5,5 表示实线5，空白线5
        },
        label: {
            fill :'none', // 填充颜色
            strokeWidth:1, // 边框宽度
            stroke:'#fb532f', // 边框颜色
            fillOpacity:0,// 填充透明度
            strokeOpacity:1,//边框透明度
            opacity:1,// 元素透明度
            strokeLinecap:'round',// 线的两端线帽,butt: round 圆形， square
            strokeLinejoin:'round',//两条先的交点外观 miter | round | bevel | inherit
            strokeDasharray:'',//虚线设置 5,5 表示实线5，空白线5
            fontFamily: "Arial, sans-serif, 阿里巴巴普惠体 Light,微软雅黑",
            fontSize: '12px',
            fontWeight: 'normal'
        }
    },
    'cancel':{
        frame: {
            fill :'#red', // 填充颜色
            strokeWidth:2, // 边框宽度
            stroke:'#ff0000', // 边框颜色
            fillOpacity:0.3,// 填充透明度
            strokeOpacity:1,//边框透明度
            opacity:1,// 元素透明度
            strokeLinecap:'round',// 线的两端线帽,butt: round 圆形， square
            strokeLinejoin:'round',//两条先的交点外观 miter | round | bevel | inherit
            strokeDasharray:'4,4',//虚线设置 5,5 表示实线5，空白线5
        },
        shape: {
            fill :'#ff0000', // 填充颜色
            strokeWidth:2, // 边框宽度
            stroke:'#ff0000', // 边框颜色
            fillOpacity:0.3,// 填充透明度
            strokeOpacity:1,//边框透明度
            opacity:1,// 元素透明度
            strokeLinecap:'round',// 线的两端线帽,butt: round 圆形， square
            strokeLinejoin:'round',//两条先的交点外观 miter | round | bevel | inherit
            strokeDasharray:'',//虚线设置 5,5 表示实线5，空白线5
        },
        connection:{
            fill :'none', // 填充颜色
            strokeWidth:2, // 边框宽度
            stroke:'#ff0000', // 边框颜色
            fillOpacity:0.3,// 填充透明度
            strokeOpacity:1,//边框透明度
            opacity:1,// 元素透明度
            strokeLinecap:'round',// 线的两端线帽,butt: round 圆形， square
            strokeLinejoin:'round',//两条先的交点外观 miter | round | bevel | inherit
            strokeDasharray:'',//虚线设置 5,5 表示实线5，空白线5
        },
        label: {
            fill :'none', // 填充颜色
            strokeWidth:1, // 边框宽度
            stroke:'#ff0000', // 边框颜色
            fillOpacity:0,// 填充透明度
            strokeOpacity:1,//边框透明度
            opacity:1,// 元素透明度
            strokeLinecap:'round',// 线的两端线帽,butt: round 圆形， square
            strokeLinejoin:'round',//两条先的交点外观 miter | round | bevel | inherit
            strokeDasharray:'',//虚线设置 5,5 表示实线5，空白线5
            fontFamily: "Arial, sans-serif, 阿里巴巴普惠体 Light,微软雅黑",
            fontSize: '12px',
            fontWeight: 'normal'
        }
    },
    'active':{
        frame: {
            fill :'#e87f73', // 填充颜色
            strokeWidth:2, // 边框宽度
            stroke:'#e87f73', // 边框颜色
            fillOpacity:0.15,// 填充透明度
            strokeOpacity:1,//边框透明度
            opacity:1,// 元素透明度
            strokeLinecap:'round',// 线的两端线帽,butt: round 圆形， square
            strokeLinejoin:'round',//两条先的交点外观 miter | round | bevel | inherit
            strokeDasharray:'',//虚线设置 5,5 表示实线5，空白线5
        },
        shape: {
            fill :'#e87f73', // 填充颜色
            strokeWidth:2, // 边框宽度
            stroke:'#e87f73', // 边框颜色
            fillOpacity:0.15,// 填充透明度
            strokeOpacity:1,//边框透明度
            opacity:1,// 元素透明度
            strokeLinecap:'round',// 线的两端线帽,butt: round 圆形， square
            strokeLinejoin:'round',//两条先的交点外观 miter | round | bevel | inherit
            strokeDasharray:'',//虚线设置 5,5 表示实线5，空白线5
        },
        connection:{
            fill :'none', // 填充颜色
            strokeWidth:2, // 边框宽度
            stroke:'#e87f73', // 边框颜色
            fillOpacity:0.3,// 填充透明度
            strokeOpacity:1,//边框透明度
            opacity:1,// 元素透明度
            strokeLinecap:'round',// 线的两端线帽,butt: round 圆形， square
            strokeLinejoin:'round',//两条先的交点外观 miter | round | bevel | inherit
            strokeDasharray:'',//虚线设置 5,5 表示实线5，空白线5
        },
        label: {
            fill :'none', // 填充颜色
            strokeWidth:1, // 边框宽度
            stroke:'#fb532f', // 边框颜色
            fillOpacity:0,// 填充透明度
            strokeOpacity:1,//边框透明度
            opacity:1,// 元素透明度
            strokeLinecap:'round',// 线的两端线帽,butt: round 圆形， square
            strokeLinejoin:'round',//两条先的交点外观 miter | round | bevel | inherit
            strokeDasharray:'',//虚线设置 5,5 表示实线5，空白线5
            fontFamily: "Arial, sans-serif, 阿里巴巴普惠体 Light,微软雅黑",
            fontSize: '12px',
            fontWeight: 'normal'
        }
    },
    'pending':{
        frame: {
            fill :'#fb532f', // 填充颜色
            strokeWidth:2, // 边框宽度
            stroke:'#fb532f', // 边框颜色
            fillOpacity:0.3,// 填充透明度
            strokeOpacity:1,//边框透明度
            opacity:1,// 元素透明度
            strokeLinecap:'round',// 线的两端线帽,butt: round 圆形， square
            strokeLinejoin:'round',//两条先的交点外观 miter | round | bevel | inherit
            strokeDasharray:'4,4',//虚线设置 5,5 表示实线5，空白线5
        },
        shape: {
            fill :'#fb532f', // 填充颜色
            strokeWidth:2, // 边框宽度
            stroke:'#fb532f', // 边框颜色
            fillOpacity:0.3,// 填充透明度
            strokeOpacity:1,//边框透明度
            opacity:1,// 元素透明度
            strokeLinecap:'round',// 线的两端线帽,butt: round 圆形， square
            strokeLinejoin:'round',//两条先的交点外观 miter | round | bevel | inherit
            strokeDasharray:'',//虚线设置 5,5 表示实线5，空白线5
        },
        connection:{
            fill :'none', // 填充颜色
            strokeWidth:2, // 边框宽度
            stroke:'#fb532f', // 边框颜色
            fillOpacity:0.3,// 填充透明度
            strokeOpacity:1,//边框透明度
            opacity:1,// 元素透明度
            strokeLinecap:'round',// 线的两端线帽,butt: round 圆形， square
            strokeLinejoin:'round',//两条先的交点外观 miter | round | bevel | inherit
            strokeDasharray:'',//虚线设置 5,5 表示实线5，空白线5
        },
        label: {
            fill :'none', // 填充颜色
            strokeWidth:1, // 边框宽度
            stroke:'#fb532f', // 边框颜色
            fillOpacity:0,// 填充透明度
            strokeOpacity:1,//边框透明度
            opacity:1,// 元素透明度
            strokeLinecap:'round',// 线的两端线帽,butt: round 圆形， square
            strokeLinejoin:'round',//两条先的交点外观 miter | round | bevel | inherit
            strokeDasharray:'',//虚线设置 5,5 表示实线5，空白线5
            fontFamily: "Arial, sans-serif, 阿里巴巴普惠体 Light,微软雅黑",
            fontSize: '12px',
            fontWeight: 'normal'
        }
    },
    'default':{
        frame: {
            fill :'#060606', // 填充颜色
            strokeWidth:2, // 边框宽度
            stroke:'#060606', // 边框颜色
            fillOpacity:0,// 填充透明度
            strokeOpacity:1,//边框透明度
            opacity:1,// 元素透明度
            strokeLinecap:'round',// 线的两端线帽,butt: round 圆形， square
            strokeLinejoin:'round',//两条先的交点外观 miter | round | bevel | inherit
            strokeDasharray:'4,4',//虚线设置 5,5 表示实线5，空白线5
        },
        shape: {
            fill :'#060606', // 填充颜色
            strokeWidth:2, // 边框宽度
            stroke:'#060606', // 边框颜色
            fillOpacity:0,// 填充透明度
            strokeOpacity:1,//边框透明度
            opacity:1,// 元素透明度
            strokeLinecap:'round',// 线的两端线帽,butt: round 圆形， square
            strokeLinejoin:'round',//两条先的交点外观 miter | round | bevel | inherit
            strokeDasharray:'',//虚线设置 5,5 表示实线5，空白线5
        },
        connection:{
            fill :'none', // 填充颜色
            strokeWidth:2, // 边框宽度
            stroke:'#060606', // 边框颜色
            fillOpacity:0,// 填充透明度
            strokeOpacity:1,//边框透明度
            opacity:1,// 元素透明度
            strokeLinecap:'round',// 线的两端线帽,butt: round 圆形， square
            strokeLinejoin:'round',//两条先的交点外观 miter | round | bevel | inherit
            strokeDasharray:'',//虚线设置 5,5 表示实线5，空白线5
        },
        label: {
            fill :'none', // 填充颜色
            strokeWidth:1, // 边框宽度
            stroke:'#060606', // 边框颜色
            fillOpacity:0,// 填充透明度
            strokeOpacity:1,//边框透明度
            opacity:1,// 元素透明度
            strokeLinecap:'round',// 线的两端线帽,butt: round 圆形， square
            strokeLinejoin:'round',//两条先的交点外观 miter | round | bevel | inherit
            strokeDasharray:'',//虚线设置 5,5 表示实线5，空白线5
            fontFamily: "Arial, sans-serif, 阿里巴巴普惠体 Light,微软雅黑",
            fontSize: '12px',
            fontWeight: 'normal'
}
    }
}

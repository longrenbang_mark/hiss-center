import CustomBisRenderer from './CustomBisRenderer';

export default {
  __init__: [ 'customRenderer' ],
  customRenderer: [ 'type', CustomBisRenderer ]
};

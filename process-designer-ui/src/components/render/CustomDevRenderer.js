import BaseRenderer from 'diagram-js/lib/draw/BaseRenderer';

import {attr as svgAttr} from 'tiny-svg';
import {assign} from 'min-dash';
import {isFrameElement} from 'diagram-js/lib/util/Elements';
import {STYLE_CONFIG} from './DevDesignStyle';
import {is} from "bpmn-js/lib/util/ModelUtil";
import {ElPopper} from 'element-plus'
const HIGH_PRIORITY = 1500;


export default class CustomDevRenderer extends BaseRenderer {
  constructor(eventBus, bpmnRenderer,styles,canvas) {
    super(eventBus, HIGH_PRIORITY);
    this.canvas = canvas
    this.bpmnRenderer = bpmnRenderer;
    this.STYLE_CONFIG = STYLE_CONFIG;
    this.eventBus = eventBus;
    this.styles = styles;
    // 显示tooltip的回调
    this.tooltip;
    // 当前需要高亮的ID,已经提示的文字,status = complete完成，active激活（正在办理）,pending挂起（不可办理）
    this.highlight = {}
    // 例子
    // {'Activity_1xi315o':{
    //     status:'active',
    //     info:{
    //       title:'办理人',
    //       content:'罗学勇'
    //     }
    //   },'Flow_109kst5': {
    //     status:'complete'
    //   },'StartEvent_1':{
    //     status:'complete',
    //     info:{
    //       title:'发起人',
    //       content:'罗学勇'
    //     }
    //   }
    // }
  }

  canRender(element) {
    return true;
  }

  setHighlight(ids){
    this.highlight = ids || {};
  }

  setTooltip(tooltip){
    this.tooltip = tooltip;
  }

  getStyle(element,isLine){
    const id = element.id
    const config = this.highlight[id]?this.highlight[id][0]:{status:'default'}
    config.status = config.status.toLowerCase()
    const styleConfig = this.STYLE_CONFIG[config.status];
    let style = {}
    if(isLine){
      style = styleConfig.connection
      element.di.set('fill',style['fill'])
      element.di.set('stroke',style['stroke'])
    }else{
      // 线上文字的样式
      if(element.type=='label'){
        style = styleConfig.label;
        return style;
      }
      style = isFrameElement(element)?styleConfig.frame:styleConfig.shape;
      // 回调鼠标悬停事件
      if(config.tipTitle&&this.tooltip){
        this.eventBus.on('element.hover', (e)=>{
          if(e.element==element){
            this.tooltip.call(this,element,e,config.nodeId,true)
          }
        });
        this.eventBus.on('element.out', (e)=>{
          if(e.element==element){
            this.tooltip.call(this,element,e,config.nodeId,false)
          }
        });
      }
    }
    if(config.status){
      this.canvas.addMarker(element, 'hp-highlight-'+config.status)
    }
    return style;
  }

  drawShape(pNode, element) {
    const shape = this.bpmnRenderer.drawShape(pNode, element);
    svgAttr(shape, this.getStyle(element,false));
    return shape;
  }

  drawConnection(visuals, connection, attrs) {
    let style = this.getStyle(connection,true);
    const line = this.bpmnRenderer.drawConnection(visuals, connection, attrs);
    svgAttr(line, style);
    return line;
  };

}
CustomDevRenderer.$inject = [ 'eventBus','bpmnRenderer', 'styles','canvas'];

import translations from './translationsGerman'
export default function customTranslate (template, replacements) {
  replacements = replacements || {}
  if(!template){
    return ''
  }
  template = translations[template] || template
  if(!template){
    return ''
  }
  return template.replace(/{([^}]+)}/g, function (_, key) {
    var str = replacements[key]
    if (translations[replacements[key]] != null && translations[replacements[key]] !== 'undefined') {
      str = translations[replacements[key]]
    }
    return str || '{' + key + '}'
  })
}

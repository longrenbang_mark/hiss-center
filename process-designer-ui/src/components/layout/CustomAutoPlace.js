import { getNewShapePosition } from 'bpmn-js/lib/features/auto-place/BpmnAutoPlaceUtil';
import {
    getMid,
    asTRBL,
    getOrientation
} from 'diagram-js/lib/layout/LayoutUtil';

import {
    findFreePosition,
    generateGetNextPosition,
    getConnectedDistance
} from 'diagram-js/lib/features/auto-place/AutoPlaceUtil';


// {import('diagram-js/lib/util/Types').Point} Point

const DEFAULT_DISTANCE = 100
/**
 * BPMN auto-place behavior.
 *
 * @param {EventBus} eventBus
 */
export default function CustomAutoPlace(eventBus,modeling,canvas) {
    eventBus.on('autoPlace',1000000000 , function(context) {
        var shape = context.shape,
            source = context.source;
        return getNewShapePosition(source, shape);
    });


    this.append = function(source, shape, hints) {
        eventBus.fire('autoPlace.start', {
            source: source,
            shape: shape
        });

        // allow others to provide the position
        var position = eventBus.fire('autoPlace', {
            source: source,
            shape: shape
        });

        var newShape = modeling.appendShape(source, shape, position, source.parent, hints);

        eventBus.fire('autoPlace.end', {
            source: source,
            shape: newShape
        });
        console.log(111,hints)
        console.log(222,newShape)
        console.log(333,modeling)
        return newShape;
    };

    function getNewShapePosition(source, element, hints) {
        if (!hints) {
            hints = {};
        }

        var distance = hints.defaultDistance || DEFAULT_DISTANCE;

        var sourceMid = getMid(source),
            sourceTrbl = asTRBL(source);

        // simply put element right next to source
        // return {
        //     x: sourceTrbl.right + distance + element.width / 2,
        //     y: sourceMid.y
        // };
        if(window['hiss_bis_dir']){
            var position = {
                y: sourceTrbl.bottom + distance + element.height / 2,
                x: sourceMid.x
            };

            var nextPositionDirection = {
                x: {
                    margin: DEFAULT_DISTANCE,
                    minDistance: 30
                }
            };

            return findFreePosition(source, element, position, generateGetNextPosition(nextPositionDirection));
        }else{
            var position = {
                x: sourceTrbl.right + distance + element.width / 2,
                y: sourceMid.y
            };

            var nextPositionDirection = {
                x: {
                    margin: 30,
                    minDistance:DEFAULT_DISTANCE
                }
            };

            return findFreePosition(source, element, position, generateGetNextPosition(nextPositionDirection));
        }
    }
}

CustomAutoPlace.$inject = [ 'eventBus','modeling','canvas' ];


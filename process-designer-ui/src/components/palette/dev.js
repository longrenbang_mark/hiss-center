import CustomDevPalette from './CustomDevPalette';
import CustomDevContextPadProvider from "@/components/palette/CustomDevContextPadProvider";
import CustomDevReplaceMenuProvider from "@/components/palette/CustomDevReplaceMenuProvider";

export default {
  __init__: [ 'paletteProvider','contextPadProvider','replaceMenuProvider'],
  paletteProvider:['type', CustomDevPalette],
  replaceMenuProvider:['type', CustomDevReplaceMenuProvider],
  contextPadProvider:['type',CustomDevContextPadProvider]
};

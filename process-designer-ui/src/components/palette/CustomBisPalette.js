import {
  assign,omit
} from 'min-dash';

import businessConfig from '../business/config'

export default function PaletteProvider(palette, create, elementFactory, globalConnect,spaceTool, lassoTool) {
  this.create = create
  this.elementFactory = elementFactory
  this.globalConnect = globalConnect
  this.spaceTool = spaceTool;
  this.lassoTool = lassoTool;

  palette.registerProvider(this)
}

PaletteProvider.$inject = [
  'palette',
  'create',
  'elementFactory',
  'globalConnect',
  'spaceTool', 'lassoTool'
]


PaletteProvider.prototype.getPaletteEntries = function(element) { // 此方法和上面案例的一样
  const {
    create,
    elementFactory
  } = this;
  var actions = {}
  var _this = this;

  function createAction(type, group, className, title, options) {

    function createListener(event) {
      var shape = elementFactory.createShape(assign({ type: type }, options));
      if(options&&options.hissType){
        shape.di.bpmnElement.$attrs['hissType' ] = options.hissType;
        shape.di.bpmnElement.$attrs['hissTypeName' ] = businessConfig[options.hissType]['name'];
        shape.di.bpmnElement.$attrs['hissColor' ] = businessConfig[options.hissType]['bgColor'];
      }
      create.start(event, shape);
    }

    var shortType = type.replace(/^bpmn:/, '');
    return {
      group: group,
      className: className,
      title: title || 'Create ' + shortType,
      action: {
        dragstart: createListener,
        click: createListener
      }
    };
  }

  function createTask() {
    return function (event) {
      const shape = elementFactory.createShape({
        type: 'bpmn:Task'
      });
      create.start(event, shape);
    }
  }

  for(var key in businessConfig){
    let obj = businessConfig[key]
    const action = createAction('bpmn:UserTask', 'hiss', 'hiss-task-'+key,obj.title,{hissType:key })
    actions['hiss-'+key]=action;
  }

  assign(actions, {
    'custom-separator': {
      group: 'custom',
      separator: true
    },
    'lasso-tool': {
      group: 'tools',
      className: 'bpmn-icon-lasso-tool',
      title: 'Activate the lasso tool',
      action: {
        click: function (event) {
          _this.lassoTool.activateSelection(event);
        }
      }
    },
    'space-tool': {
      group: 'tools',
      className: 'bpmn-icon-space-tool',
      title: 'Activate the create/remove space tool',
      action: {
        click: function (event) {
          _this.spaceTool.activateSelection(event);
        }
      }
    }

  });
  return actions;
}


import ContextPadProvider, {ContextPadConfig, Translate} from 'bpmn-js/lib/features/context-pad/ContextPadProvider';
import {
  assign,
  forEach,
  isArray,
  every
} from 'min-dash';
import businessConfig from "@/components/business/config";
import CUSTOM_TYPE_CONFIG from "@/components/business/config";

export default class CustomBisContextPadProvider extends ContextPadProvider {
  constructor(config, injector, eventBus, contextPad, modeling, elementFactory, connect, create, popupMenu, canvas, rules, translate){
    super(config, injector, eventBus, contextPad, modeling, elementFactory, connect, create, popupMenu, canvas, rules, translate)
    this.create = create;
    this.elementFactory = elementFactory;
    this.translate = translate;
    this.rules = rules;
    this.modeling = modeling;
    config = config || {};
    if (config.autoPlace !== false) {
      // this.autoPlace = injector.get('autoPlace', false);
      this.autoPlace = injector.get('customAutoPlace', false);
    }
    contextPad.registerProvider(this);
  }

  getContextPadEntries(element) {
    const {
      autoPlace,
      create,
      elementFactory,
      translate,
      contextPad
    } = this;
    const temps = super.getContextPadEntries(element);
    const actions = {}
    for(var key in temps){
      if(key=='delete'||key=='append.text-annotation'||key=='connect'){
        actions[key]=temps[key];
      }
    }
    let _autoPlace = this.autoPlace;
    function appendAction(type, className, title, options) {
      if (typeof title !== 'string') {
        options = title;
        title = translate('Append {type}', { type: type.replace(/^bpmn:/, '') });
      }

      function appendStart(event, element) {
        var shape = elementFactory.createShape(assign({ type: type }, options));
        if(options&&options.hissType){
          shape.di.bpmnElement.$attrs['hissType' ] = options.hissType;
          shape.di.bpmnElement.$attrs['hissTypeName' ] = businessConfig[options.hissType]['name'];
          shape.di.bpmnElement.$attrs['hissColor' ] = businessConfig[options.hissType]['bgColor'];
        }
        create.start(event, shape, {
          source: element
        });
      }

      var append = function(event, element) {
        var shape = elementFactory.createShape(assign({ type: type }, options));
        if(options&&options.hissType){
          shape.di.bpmnElement.$attrs['hissType' ] = options.hissType;
          shape.di.bpmnElement.$attrs['hissTypeName' ] = businessConfig[options.hissType]['name'];
          shape.di.bpmnElement.$attrs['hissColor' ] = businessConfig[options.hissType]['bgColor'];
        }
        _autoPlace.append(element, shape);
      };

      return {
        group: 'model',
        className: className,
        title: title,
        action: {
          dragstart: appendStart,
          click: append
        }
      };
    }

    assign(actions, {
      'append.starter' : appendAction(
          'bpmn:UserTask',
          'hiss-task-p-starter',
          translate('Append starterTask'),
          {hissType:'starter' }
      )})

    assign(actions, {
      'append.single' : appendAction(
          'bpmn:UserTask',
          'hiss-task-p-single',
          translate('Append singleTask'),
          {hissType:'singleApprove' }
      )})

    assign(actions, {
      'append.multiple' : appendAction(
          'bpmn:UserTask',
          'hiss-task-p-multiple',
          translate('Append multipleTask'),
          {hissType:'multipleApprove' }
      )})

    assign(actions, {
      'append.notifier' : appendAction(
          'bpmn:UserTask',
          'hiss-task-p-notifier',
          translate('Append notifierTask'),
          {hissType:'notifier' }
      )})

    assign(actions, {
      'append.condition' : appendAction(
          'bpmn:UserTask',
          'hiss-task-p-condition',
          translate('Append conditionTask'),
          {hissType:'condition' }
      )})

    assign(actions, {
      'append.cc' : appendAction(
          'bpmn:UserTask',
          'hiss-task-p-cc',
          translate('Append ccTask'),
          {hissType:'cc' }
      )})

    return actions;
  }

}

CustomBisContextPadProvider.$inject = [
  'config.contextPad',
  'injector',
  'eventBus',
  'contextPad',
  'modeling',
  'elementFactory',
  'connect',
  'create',
  'popupMenu',
  'canvas',
  'rules',
  'translate'
];

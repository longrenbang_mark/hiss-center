
import ContextPadProvider, {ContextPadConfig, Translate} from 'bpmn-js/lib/features/context-pad/ContextPadProvider';
import {
  assign,
  forEach,
  isArray,
  every
} from 'min-dash';

export default class CustomBisContextPadProvider extends ContextPadProvider {
  constructor(config, injector, eventBus, contextPad, modeling, elementFactory, connect, create, popupMenu, canvas, rules, translate){
    super(config, injector, eventBus, contextPad, modeling, elementFactory, connect, create, popupMenu, canvas, rules, translate)
    this.create = create;
    this.elementFactory = elementFactory;
    this.translate = translate;
    this.rules = rules;
    this.modeling = modeling;
    this.popupMenu = popupMenu
    config = config || {};
    if (config.autoPlace !== false) {
      this._autoPlace = injector.get('autoPlace', false);
    }
    contextPad.registerProvider(this);
  }

  getContextPadEntries(element) {
    const {
      create,
      elementFactory,
      translate,
      contextPad
    } = this;
    const actions = super.getContextPadEntries(element);
    function appendStart(event, element) {
      var shape = elementFactory.createShape(assign({ type: 'bpmn:UserTask'}));
      create.start(event, shape, {
        source: element
      });
    }
    let autoPlace = this._autoPlace;
    var append =  function(event, element) {
      var shape = elementFactory.createShape(assign({ type: 'bpmn:UserTask'}));
      autoPlace.append(element, shape);
    } ;
    const task = actions['append.append-task'];
    if(task){
      task.className="bpmn-icon-user-task"
      task.action={
        // dragstart: appendStart,
        click: append
      }
    }
    return actions;
  }
}

CustomBisContextPadProvider.$inject = [
  'config.contextPad',
  'injector',
  'eventBus',
  'contextPad',
  'modeling',
  'elementFactory',
  'connect',
  'create',
  'popupMenu',
  'canvas',
  'rules',
  'translate'
];

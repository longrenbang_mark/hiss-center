import CustomBisContextPadProvider from './CustomBisContextPadProvider';
import CustomBisPalette from './CustomBisPalette';

export default {
  __init__: [ 'paletteProvider','contextPadProvider'],
  paletteProvider:['type', CustomBisPalette],
  contextPadProvider:['type',CustomBisContextPadProvider]
};

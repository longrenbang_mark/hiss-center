import ElementFactory, {Translate} from 'bpmn-js/lib/features/modeling/ElementFactory';

import {
  append as svgAppend,
  attr as svgAttr,
  create as svgCreate,
  remove as svgRemove,
  clear as clearChildren,
  classes as svgClasses,
  select as selectNode,
  selectAll as selectNodeAll
} from 'tiny-svg';

import {
  getRoundRectPath
} from 'bpmn-js/lib/draw/BpmnRenderUtil';

import {getBusinessObject, getDi, is} from 'bpmn-js/lib/util/ModelUtil';
import { isAny } from 'bpmn-js/lib/features/modeling/util/ModelingUtil';
import {isExpanded} from "bpmn-js/lib/util/DiUtil";

export default class CustomElementFactory extends ElementFactory {

  constructor(bpmnFactory, moddle, translate){
    super(bpmnFactory,moddle,translate);
  }

  getDefaultSize(element, di) {
    var bo = getBusinessObject(element);
    di = di || getDi(element);

    if (is(bo, 'bpmn:SubProcess')) {
      if (isExpanded(bo, di)) {
        return { width: 350, height: 200 };
      } else {
        return { width: 100, height: 80 };
      }
    }

    if (is(bo, 'bpmn:Task')) {
      return { width: 200, height: 80 };
    }

    if (is(bo, 'bpmn:Gateway')) {
      return { width: 50, height: 50 };
    }

    if (is(bo, 'bpmn:Event')) {
      return { width: 36, height: 36 };
    }

    if (is(bo, 'bpmn:Participant')) {
      if (isExpanded(bo, di)) {
        return { width: 600, height: 250 };
      } else {
        return { width: 400, height: 60 };
      }
    }

    if (is(bo, 'bpmn:Lane')) {
      return { width: 400, height: 100 };
    }

    if (is(bo, 'bpmn:DataObjectReference')) {
      return { width: 36, height: 50 };
    }

    if (is(bo, 'bpmn:DataStoreReference')) {
      return { width: 50, height: 50 };
    }

    if (is(bo, 'bpmn:TextAnnotation')) {
      return { width: 100, height: 30 };
    }

    if (is(bo, 'bpmn:Group')) {
      return { width: 300, height: 300 };
    }

    return { width: 200, height: 80 };
  };

}

CustomElementFactory.$inject = [ 'bpmnFactory', 'moddle','translate' ];

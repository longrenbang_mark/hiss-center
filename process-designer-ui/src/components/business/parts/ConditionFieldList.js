import {CollapsibleEntry, ListEntry} from "@bpmn-io/properties-panel";
import {getBusinessObject} from "bpmn-js/lib/util/ModelUtil";
import {getFormFields} from '@/api/BusinessForDesignApi'
import {useService} from "bpmn-js-properties-panel";
import {jsx} from "@bpmn-io/properties-panel/preact/jsx-runtime";
import {getBusinessPeopleFieldValue} from "@/components/business/util";

const component = {
  id: 'conditionFieldList',
  component: ConditionFieldList,
  shouldOpen:true
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}

export default function(element) {
  component['element']=element;
  return component;
}
function ConditionFieldList(props) {
  const { element, id,register,property,defaultFormAuthValue } = props;
  const bpmnFactory = useService('bpmnFactory');
  const commandStack = useService('commandStack');
  const translate = useService('translate');
  const propertyBusinessObject =  getBusinessObject(property);
  if(propertyBusinessObject) {
    const value = propertyBusinessObject.get('value')
    if(value) {
      let temp = eval('('+value+')')
      // 远程查询表单字段列表
      let respones = [];
      respones.push({id:'$$starter',"name":'发起人'})
      respones.push({id:'$$prevApprover',"name":'上一步审批人'})
      let tempFields = getFormFields(temp.id,temp.type);// 添加默认的条件设置
      if(tempFields){
        for (let i = 0; i < tempFields.length; i++) {
          respones.push(tempFields[i])
        }
      }

      if(respones){
        return jsx(ListEntry, {
          id: id,
          register: register,
          translate: translate,
          element: element,
          label: translate('ConditionFields'),
          items: respones,
          component: FieldCondition,
          formData:temp,
          showAdd:false,
          autoFocusEntry: true,
          shouldOpen:true,
          open:true
        });
      }
    }
  }
}
function FieldCondition(props) {
  const {
    element,
    id,
    index,
    formData,
    item: field,
    open,
    register
  } = props;
  const fieldId = `${id}-cfield-${index}`;
  return jsx(CollapsibleEntry, {
    id: fieldId,
    element: element,
    entries: FieldInjection({
      element,
      field,
      register,
      formData,
      idPrefix: fieldId
    }),
    label: field.name || '<empty>',
    open: true,
    shouldOpen:true
  });
}

function FieldInjection(props) {
  const {
    element,
    idPrefix,
    field,
    formData,
    register,
    translate
  } = props;
  let arrs = ['conditionFieldType','conditionFieldValue1'];
  let filedName = getFiledName(field,'conditionFieldType',formData)
  let type = getBusinessPeopleFieldValue(getBusinessObject(element),filedName)
  if(type&&type=='between'){
    arrs.push('conditionFieldValue2')
  }
  const list = []
  for (let i = 0; i < arrs.length; i++) {
    let entries = register.findEntries(arrs[i],translate)
    entries = {...entries}
    entries.id = idPrefix+"_"+arrs[i];
    entries.element =element;
    entries.field =field;
    entries.idPrefix =idPrefix;
    entries.property=field
    entries.filedName=getFiledName(field,arrs[i],formData)
    entries.formData=formData
    entries.label='  '
    list.push(entries)
  }
  return list;
}


function getFiledName(field,type,formData){
  return field.id+'#hiss#'+formData.id+type.replace('conditionField','#hiss#')
}

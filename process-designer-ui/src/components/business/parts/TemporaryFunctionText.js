import {CheckboxProps, FieldProps} from "@/components/business/util";

const component = {
  id: 'temporaryFunctionText',
  component: FieldProps,
  filedName:'temporaryFunctionText',
  filedType:'string',
  labelPostion:'right',
  placeholder:'temporaryFunctionTextPlaceholder'
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

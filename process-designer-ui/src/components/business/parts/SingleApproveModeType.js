import {FieldCommonSelect} from "@/components/business/util";
import Constants from "@/components/business/constants";

const component = {
  id: 'singleApproveModeType',
  component: FieldCommonSelect,
  filedName:Constants.hissBusinessSingleMode,
  filedType:'string',
  defaultValue:Constants.values.singleModeManual,
  getOptions :function(translate) {
    return [{
      value: Constants.values.singleModeManual,
      label: translate(Constants.values.singleModeManual)
    }, {
      value: Constants.values.singleModeAutoApproval,
      label: translate(Constants.values.singleModeAutoApproval)
    }, {
      value: Constants.values.singleModeAutoRejection,
      label: translate(Constants.values.singleModeAutoRejection)
    }];
  }
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}

export default function(element) {
  component['element']=element;
  return component;
}


import {FieldCommonSelect} from "@/components/business/util";
import Constants from "@/components/business/constants";

const component = {
  id: 'starterPeopleType',
  component: FieldCommonSelect,
  filedName:Constants.hissBusinessStarterUserType,
  filedType:'string',
  getOptions :function(translate) {
    return [{
      value: Constants.values.currentLoginUser,
      label: translate(Constants.values.currentLoginUser)
    }, {
      value: Constants.values.fixedUser,
      label: translate(Constants.values.fixedUser)
    }];
  }
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}

export default function(element) {
  component['element']=element;
  return component;
}


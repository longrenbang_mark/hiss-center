import {CheckboxProps, FieldProps} from "@/components/business/util";

const component = {
  id: 'addSignatureFunction',
  component: CheckboxProps,
  filedName:'addSignatureFunction',
  filedType:'string'
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

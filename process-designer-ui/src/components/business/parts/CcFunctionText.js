import {CheckboxProps, FieldProps} from "@/components/business/util";

const component = {
  id: 'ccFunctionText',
  component: FieldProps,
  filedName:'ccFunctionText',
  filedType:'string',
  labelPostion:'right',
  placeholder:'ccFunctionTextPlaceholder'
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

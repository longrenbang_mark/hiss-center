import {CheckboxProps, FieldProps} from "@/components/business/util";

const component = {
  id: 'delegateFunction',
  component: CheckboxProps,
  filedName:'delegateFunction',
  filedType:'string'
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

import {CheckboxProps, FieldProps} from "@/components/business/util";

const component = {
  id: 'unPassFunctionText',
  component: FieldProps,
  filedName:'unPassFunctionText',
  filedType:'string',
  labelPostion:'right',
  placeholder:'unPassFunctionTextPlaceholder'
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

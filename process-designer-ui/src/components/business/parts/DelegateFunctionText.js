import {CheckboxProps, FieldProps} from "@/components/business/util";

const component = {
  id: 'delegateFunctionText',
  component: FieldProps,
  filedName:'delegateFunctionText',
  filedType:'string',
  labelPostion:'right',
  placeholder:'delegateFunctionTextPlaceholder'
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

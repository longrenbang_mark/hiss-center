import {CheckboxProps, FieldProps} from "@/components/business/util";

const component = {
  id: 'informFunctionText',
  component: FieldProps,
  filedName:'informFunctionText',
  filedType:'string',
  labelPostion:'right',
  placeholder:'informFunctionText'
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

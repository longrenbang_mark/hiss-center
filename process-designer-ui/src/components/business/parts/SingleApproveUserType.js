import {FieldCommonSelect} from "@/components/business/util";
import Constants from "@/components/business/constants";

const component = {
  id: 'singleApproveUserType',
  component: FieldCommonSelect,
  filedName:Constants.hissBusinessSingleUserType,
  filedType:'string',
  defaultValue:Constants.values.singleUserTypeDirectLeader,
  getOptions :function(translate) {
    return [{
      value: Constants.values.singleUserTypeDirectLeader,
      label: translate(Constants.values.singleUserTypeDirectLeader)
    }, {
      value: Constants.values.singleUserTypeDirectDepartment,
      label: translate(Constants.values.singleUserTypeDirectDepartment)
    },{
      value: Constants.values.singleUserTypeDesignatedUser,
      label: translate(Constants.values.singleUserTypeDesignatedUser)
    }, {
      value: Constants.values.singleUserTypeDesignatedRole,
      label: translate(Constants.values.singleUserTypeDesignatedRole)
    },{
      value: Constants.values.singleUserTypeDesignatedDepartment,
      label: translate(Constants.values.singleUserTypeDesignatedDepartment)
    }, {
      value: Constants.values.singleUserTypeOwnerSelect,
      label: translate(Constants.values.singleUserTypeOwnerSelect)
    },{
      value: Constants.values.singleUserTypeOwner,
      label: translate(Constants.values.singleUserTypeOwner)
    }];
  }
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}

export default function(element) {
  component['element']=element;
  return component;
}


import BusinessPropertiesProvider from './BusinessPropertiesProvider.js';

export default {
  __init__: [ 'businessPropertiesProvider' ],
  businessPropertiesProvider: [ 'type', BusinessPropertiesProvider ]
};

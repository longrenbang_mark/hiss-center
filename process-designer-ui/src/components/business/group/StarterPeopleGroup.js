import {ListGroup} from "@bpmn-io/properties-panel";
import {getBusinessObject, is} from "bpmn-js/lib/util/ModelUtil";
import {getExtensionElementsAndName} from "@/components/business/util";
import Constants from "@/components/business/constants";

const starterPeopleGroup = {
    id: 'starterPeopleGroup',
    label: 'StarterPeopleGroup',
    shouldOpen:true,
    component:ListGroup,
    function:function (groups,element, translate,injector,register){
        element.translate = translate
        let a = StarterPeopleGroupProps({
            element,
            translate,
            injector,
            register
        })
        return a;
    }
};
export default starterPeopleGroup;

function StarterPeopleGroupProps(props) {
    const {
        id,element,injector,register
    } = props;
    if (!is(element, 'bpmn:UserTask')) {
        return;
    }
    const bpmnFactory = injector.get('bpmnFactory'),
        commandStack = injector.get('commandStack');
    const translate = injector.get('translate');
    const businessObject = getBusinessObject(element);
    let fieldType = getExtensionElementsAndName(businessObject,'activiti:Field',Constants.hissBusinessStarterUserType);
    if(fieldType&&fieldType.length){
        fieldType = fieldType[0].string || fieldType[0].stringValue || fieldType[0].expression;
    }
    const items = []
    items.push({
        id,
        label: getLabel(fieldType,translate),
        entries: getEntries(element,id,register,translate,businessObject,fieldType),
        shouldOpen:true
    })
    return {
        items,
        shouldOpen:true
    };
}

function getLabel(fieldType,translate){
    return "   "
    if (fieldType&&fieldType==Constants.values.fixedUser){
        return translate(Constants.values.fixedUser)
    }
    return translate(Constants.values.currentLoginUser)
}

function getEntries(element,id,register,translate,businessObject,fieldType){
    let arrs = ['starterPeopleType'];
    if (fieldType&&fieldType==Constants.values.fixedUser){
        arrs.push('starterFixedPeople')
    }
    const list = []
    for (let i = 0; i < arrs.length; i++) {
        let entries = register.findEntries(arrs[i],translate)
        entries = {...entries}
        entries.id = arrs[i];
        list.push(entries)
    }
    return list
}

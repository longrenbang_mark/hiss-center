import {ListGroup} from "@bpmn-io/properties-panel";
import {getBusinessObject, is} from "bpmn-js/lib/util/ModelUtil";
import {getRootElement, getRootElementProperties} from "@/components/business/util";
import businessConfig from '../config'

const conditionGroup = {
    id: 'conditionGroup',
    label: 'ConditionGroup',
    shouldOpen:true,
    component:ListGroup,
    function:function (groups,element, translate,injector,register){
        element.translate = translate
        let a = ConditionGroupProps({
            element,
            translate,
            injector,
            register
        })
        return a;
    }
};
export default conditionGroup;

function ConditionGroupProps(props) {
    const {
        id,element,injector,register
    } = props;
    if (!is(element, 'bpmn:UserTask')) {
        return;
    }
    let businessObject = getBusinessObject(element);
    let hissType = businessObject.get('hissType');
    let hissConf = businessConfig[hissType]
    const bpmnFactory = injector.get('bpmnFactory'),
        commandStack = injector.get('commandStack');
    const translate = injector.get('translate');
    const namespace = 'activiti'
    // 表单信息从root元素下获取
    const rootElement = getRootElement(element);
    // 获取所有的表单
    const forms = getRootElementProperties(rootElement,'formConfig')
    const items = forms.map((property, index) => {
        const id = element.id + '-property-' + index;
        // 表单元素的VALUE值
        let formData = eval('('+property.get('value')+')')
        if(!formData){
            formData = {}
        }
        // 如果不是自己添加的表单，则不能删
        let remove;
        return {
            id,
            label: 'Form:'+(formData.id||(index+1)),
            entries: ExtensionProperty(id,property,translate,injector,register,element),
            autoFocusEntry: id + '-name',
            remove: remove
        };
    });
    return {
        items,
        shouldOpen:true
    };
}
// 返回每个属性的填写字段
function ExtensionProperty(id,property,translate,injector,register,element) {
    const arrs = ['conditionType'];
    arrs.push('conditionFieldList')
    const entries = []
    for (let i = 0; i < arrs.length; i++) {
        let com = register.findEntries(arrs[i],translate);
        com = {...com}
        com.element=element
        com.register = register
        com.idPrefix=id + '-'+arrs[i]
        com.property = property;
        entries.push(com)
    }
    return entries;
}

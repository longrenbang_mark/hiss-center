/**
 * 更新元素名称
 * @param businessObject
 * @param name
 */
export function updateElementName (bpmnModeler,element,businessObject,name){
    let commandStack = bpmnModeler.get("commandStack");
    commandStack.execute('element.updateModdleProperties', {
        element,
        moddleElement: businessObject,
        properties: {
            name: name || ''
        }
    });
}

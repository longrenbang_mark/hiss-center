import request from '@/utils/request'
import url from './ApiUrl'
import mid from './MessageId'
import {packageMessage} from './PackageMessage'

/**
 * 获取表单的字段信息
 * @param data
 * @returns {*}
 */
export function formActionFields(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.formActionFields,data,messageAuth)
    })
}

/**
 * 获取表单的列表数据
 * @param data
 * @returns {*}
 */
export function formActionDataList(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.formActionDataList,data,messageAuth)
    })
}

/**
 * 删除表单数据
 * @param data
 * @returns {*}
 */
export function formActionDelete(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.formActionDelete,data,messageAuth)
    })
}

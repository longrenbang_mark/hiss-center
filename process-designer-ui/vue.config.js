const { defineConfig } = require('@vue/cli-service')
const NODE_ENV = process.env.NODE_ENV
var path = require('path')
module.exports = defineConfig({
  css:{
    extract:false
  },
  productionSourceMap: false,
  transpileDependencies: true,
  // outputDir: '../project-zhyl-admin-vue3-java/public/', // 打包输出目录
  outputDir: '../process-center-ui/public/', // 打包输出目录
  configureWebpack: {
    performance:{//打包文件大小配置
      "maxEntrypointSize": 10000000,
      "maxAssetSize": 30000000
    },
    optimization: {
      splitChunks: false,
    },
    entry: {
      app: NODE_ENV=='production'?'./src/build.js':'./src/main.js'
    },
    output: {
      libraryExport: 'default',
      libraryTarget: 'umd',
      filename: 'hiss.js'
    }
  },
  devServer: {
    port: 8080,
    open: false,
    proxy: {
      '/api/v1/receiver': {
        // target: 'http://localhost:8081/v1/receiver',
        target: 'http://localhost:8081',
        changeOrigin: true,
        // rewrite: (path) => path.replace(/^\/api/, '')
      }
    }
  }
})
